import logging
import threading

from prions.settings import cfg

APP_NAME = cfg.get('PROJECT', 'APP_NAME')

django_logger = logging.getLogger('django')

headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}

POSTS_PAGINATE_BY = 6
POSTS_BY_SLIDER = 4
RECENT_POSTS_COUNT = 8
POSTS_TOTAL = POSTS_PAGINATE_BY + POSTS_BY_SLIDER
POSTS_COUNT = 4
MEDIA_POSTS_COUNT = 6

_thread_locals = threading.local()


def set_current_user(user):
    _thread_locals.user = user


def get_current_user():
    return getattr(_thread_locals, 'user', None)
