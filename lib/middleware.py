import decimal
import itertools
import json
import os
import re
import urllib
from ast import literal_eval
from datetime import datetime
from math import cos, asin, sqrt
from time import strftime
from urllib.request import urlopen

import requests
import unidecode as unidecode
import urllib3
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connection, models
from django.db.models import Q
from django.db.models.fields.files import ImageFieldFile
from django.forms import model_to_dict
from django.utils.decorators import method_decorator
from django.utils.deprecation import MiddlewareMixin
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from requests.adapters import HTTPAdapter

from lib.config import django_logger, POSTS_PAGINATE_BY, set_current_user, get_current_user
from lib.resources import confs

app_name = __package__.split('.')[0]
notification = confs.get('notification')
mailer = confs.get('mailer')


class Hider(object):

    def __get__(self, instance, owner):
        raise AttributeError('Hidden attribute')

    def __set__(self, obj, val):
        raise AttributeError('Hidden attribute')


class CurrentUserMiddleware(MiddlewareMixin):
    def process_request(self, request):
        set_current_user(getattr(request, 'user', None))


class Monitor(models.Model):
    created_by = models.ForeignKey(User, on_delete = models.SET_NULL, blank = True, null = True,
                                   related_name = 'created_%(class)ss', editable = False)
    updated_by = models.ForeignKey(User, on_delete = models.SET_NULL, blank = True, null = True,
                                   related_name = 'updated_%(class)ss', editable = False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user and user.is_authenticated:
            self.updated_by = user
            if not self.id:
                self.created_by = user
        super().save(*args, **kwargs)


def upload_path(instance, filename):
    ext = filename.split('.')[-1]
    model = "{}".format(instance.__class__.__name__.lower())
    file_name = "{0}_{1}".format(model, generate_id()) if not hasattr(instance, 'slug') else instance.slug
    filename = '{0}.{1}'.format(file_name, ext)
    path = os.path.join("uploads", model)
    from apps.posts.models import Post
    if isinstance(instance, Post):
        path = os.path.join(path, "{}".format(instance.denomination.slug))
        post_date = strftime("%Y/%m/%d")
        path = os.path.join(path, post_date)
    return os.path.join(path, filename)


def upload_faith_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{0}.{1}'.format(instance.slug, ext)
    path = "{}".format(instance.denomination.name).lower()
    path = os.path.join("uploads", path)
    return os.path.join(path, filename)


def upload_media_files(instance, filename):
    ext = filename.split('.')[-1]
    file_name = "{0}_{1}".format(instance.model, generate_id()) if not hasattr(instance, 'slug') else instance.slug
    filename = '{0}.{1}'.format(file_name, ext)
    path = "{}".format(instance.model).lower()
    path = os.path.join("uploads", path)
    if instance.model.lower() == "post":
        post_date = strftime("%Y/%m/%d")
        path = os.path.join("uploads", post_date)
    return os.path.join(path, filename)


def generate_id():
    import uuid
    return uuid.uuid4()


def requests_retry_session(
        retries = 1,
        backoff_factor = 0.3,
        status_forcelist = (500, 502, 504),
        session = None,
):
    session = session or requests.Session()
    retry = urllib3.Retry(
        total = retries,
        read = retries,
        connect = retries,
        backoff_factor = backoff_factor,
        status_forcelist = status_forcelist,
    )
    adapter = HTTPAdapter(max_retries = retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    return session


def api_call(url = "", method = "", data = None, timeout = 15, retries = 1, verify = False, auth = None):
    try:
        if data is None:
            data = {}

        if "headers" in data:
            headers = data["headers"]
        else:
            headers = {}

        if "params" in data:
            params = data["params"]
        else:
            params = {}

        django_logger.info("{0} => {1}".format(method, url))

        r = None

        if method:
            if method == 'GET':
                r = requests_retry_session(retries = retries).get(url, headers = headers, params = params, timeout = timeout, verify = verify,
                                                                  auth = auth)
            elif method == 'PUT':
                r = requests_retry_session(retries = retries).put(url, headers = headers, data = params, timeout = timeout, verify = verify,
                                                                  auth = auth)
            elif method == 'POST':
                r = requests_retry_session(retries = retries).post(url, headers = headers, data = params, timeout = timeout, verify = verify,
                                                                   auth = auth)
            elif method == 'DELETE':
                r = requests_retry_session(retries = retries).delete(url, headers = headers, timeout = timeout)

            django_logger.info("{}".format(r))

        return r
    except requests.exceptions.ConnectionError as e:
        django_logger.error("Network problem occurred => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.HTTPError as e:
        django_logger.error("Invalid HTTP response => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.Timeout as e:
        django_logger.error("Oops ! The request has timed out => {}".format(e))
        return {"status": False, "message": "{}".format(e)}
    except requests.exceptions.RequestException as e:
        django_logger.error("An error occured => {}".format(e))
        return {"status": False, "message": "{}".format(e)}


def exec_api_call(url = "", method = "", params = None, timeout = 15, scope = '', verify = False, auth = None):
    data = {}
    data["headers"] = params.get('headers', None)
    if method != "GET":
        data["params"] = params.get('params')
    call = api_call(url, method, data, timeout = timeout, verify = verify, auth = auth)
    if "status" in call:
        return {}
    if not url_works(call.status_code):
        django_logger.error("{} API => {} : {}".format(scope, call.status_code, call.reason))
        return None
    return call.json()


def url_works(status_code):
    if 200 <= status_code < 400:
        return True
    else:
        return False


def remove_accents(s):
    return unidecode.unidecode(s)


def py_slugify(s):
    # Remove all non-word characters (everything except numbers and letters)
    s = re.sub(r"[^\w\s]", '', s)

    # Replace all runs of whitespace with a single underscore
    s = re.sub(r"\s+", '_', s)

    return s


def to_slug(s):
    # Remove all non-word characters (everything except numbers and letters)
    slug = slugify(s)
    return remove_accents(slug)


def dict_to_querydict(dictionary):
    from django.http import QueryDict
    from django.utils.datastructures import MultiValueDict

    qdict = QueryDict('', mutable = True)

    if type(dictionary) is dict:
        for key, value in dictionary.items():
            d = {key: value}
            qdict.update(MultiValueDict(d) if isinstance(value, list) else d)

    elif type(dictionary) is QueryDict:
        qdict.update(MultiValueDict(dictionary))

    return qdict


class JSONEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'isoformat'):  # handles both date and datetime objects
            return obj.isoformat()
        elif isinstance(obj, decimal.Decimal):
            return float(obj)
        elif isinstance(obj, ImageFieldFile):
            try:
                return obj.url
            except ValueError as e:
                return ''
        else:
            return json.JSONEncoder.default(self, obj)


def toJSON(m_object):
    return json.dumps(m_object, cls = JSONEncoder, default = lambda o: o.__dict__, sort_keys = True, indent = 4)


def dumper(obj):
    try:
        return obj.toJSON()
    except:
        return obj.__dict__


def dictfetchall(cursor):
    """Return all rows from a cursor as a dic"""
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def sql_query(query, params = None):
    with connection.cursor() as cursor:
        cursor.execute(query, params)
        rows = dictfetchall(cursor)

    return rows


def get_unique_slug(instance, title):
    slug_candidate = slug_original = slugify(title)
    for i in itertools.count(1):
        try:
            exists = instance.__class__._default_manager.get(slug = slug_candidate)
            if instance.pk != exists.pk:
                slug_candidate = '{}-{}'.format(slug_original, i)
            else:
                break
        except ObjectDoesNotExist:
            break

    return slug_candidate


def get_fields_and_properties(model, instance, fields = None):
    if fields is not None:
        field_names = fields
    else:
        field_names = [f.name for f in model._meta.fields if f.name not in ['pk', 'id']]
    property_names = [name for name in dir(model) if isinstance(getattr(model, name), property) if name not in ['pk', 'id']]
    return dict((name, "{}".format(getattr(instance, name))) for name in field_names + property_names)


def fields_and_properties(model, instance):
    # field_names = [f.name for f in model._meta.fields]
    property_names = [name for name in dir(model) if isinstance(getattr(model, name), property)]
    return {**model_to_dict(instance), **dict((name, getattr(instance, name)) for name in property_names)}


def download_file(instance, file):
    if instance.file_url and len(instance.file_url) != 0 and not file:
        img_temp = NamedTemporaryFile(delete = True)
        img_temp.write(urlopen(instance.file_url).read())
        img_temp.flush()
        file.save(os.path.basename(instance.file_url), File(img_temp))


def get_dates_from_date_range(daterange = '', delimiter = '-', has_time = False):
    dates = daterange.split(delimiter)
    date1 = create_sql_datetime(dates[0].strip(), has_time)
    date2 = create_sql_datetime(dates[1].strip(), has_time)
    return [date1, date2]


def date_reverse(delimiter_in = '/', delimiter_out = '-', date = ''):
    if delimiter_in == delimiter_out:
        array = date
    else:
        array = date.replace(delimiter_in, delimiter_out)
    split_date = array.split('-')
    reversed_list = list(reversed(split_date))
    return '-'.join(reversed_list)


def convert(s, time = False):
    if time:
        return datetime.strptime(s, '%Y-%m-%dT%H:%M:%SZ')
    else:
        return datetime.strptime(s, '%Y-%m-%d').date()


def create_sql_datetime(datetime = '', has_time = False):
    array = datetime.split()
    date = date_reverse('/', '-', array[0])
    if has_time is True:
        return date + ' ' + array[1]
    else:
        return date


def str_to_json(str = ""):
    if len(str) > 1:
        return literal_eval(str)
    else:
        return {}


def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a))


def closest(data, v):
    return sorted(data, key = lambda p: distance(v['lat'], v['lon'], p['lat'], p['lon']))


def extract_values(value):
    if value is not None:
        return {'id': value.get('key', None), 'value': value.get('value', None), 'type': value.get('type', None)}
    return None


def get_class_instance(klass, pk = None, slug = None):
    try:
        if slug is not None:
            return klass.objects.get(slug = slug)
        return klass.objects.get(pk = pk)
    except ObjectDoesNotExist as e:
        return None


def get_geo_loc(klass, geo_loc, search_result, by = "coords|address", place = False):
    if bool(geo_loc):
        coords = geo_loc.get('coords', None)
        addr = geo_loc.get('address', None)
        try:
            city = addr['city']
        except KeyError:
            city = addr['municipality']
        state = addr.get('state', None)
        if by == "coords":
            if coords is not None:
                place_list = [{'id': x.id, 'lat': float(x.lat) if x.lat else 0, 'lon': float(x.lon) if x.lon else 0} for x in search_result]
                closest_places = closest(place_list, coords)[:POSTS_PAGINATE_BY]

                return [get_class_instance(klass, x['id']) for x in closest_places], len(closest_places)
        elif by == "address":
            if place:
                search_result = search_result.filter(Q(place__city__name__icontains = state) | Q(place__town__name__icontains = city))
            else:
                search_result = search_result.filter(Q(city__name__icontains = state) | Q(town__name__icontains = city))

        return search_result
    return search_result


def nearest_date(items, pivot):
    if bool(items):
        nearest = min(items, key = lambda x: abs(x[1] - pivot))
        timedelta = abs(nearest[1] - pivot)
        return {
            "place_activity": nearest[0],
            "activity_details": get_activity_details(nearest[0]),
            "date": nearest[1],
            "days": timedelta.days
        }
    return None


def get_activity(place_activity, activity_date):
    if activity_date and activity_date != "":
        activity_date = date_reverse("-", "-", activity_date)
        activity_date = convert(activity_date)
        return [(x.get('place_activity'), x.get('date')) for x in get_occurrences(place_activity.id) if x['date'] == activity_date]


def get_activity_details(place_activity, date = None, language = None):
    from apps.religion.models import PlaceActivityDetails

    try:
        if date is None:
            return PlaceActivityDetails.objects.filter(place_activity_id = place_activity.pk, date__gte = datetime.today()).order_by('date').first()
        if language is None:
            return PlaceActivityDetails.objects.get(place_activity_id = place_activity.pk, date = date)
        return PlaceActivityDetails.objects.get(place_activity_id = place_activity.pk, date = date, language__codename = language)
    except ObjectDoesNotExist as e:
        return None


def get_occurrences(place_activity_id, what = '', language = None, time = None, limit = 1):
    from apps.religion.models import PlaceActivity
    if time is not None:
        try:
            time = datetime.strptime(time, '%H:%M').time()
            place_activity = PlaceActivity.objects.get(pk = place_activity_id, start_time__gte = time)
        except ObjectDoesNotExist:
            place_activity = None
    else:
        place_activity = PlaceActivity.objects.get(pk = place_activity_id)

    if place_activity is not None:

        if what == 'next':
            next_occ = place_activity.recurrences.after(
                datetime.today(),
                # dtstart = datetime.today(),
                inc = True,
            )

            if next_occ is None or place_activity is None:
                return None
            return place_activity, next_occ.date()

        occurrences = place_activity.recurrences

        if occurrences:
            end_date = datetime.today() + relativedelta(days = +15)
            occurrences = occurrences.occurrences(dtend = end_date, cache = True)

            # return [(x.date(), activity.start_time, place_activity_id, activity.place) for x in occurrences]
            activities_list = {}
            if what == "to_json":
                return [
                    {
                        "place": model_to_dict(place_activity.place, fields = [field.name for field in place_activity.place._meta.fields]),
                        "activity": model_to_dict(place_activity,
                                                  fields = [field.name for field in place_activity._meta.fields if field.name != "recurrences"]),
                        "date": o.date(),
                        "start_time": place_activity.start_time, "end_time": place_activity.end_time
                    } for o in occurrences if o != end_date
                ]
            else:
                return [
                    {
                        "activity": place_activity.activity,
                        "place": place_activity.place,
                        "place_activity": place_activity,
                        "activity_details": get_activity_details(place_activity, o.date(), language),
                        "date": o.date(),
                        "start_time": place_activity.start_time,
                        "end_time": place_activity.end_time
                    }
                    for o in occurrences if o != end_date
                ]
    else:
        return []


class CSRFExemptMixin(object):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSRFExemptMixin, self).dispatch(*args, **kwargs)


def url_with_querystring(path, params = None):
    if params is None:
        params = {}
    return path + '?' + urllib.parse.urlencode(params)
