import json
import os

from django.conf import settings
from prions.settings import cfg

confs = {}
files = []

for filename in os.listdir(settings.RESOURCES_DIR):
    if filename.endswith(".json"):
        cfg.read('{}'.format(filename))

        config_name = os.path.splitext(filename)[0]
        files.append(config_name)

        with open('{}/{}'.format(settings.RESOURCES_DIR, filename), 'r') as f:
            var = json.load(f)
        confs['{}'.format(config_name)] = var
