"""prions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.utils.translation import ugettext_lazy
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog

from prions.settings import cfg

admin.site.site_title = ugettext_lazy('{}'.format(cfg.get('PROJECT', 'APP_DESCRIPTION')))
admin.site.site_header = ugettext_lazy('{}'.format(cfg.get('PROJECT', 'APP_DESCRIPTION')))

urlpatterns = [
    re_path(r'^favicon\.ico$', RedirectView.as_view(url = '/static/core/icons/favicon.ico')),
    # path('api/', include('backend.urls')),
    path('admin/', admin.site.urls),
    path('', include('apps.core.urls', namespace = 'core')),
    path('administrator/', include('apps.administrator.urls', namespace = 'administrator')),
    # re_path('api/(?P<version>(v1|v2))/', include('apps.api.urls', namespace = 'api')),
    # path('api-auth/', include('rest_framework.urls', namespace = 'rest_framework')),
]

# Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]

# Other plugins urls
urlpatterns += [
    re_path(r'^captcha/', include('captcha.urls')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

# If you already have a js_info_dict dictionary, just add
# 'recurrence' to the existing 'packages' tuple.
"""js_info_dict = {
    'packages': ('recurrence',),
}"""

# jsi18n can be anything you like here
"""urlpatterns += [
    re_path(r'^jsi18n/$', JavaScriptCatalog.as_view(), js_info_dict),
]"""

urlpatterns += [path('jsi18n.js', JavaScriptCatalog.as_view(packages = ['recurrence']), name = 'jsi18n'), ]

# For Ads
# urlpatterns += re_path(r'^ads/', include('ads.urls')),

if settings.DEBUG is True:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] + urlpatterns
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
