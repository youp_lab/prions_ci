from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.posts.models import *

for model in apps.get_app_config('posts').models.values():
    admin.site.register(model)


class TagAdmin(admin.ModelAdmin):
    search_fields = ['name']


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name']


class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    autocomplete_fields = ['group', 'denomination', 'place', 'category', 'tags']
    search_fields = ['title']
    list_filter = ('group', 'denomination', 'category',)
    list_display = ('id', 'title', 'category', 'excerpt', 'flash', 'status', 'denomination', 'created_at', 'created_by',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class AnnounceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    autocomplete_fields = ['group', 'denomination', 'place', 'category']
    search_fields = ['title']
    list_filter = ('group', 'denomination', 'category',)
    list_display = ('id', 'title', 'category', 'excerpt', 'status', 'denomination', 'created_at', 'created_by',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class NecrologyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ['title']
    list_display = ('id', 'title', 'excerpt', 'status', 'created_at', 'created_by',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class TestimonyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    autocomplete_fields = ['group', 'denomination', 'place']
    search_fields = ['title']
    list_display = ('id', 'title', 'excerpt', 'status', 'created_at', 'created_by',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class MediaTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'codename': ('label',)}
    search_fields = ['label']


class MediaSourceTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'codename': ('label',)}
    search_fields = ['label']


class GalleryAdmin(admin.ModelAdmin):
    list_filter = ('type',)
    search_fields = ['name']
    autocomplete_fields = ['group', 'denomination', 'type', 'source_type', 'tags']
    list_display = ('id', 'name', 'type', 'source_type', 'status', 'created_at', 'created_by',)
    prepopulated_fields = {'slug': ('name',)}

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


admin.site.unregister(Tag)
admin.site.register(Tag, TagAdmin)
admin.site.unregister(Category)
admin.site.register(Category, CategoryAdmin)
admin.site.unregister(Article)
admin.site.register(Article, ArticleAdmin)
admin.site.unregister(Announce)
admin.site.register(Announce, AnnounceAdmin)
admin.site.unregister(Necrology)
admin.site.register(Necrology, NecrologyAdmin)
admin.site.unregister(Testimony)
admin.site.register(Testimony, TestimonyAdmin)
admin.site.unregister(MediaType)
admin.site.register(MediaType, MediaTypeAdmin)
admin.site.unregister(MediaSourceType)
admin.site.register(MediaSourceType, MediaSourceTypeAdmin)
admin.site.unregister(Gallery)
admin.site.register(Gallery, GalleryAdmin)
