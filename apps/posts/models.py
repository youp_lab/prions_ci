from captcha.fields import CaptchaField
from ckeditor_uploader.fields import RichTextUploadingField
from dateutil.relativedelta import relativedelta
from django.db import models
from django.db.models import FileField
from django.urls import reverse
from django.utils import timezone
from django_countries.fields import CountryField
from sorl.thumbnail import ImageField

from apps.core.enums import STATUS_CHOICES, PUBLISHED, STATE_CHOICES, INACTIVATED, DEATH_LINK_CHOICES
from apps.managers import PostsQuerySet
from apps.managers.medias import MediaManager
from apps.religion.models import Religion
from lib.middleware import get_unique_slug, Monitor, Hider, upload_path, to_slug


class MediaType(Monitor):
    id = models.AutoField(primary_key = True)
    label = models.CharField(max_length = 45, blank = True, verbose_name = 'Libellé')
    codename = models.CharField(max_length = 90, blank = True, verbose_name = 'Type de média')

    class Meta:
        verbose_name = 'Type de média'
        verbose_name_plural = 'Types des médias'
        ordering = ['label']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        codename = to_slug(self.label)
        if self.codename != codename:
            self.codename = codename
        super(MediaType, self).save(*args, **kwargs)


class MediaSourceType(Monitor):
    id = models.AutoField(primary_key = True)
    label = models.CharField(max_length = 45, blank = True, verbose_name = 'Libellé')
    codename = models.CharField(max_length = 90, blank = True, verbose_name = 'Type de média')

    class Meta:
        verbose_name = 'Source du média'
        verbose_name_plural = 'Sources des médias'
        ordering = ['label']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        codename = to_slug(self.label)
        if self.codename != codename:
            self.codename = codename
        super(MediaSourceType, self).save(*args, **kwargs)


class Gallery(Monitor, Religion):
    id = models.BigAutoField(primary_key = True)
    type = models.ForeignKey(MediaType, models.PROTECT, verbose_name = 'Type de média')
    source_type = models.ForeignKey(MediaSourceType, models.PROTECT, blank = True, null = True, verbose_name = 'Source du média')
    name = models.CharField(max_length = 255, blank = True, verbose_name = 'Titre')
    slug = models.SlugField(unique = True)
    tags = models.ManyToManyField('posts.Tag', blank = True, verbose_name = 'Tags')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    file = models.FileField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Fichier')
    link = models.URLField(blank = True, null = True, verbose_name = 'Lien')
    status = models.BooleanField(null = True, choices = STATUS_CHOICES, db_index = True, verbose_name = 'Statut')
    media_code = models.CharField(max_length = 60, blank = True, null = True, db_index = True, editable = False)
    place = Hider()
    objects = MediaManager()

    class Meta:
        verbose_name = 'Galérie'
        verbose_name_plural = 'Galéries'
        ordering = ['-created_at']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(Gallery, self).save(*args, **kwargs)


class Category(Monitor, Religion):
    id = models.AutoField(primary_key = True)
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Catégorie Parent')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    slug = models.SlugField(unique = True)
    status = models.BooleanField(null = True, default = PUBLISHED, db_index = True, verbose_name = 'Publié')
    place = Hider()

    class Meta:
        verbose_name = 'Catégorie'
        verbose_name_plural = 'Catégories'
        ordering = ['name']

    def __str__(self):
        return self.name


class Tag(Monitor):
    id = models.BigAutoField(primary_key = True)
    name = models.CharField(max_length = 60, unique = True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Post(Monitor, Religion):
    id = models.BigAutoField(primary_key = True)
    category = models.ForeignKey(Category, models.PROTECT, verbose_name = 'Catégorie')
    title = models.CharField(max_length = 170, db_index = True, verbose_name = 'Titre')
    slug = models.SlugField(unique = True)
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image')
    file_url = models.URLField(blank = True, null = True, verbose_name = 'URL de l\'image')
    excerpt = models.CharField(max_length = 255, default = '', verbose_name = 'Résumé')
    tags = models.ManyToManyField(Tag, blank = True, verbose_name = 'Tags')
    flash = models.BooleanField(default = False, verbose_name = 'Flash ?')
    content = RichTextUploadingField(default = '', verbose_name = 'Contenu')
    source = models.CharField(max_length = 255, blank = True, verbose_name = 'Source de l\'article')
    # stick = models.BooleanField(default = False, verbose_name = '')
    status = models.BooleanField(null = True, choices = STATUS_CHOICES, db_index = True, verbose_name = 'Statut')
    published_on = models.DateTimeField(db_index = True, verbose_name = 'Publié le')
    end_publication = models.DateTimeField(db_index = True, blank = True, null = True, verbose_name = 'Fin de publication')
    absolute_url = models.CharField(max_length = 400, blank = True, editable = False)
    objects = PostsQuerySet().as_manager()

    class Meta:
        abstract = True
        ordering = ['-published_on']

    # def save(self, *args, **kwargs):
    #     slug = get_unique_slug(self, self.title)
    #     if self.slug != slug:
    #         self.slug = slug
    #     super(Post, self).save(*args, **kwargs)

    @property
    def is_actual(self):
        """
        Checks if an entry is within his publication period.
        """
        now = timezone.now()
        if self.published_on and now < self.published_on:
            return False

        if self.end_publication and now >= self.end_publication:
            return False
        return True

    @property
    def is_visible(self):
        """
        Checks if an entry is visible and published.
        """
        return self.is_actual and self.status == PUBLISHED

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url


class Article(Post):
    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('core:article',
                       kwargs = {
                           'group': self.group.slug,
                           'denomination': self.denomination.slug,
                           'slug': self.slug
                       })


class Announce(Post):
    # category = Hider()
    tags = Hider()
    flash = Hider()
    source = Hider()

    class Meta:
        verbose_name = 'Annonce'
        verbose_name_plural = 'Annonces'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('core:announce',
                       kwargs = {
                           'group': self.group.slug,
                           'denomination': self.denomination.slug,
                           'slug': self.slug
                       })


class Necrology(Post):
    date = models.DateField(db_index = True, verbose_name = 'Date du décès')
    content = RichTextUploadingField(default = '', blank = True, verbose_name = 'Annonce')
    ceremony = RichTextUploadingField(default = '', blank = True, verbose_name = 'Cérémonies')
    thanks = RichTextUploadingField(default = '', blank = True, verbose_name = 'Remerciements')
    in_memoriam = RichTextUploadingField(default = '', blank = True, verbose_name = 'In Memoriam')
    image = ImageField(upload_to = 'uploads/necrology/%Y/%m/%d/', blank = True, null = True)
    file = FileField(upload_to = 'uploads/necrology/%Y/%m/%d/', blank = True, null = True, verbose_name = 'Fichier, Flyer, ...')
    category = Hider()
    tags = Hider()
    group = Hider()
    denomination = Hider()
    place = Hider()
    flash = Hider()
    source = Hider()
    published_on = Hider()
    end_publication = Hider()

    class Meta:
        verbose_name = 'Nécrologie'
        verbose_name_plural = 'Nécrologie'
        ordering = ['-date']

    def __str__(self):
        return self.title

    @property
    def is_birthday(self):
        """
        Checks if it's the birthday of an entry.
        """
        now = timezone.now()
        if self.date.month == now.month and self.date.day == now.day:
            return True

    @property
    def has_thanks(self):
        """
        Checks if thanks entry is not empty
        """
        if len(self.thanks) > 0:
            return True
        return False

    @property
    def has_birth(self):
        """
        Checks if it's the birthday of an entry.
        """
        now = timezone.now()
        next_date = self.date + relativedelta(years = +1)
        if next_date <= now.date():
            return True
        return False


class NecrologyCondolences(Monitor):
    id = models.BigAutoField(primary_key = True)
    necrology = models.ForeignKey(Necrology, models.PROTECT, verbose_name = 'Décès')
    last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    first_name = models.CharField(max_length = 160, blank = True, verbose_name = 'Prénom(s)')
    email = models.EmailField(max_length = 160, verbose_name = 'Email',
                              help_text = 'Votre e-mail sera caché et servira à recevoir le message de remerciement de la famille')
    link_with_death = models.CharField(max_length = 160, choices = DEATH_LINK_CHOICES, default = "Parent", blank = True,
                                       verbose_name = 'Lien avec défunt')
    city = models.ForeignKey('location.City', models.SET_NULL, blank = True, null = True, verbose_name = 'Ville')
    country = CountryField(blank = True, null = True, verbose_name = 'Votre pays de résidence')
    message = models.TextField(default = '', verbose_name = 'Votre message à la famille')
    status = models.BooleanField(choices = STATE_CHOICES, default = INACTIVATED, db_index = True, verbose_name = 'Statut')
    captcha = CaptchaField()

    class Meta:
        verbose_name = 'Cahier de condoléances'
        verbose_name_plural = 'Cahiers de condoléances'
        ordering = ['-created_at']
        unique_together = (('necrology', 'email'),)

    @property
    def full_name(self):
        return '{0} {1}'.format(self.last_name, self.first_name)


class Testimony(Post):
    category = Hider()
    tags = Hider()
    flash = Hider()
    source = Hider()

    class Meta:
        verbose_name = 'Témoignage'
        verbose_name_plural = 'Témoignages'
        ordering = ['-published_on']

    def __str__(self):
        return self.title
