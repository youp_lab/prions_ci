from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django_cleanup.signals import cleanup_pre_delete
from sorl.thumbnail import delete

from lib.middleware import download_file, get_unique_slug


@receiver(post_save)
def update_post_file(sender, instance, **kwargs):
    if instance.__class__.__name__ in ['MediaFiles', 'Multimedia']:
        download_file(instance, instance.file)
    if instance.__class__.__name__ in ['Article', 'Announce', 'Necrology', 'Testimony', 'Place']:
        download_file(instance, instance.image)


@receiver(pre_save)
def update_video_code(sender, instance, **kwargs):
    if instance.__class__.__name__ in ['MediaFiles', 'Multimedia', 'Gallery']:
        if instance.link:
            if 'youtube' in instance.link:
                code = instance.link.rsplit('/', 1)[-1]
                instance.media_code = code


@receiver(pre_save)
def update_unique_slug(sender, instance, **kwargs):
    if instance.__class__.__name__ in ['Testimony']:
        instance.slug = get_unique_slug(instance, instance.title)


def sorl_delete(**kwargs):
    delete(kwargs['file'])


cleanup_pre_delete.connect(sorl_delete)

"""@receiver(post_save)
def get_absolute_url(sender, instance, **kwargs):
    if instance.__class__.__name__ in ["Article", "Announce", "Multimedia", "Necrology", "Testimony"]:
        url = {'group': instance.group.slug, 'denomination': instance.denomination.slug, 'category': instance.category.slug, 'slug': instance.slug}
        return ''.join('{}/'.format(val) for key, val in url.items()).rstrip('/')"""
