from datetime import datetime

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django_cleanup.signals import cleanup_pre_delete
from sorl.thumbnail import delete

from apps.religion.models import ActivityRequest, ContactRequest, ReceiveRequest


@receiver(pre_save)
def update_religion(sender, instance, **kwargs):
    group = None
    denomination = None
    if instance.__class__.__name__ not in ['LogEntry', 'Necrology', 'ActivityRequest', 'ReceiveRequest', 'Responsible']:
        if hasattr(sender, 'place') and instance.place:
            denomination = instance.place.denomination
            group = instance.place.denomination.group
        if hasattr(sender, 'denomination') and instance.denomination:
            denomination = instance.denomination
            group = instance.denomination.group
        if hasattr(sender, 'group') and instance.group:
            group = instance.group
        if hasattr(sender, 'absolute_url') and denomination is not None:
            url = {
                # 'group': group.slug,
                'denomination': denomination.slug,
                'category': 'annonce' if instance.__class__.__name__.lower() == 'announce' else instance.__class__.__name__.lower(),
                'slug': instance.slug
            }
            instance.absolute_url = ''.join('{}/'.format(val) for key, val in url.items()).rstrip('/')

        instance.group = group
        instance.denomination = denomination


@receiver(post_save)
def add_activity_request_reference(sender, instance, created, **kwargs):
    if created and sender == ActivityRequest:
        instance.reference = '#ACTIVITY-{0}/{1}/{2}'.format(instance.id, datetime.now().date().strftime('%Y%m%d'), instance.place_id)
        instance.save()


@receiver(post_save)
def add_contact_request_reference(sender, instance, created, **kwargs):
    if created and sender == ContactRequest:
        instance.reference = '#CONTACT-{0}/{1}/{2}'.format(instance.id, instance.date.date().strftime('%Y%m%d'), instance.place_id)
        instance.save()


@receiver(post_save)
def add_receive_request_reference(sender, instance, created, **kwargs):
    if created and sender == ReceiveRequest:
        instance.reference = '#SOS-{0}/{1}'.format(instance.id, instance.created_at.date().strftime('%Y%m%d'))
        instance.save()


def sorl_delete(**kwargs):
    delete(kwargs['file'])


cleanup_pre_delete.connect(sorl_delete)
