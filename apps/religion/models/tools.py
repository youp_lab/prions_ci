from django.db import models
from sorl.thumbnail import ImageField

from apps.core.enums import STATE_CHOICES, RECEIVE_TYPE_CHOICES, INACTIVATED
from apps.managers import ReligionQuerySet
from apps.religion.models import Religion, Place
from lib.middleware import Hider, Monitor, upload_path


class ActivityRequestType(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey('religion.Denomination', models.SET_DEFAULT, default = 1, verbose_name = 'Denomination')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Type de demande'
        verbose_name_plural = 'Types de demandes'
        ordering = ['name']

    def __str__(self):
        return self.name


class ActivityRequest(Monitor):
    id = models.BigAutoField(primary_key = True)
    type = models.ForeignKey(ActivityRequestType, models.SET_DEFAULT, default = 1, verbose_name = 'Type')
    reference = models.CharField(max_length = 25, blank = True, editable = False)
    place = models.ForeignKey(Place, models.PROTECT, verbose_name = 'Lieu de prière')
    sender_last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    sender_first_name = models.CharField(max_length = 160, verbose_name = 'Prénom(s)')
    sender_contact = models.CharField(max_length = 45, verbose_name = 'Contact Téléphonique')
    sender_email = models.EmailField(max_length = 160, verbose_name = 'Email')
    description = models.TextField(verbose_name = 'Demande')
    start_date = models.DateField(verbose_name = 'Date de début')
    end_date = models.DateField(verbose_name = 'Date de fin')

    class Meta:
        verbose_name = 'Demande d\'activité'
        verbose_name_plural = 'Demandes d\'activité'

    def __str__(self):
        return '{0} - Demande de {1} : {2} {3}, {4}'.format(self.id, self.place.denomination.denomination_details.celebration,
                                                            self.place.place_type.name, self.place.name,
                                                            self.place.city.name)


class ContactRequest(Monitor, Religion):
    id = models.BigAutoField(primary_key = True)
    reference = models.CharField(max_length = 25, blank = True, editable = False)
    place = models.ForeignKey(Place, models.PROTECT, verbose_name = 'Lieu de prière')
    subject = models.CharField(max_length = 160, verbose_name = 'Objet')
    sender_last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    sender_first_name = models.CharField(max_length = 160, verbose_name = 'Prénom(s)')
    sender_contact = models.CharField(max_length = 20, verbose_name = 'Contact Téléphonique')
    sender_email = models.EmailField(max_length = 160, verbose_name = 'Email')
    description = models.TextField(verbose_name = 'Message')
    date = models.DateTimeField(auto_now_add = True, verbose_name = 'Date')

    class Meta:
        verbose_name = 'Contacter un prêtre'
        verbose_name_plural = 'Contacter un prêtre'

    def __str__(self):
        return 'Contact : {0} - {1} {2}, {3}'.format(self.subject, self.place.place_type.name, self.place.name, self.place.city.name)


class ReceiveRequest(Monitor, Religion):
    place = models.ForeignKey(Place, models.PROTECT, verbose_name = 'Lieu de prière')
    id = models.BigAutoField(primary_key = True)
    reference = models.CharField(max_length = 25, blank = True, editable = False)
    subject = models.CharField(max_length = 160, verbose_name = 'Objet')
    last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    first_name = models.CharField(max_length = 160, verbose_name = 'Prénom(s)')
    contact = models.CharField(max_length = 20, verbose_name = 'Contact Téléphonique')
    email = models.EmailField(max_length = 160, blank = True, verbose_name = 'Email')
    description = models.TextField(verbose_name = 'Description')
    status = models.BooleanField(choices = STATE_CHOICES, blank = True, default = INACTIVATED, verbose_name = 'Statut')
    denomination = Hider()
    group = Hider()
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Demande de don'
        verbose_name_plural = 'Demandes de dons'

    def __str__(self):
        return '{0} - {1} {2}, {3}'.format(self.subject, self.place.place_type.name, self.place.name, self.place.city.name)

    @property
    def receiver_name(self):
        return '{0} {1}'.format(self.last_name, self.first_name)


class ReceiveRequestImage(Monitor):
    receive_request = models.ForeignKey(ReceiveRequest, models.CASCADE, related_name = 'images')
    image = ImageField(upload_to = upload_path, blank = True, null = True)


class Donation(Monitor):
    id = models.BigAutoField(primary_key = True)
    receive_request = models.ForeignKey(ReceiveRequest, models.PROTECT, related_name = 'receive_ref', verbose_name = 'Demande')
    last_name = models.CharField(max_length = 160, verbose_name = 'Nom')
    first_name = models.CharField(max_length = 160, verbose_name = 'Prénom(s)')
    contact = models.CharField(max_length = 20, verbose_name = 'Contact Téléphonique')
    email = models.EmailField(max_length = 160, verbose_name = 'Email')
    type = models.CharField(max_length = 10, choices = RECEIVE_TYPE_CHOICES, verbose_name = 'Type de don')
    comment = models.TextField(verbose_name = 'Commentaire')

    class Meta:
        verbose_name = 'Don'
        verbose_name_plural = 'Dons'
        ordering = ['-created_at']

    def __str__(self):
        return 'Don : {}'.format(self.receive_request.subject)

    @property
    def sender_name(self):
        return '{0} {1}'.format(self.last_name, self.first_name)
