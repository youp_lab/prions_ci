from django.db import models
from recurrence.fields import RecurrenceField

from lib.middleware import Monitor, to_slug, Hider
from apps.religion import my_fields
from apps.managers import ReligionQuerySet
from apps.religion.models import Group, Denomination, Place, PlaceOfficiant, Religion


class ActivityPeriod(Monitor):
    id = models.AutoField(primary_key = True)
    group = models.ForeignKey(Group, models.SET_NULL, blank = True, null = True, default = None, verbose_name = 'Groupe')
    denomination = models.ForeignKey(Denomination, models.SET_NULL, blank = True, null = True, default = None, verbose_name = 'Dénomination')
    name = models.CharField(max_length = 150, verbose_name = 'Titre', help_text = 'Période: Ordinaire, Noël, Tabaski)')
    codename = models.CharField(max_length = 15, editable = False)
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Période'
        verbose_name_plural = 'Périodes'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        codename = to_slug(self.name)
        if self.codename != codename:
            self.codename = codename
        super(ActivityPeriod, self).save(*args, **kwargs)


class ActivityType(Monitor, Religion):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 150, verbose_name = 'Titre')
    description = models.TextField(blank = True, null = True, verbose_name = 'Description')
    codename = models.CharField(max_length = 15, editable = False)
    place = Hider()

    class Meta:
        verbose_name = 'Type de célébration'
        verbose_name_plural = 'Types de célébrations'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        codename = to_slug(self.name)
        if self.codename != codename:
            self.codename = codename
        super(ActivityType, self).save(*args, **kwargs)


class Activity(Monitor, Religion):
    id = models.BigAutoField(primary_key = True)
    activity_type = models.ForeignKey(ActivityType, models.PROTECT, verbose_name = 'Type de célébration')
    title = models.CharField(max_length = 255, db_index = True, verbose_name = 'Titre')
    description = models.TextField(blank = True, verbose_name = 'Description')
    day_of_week = my_fields.DayOfTheWeekField(blank = True, verbose_name = 'Jour de la semaine')
    is_full_day_event = models.BooleanField(null = True, default = False, verbose_name = 'Toute la journée')
    is_recurring = models.BooleanField(null = True, default = True, verbose_name = 'Récurrent')
    place = Hider()
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Célébration'
        verbose_name_plural = 'Célébrations'

    def __str__(self):
        return '{0}'.format(self.title)


class ActivityRecurringType(Monitor):
    id = models.AutoField(primary_key = True)
    label = models.CharField(max_length = 45, blank = True, verbose_name = 'Libellé')

    class Meta:
        verbose_name = 'Type de recurrence'
        verbose_name_plural = 'Types de recurrences'
        ordering = ['label']

    def __str__(self):
        return self.label


class ActivityRecurringPattern(Monitor):
    activity = models.OneToOneField(Activity, models.CASCADE, verbose_name = 'Activité')
    recurring_type = models.ForeignKey(ActivityRecurringType, models.CASCADE, verbose_name = 'Type de récurrence')
    separation_count = models.IntegerField(default = 0)
    max_num_of_occurrences = models.IntegerField(blank = True, null = True)
    day_of_week = models.IntegerField(blank = True, null = True)
    week_of_month = models.IntegerField(blank = True, null = True)
    day_of_month = models.IntegerField(blank = True, null = True)
    month_of_year = models.IntegerField(blank = True, null = True)

    class Meta:
        verbose_name = 'Modèle de recurrence'
        verbose_name_plural = 'Modèles de recurrences'

    def __str__(self):
        return '{0} - {1}'.format(self.activity.title, self.recurring_type.label)


class ActivityInstanceException(Monitor):
    id = models.BigAutoField(primary_key = True)
    activity = models.ForeignKey(Activity, models.CASCADE, verbose_name = 'Activité')
    is_rescheduled = models.BooleanField(verbose_name = 'Est reprogrammé')
    is_cancelled = models.BooleanField(verbose_name = 'Est annulé')
    start_date = models.DateField(verbose_name = 'Date de début')
    end_date = models.DateField(verbose_name = 'Date de fin')
    start_time = models.TimeField(verbose_name = 'Heure de début')
    end_time = models.TimeField(verbose_name = 'Heure de fin')
    is_full_day_event = models.BooleanField(null = True, verbose_name = 'Toute la journée')

    class Meta:
        verbose_name = 'Exception d\'une instance de célébration'
        verbose_name_plural = 'Exceptions des instances de célébration'

    def __str__(self):
        return self.activity


class ActivityLanguage(Monitor):
    id = models.AutoField(primary_key = True)
    label = models.CharField(max_length = 150, verbose_name = 'Langue')
    codename = models.CharField(max_length = 15, editable = False)

    class Meta:
        verbose_name = 'Langue de Célébration'
        verbose_name_plural = 'Langues de Célébrations'

    def __str__(self):
        return '{}'.format(self.label)

    def save(self, *args, **kwargs):
        codename = to_slug(self.label)
        if self.codename != codename:
            self.codename = codename
        super(ActivityLanguage, self).save(*args, **kwargs)


class PlaceActivity(Monitor):
    place = models.ForeignKey(Place, models.CASCADE, verbose_name = 'Lieu de prière')
    activity = models.ForeignKey(Activity, models.CASCADE, verbose_name = 'Activité')
    recurrences = RecurrenceField(blank = True, null = True, default = None, verbose_name = 'Occurences')
    start_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de début')
    end_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de fin')

    class Meta:
        unique_together = (('place', 'activity'),)
        verbose_name = 'Activité du lieu de culte'
        verbose_name_plural = 'Activités des lieux de culte'

    def __str__(self):
        return '{0} : {1}'.format(self.place.name, self.activity.title)


class PlaceActivityDetails(Monitor):
    place_activity = models.ForeignKey(PlaceActivity, models.PROTECT, related_name = 'placeactivitydetails')
    date = models.DateField(verbose_name = 'Date')
    officiant = models.ForeignKey(PlaceOfficiant, models.SET_NULL, blank = True, null = True, verbose_name = 'Officiant')
    language = models.ForeignKey(ActivityLanguage, models.SET_NULL, blank = True, null = True, verbose_name = 'Langue')
    online_prayer = models.URLField(blank = True, null = True, verbose_name = 'Messe en ligne')
    particularity = models.CharField(max_length = 150, default = '', blank = True, verbose_name = 'Particularité',
                                     help_text = 'Deuxième quête, Baptême, ...')

    class Meta:
        unique_together = (('place_activity', 'date'),)
        verbose_name = 'Information'
        verbose_name_plural = 'Informations'
        ordering = ['-date']

    def __str__(self):
        return '{0} {1}'.format(self.place_activity, self.date)
