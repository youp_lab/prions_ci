from django.db import models
from django.urls import reverse
from sorl.thumbnail import ImageField

from apps.core.enums import PUBLISHED
from apps.posts.models import Post
from lib.middleware import Monitor, Hider, upload_path, get_unique_slug


class PlaceCategory(Monitor):
    id = models.AutoField(primary_key = True)
    place = models.ForeignKey('religion.Place', models.PROTECT, related_name = 'placecategories', verbose_name = 'Lieu de prière')
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Catégorie Parent')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    slug = models.SlugField(unique = True, blank = True, null = True)
    status = models.BooleanField(null = True, default = PUBLISHED, db_index = True, verbose_name = 'Publié')

    class Meta:
        verbose_name = 'Catégorie'
        verbose_name_plural = 'Catégories'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(PlaceCategory, self).save(*args, **kwargs)


class PlaceTag(Monitor):
    id = models.BigAutoField(primary_key = True)
    place = models.ForeignKey('religion.Place', models.PROTECT, related_name = 'placetags', verbose_name = 'Lieu de prière')
    name = models.CharField(max_length = 60, unique = True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class PlaceAnnounce(Post):
    category = models.ForeignKey(PlaceCategory, models.PROTECT, verbose_name = 'Catégorie')
    tags = models.ManyToManyField(PlaceTag, blank = True, verbose_name = 'Tags')
    file_url = Hider()
    flash = Hider()
    source = Hider()
    slug = models.SlugField(unique = True, blank = True, null = True)

    class Meta:
        verbose_name = 'Annonce'
        verbose_name_plural = 'Annonces'

    def __str__(self):
        return '{} : {}'.format(self.place.name, self.title)

    def get_absolute_url(self):
        return reverse('core:place-announce',
                       kwargs = {
                           'group': self.place.denomination.group.slug,
                           'denomination': self.place.denomination.slug,
                           'place': self.place.slug,
                           'announce': self.slug
                       })

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.title)
        if self.slug != slug:
            self.slug = slug
        super(PlaceAnnounce, self).save(*args, **kwargs)


class PlaceCommunity(Post):
    category = models.ForeignKey(PlaceCategory, models.PROTECT, verbose_name = 'Catégorie')
    tags = models.ManyToManyField(PlaceTag, blank = True, verbose_name = 'Tags')
    file_url = Hider()
    flash = Hider()
    source = Hider()
    slug = models.SlugField(unique = True, blank = True, null = True)

    class Meta:
        verbose_name = 'Vie du lieu de prière'
        verbose_name_plural = 'Vie des lieux de prière'

    def __str__(self):
        return '{} : {}'.format(self.place.name, self.title)

    def get_absolute_url(self):
        return reverse('core:place-life',
                       kwargs = {
                           'group': self.place.denomination.group.slug,
                           'denomination': self.place.denomination.slug,
                           'place': self.place.slug,
                           'life': self.slug
                       })

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.title)
        if self.slug != slug:
            self.slug = slug
        super(PlaceCommunity, self).save(*args, **kwargs)
