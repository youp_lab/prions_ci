from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.urls import reverse
from sorl.thumbnail import ImageField

from apps.core.enums import STATE_CHOICES, INACTIVATED, STATUS_CHOICES, ACTIVATED, PLACE_TYPE_CHOICES, CREATION_DATE
from apps.location.models import Location, Geo
from apps.managers import ReligionQuerySet
from lib.middleware import get_unique_slug, Monitor, upload_path, upload_faith_path, to_slug


class Group(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 45, unique = True, verbose_name = 'Nom')
    contact = models.CharField(max_length = 30, blank = True, verbose_name = 'Contact Téléphonique')
    email = models.EmailField(max_length = 30, blank = True, verbose_name = 'Email')
    address = models.CharField(max_length = 45, blank = True, verbose_name = 'Adresse')
    localisation = models.CharField(max_length = 45, blank = True)
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    content = RichTextUploadingField(default = '', blank = True, verbose_name = 'Contenu')
    logo = ImageField(upload_to = upload_path, blank = True, verbose_name = 'Logo')
    background = ImageField(upload_to = upload_path, blank = True, verbose_name = 'Background')
    slug = models.SlugField(unique = True)
    common_name = models.CharField(max_length = 45, blank = True, verbose_name = 'Nom Commun')
    status = models.BooleanField(choices = STATE_CHOICES, default = INACTIVATED, verbose_name = 'Statut')
    visible = models.BooleanField(choices = STATE_CHOICES, default = INACTIVATED, verbose_name = 'Est visible ?')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Groupe Réligieux'
        verbose_name_plural = 'Groupes Réligieux'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(Group, self).save(*args, **kwargs)


class Denomination(Monitor):
    id = models.AutoField(primary_key = True)
    group = models.ForeignKey(Group, models.CASCADE, verbose_name = 'Groupe')
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Parent')
    # parent = models.ForeignKey(DenominationOrganization, models.SET_NULL, blank = True, null = True, verbose_name = 'Parent')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    content = RichTextUploadingField(default = '', blank = True, verbose_name = 'Contenu')
    logo = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Logo')
    e_shop = models.URLField(blank = True, null = True, verbose_name = 'E-boutique')
    slug = models.SlugField(unique = True)
    status = models.BooleanField(choices = STATE_CHOICES, default = INACTIVATED, verbose_name = 'Statut')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Dénomination Réligieuse'
        verbose_name_plural = 'Dénominations Réligieuses'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(Denomination, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('{0}:denomination'.format(self.group.slug), kwargs = {'denomination': self.slug})


class DenominationDetail(Monitor):
    denomination = models.OneToOneField(Denomination, models.PROTECT, related_name = 'denomination_details')
    menu_name = models.CharField(max_length = 60, null = True, verbose_name = 'Nom du menu')
    common_name = models.CharField(max_length = 60, null = True, verbose_name = 'Nom Commun')
    responsible = models.CharField(max_length = 60, null = True, verbose_name = 'Responsable')
    daily_text = models.CharField(max_length = 60, null = True, verbose_name = 'Texte du jour')
    activity_request = models.CharField(max_length = 60, null = True, verbose_name = 'Type de demande d\'activité')
    necrology_activity_request = models.CharField(max_length = 60, default = 'Demander une prière pour le défunt', null = True,
                                                  verbose_name = 'Type de demande d\'activité nécrologie')
    contact_request = models.CharField(max_length = 60, null = True, verbose_name = 'Personne à contacter')
    place_type = models.CharField(max_length = 60, null = True, verbose_name = 'Type de lieu')
    place_community = models.CharField(max_length = 255, null = True, verbose_name = 'Vie du lieu de prière')
    celebration = models.CharField(max_length = 255, null = True, verbose_name = 'Type de célébration')
    announce_type = models.CharField(max_length = 255, null = True, verbose_name = 'Type d\'annonces')

    class Meta:
        verbose_name = 'Détails dénomination'
        verbose_name_plural = 'Détails dénominations'

    def __str__(self):
        return 'Détails {}'.format(self.denomination.name)


class PlaceArchdiocese(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Archidiocèse'
        verbose_name_plural = 'Archidiocèses'
        ordering = ['id']

    def __str__(self):
        return self.name


class PlaceDiocese(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    archdiocese = models.ForeignKey(PlaceArchdiocese, models.CASCADE, verbose_name = 'Archidiocèse')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Diocèse'
        verbose_name_plural = 'Diocèses'
        ordering = ['id']

    def __str__(self):
        return self.name


class PlaceVicariate(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    diocese = models.ForeignKey(PlaceDiocese, models.CASCADE, verbose_name = 'Diocèse')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Vicariat'
        verbose_name_plural = 'Vicariats'
        ordering = ['id']

    def __str__(self):
        return self.name


class PlaceDeanery(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    vicariate = models.ForeignKey(PlaceVicariate, models.CASCADE, verbose_name = 'Vicariat')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Doyenné'
        verbose_name_plural = 'Doyennés'
        ordering = ['id']

    def __str__(self):
        return self.name


class DenominationFaith(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.PROTECT, verbose_name = 'Dénomination')
    title = models.CharField(max_length = 150, verbose_name = 'Titre')
    slug = models.SlugField(unique = True)
    content = RichTextUploadingField(default = '', blank = True, verbose_name = 'Contenu')
    image = ImageField(upload_to = upload_faith_path, blank = True, null = True, verbose_name = 'Image')
    status = models.BooleanField(null = True, choices = STATUS_CHOICES, db_index = True, verbose_name = 'Statut')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Foi'
        verbose_name_plural = 'Foi'
        ordering = ['title']

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.title)
        if self.slug != slug:
            self.slug = slug
        super(DenominationFaith, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class PlaceType(Monitor):
    id = models.AutoField(primary_key = True)
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Parent')
    group = models.ForeignKey(Group, models.CASCADE, blank = True, null = True, verbose_name = 'Groupe')
    denomination = models.ForeignKey(Denomination, models.CASCADE, blank = True, null = True, verbose_name = 'Dénomination')
    name = models.CharField(max_length = 60, verbose_name = 'Nom')
    rotate_text = models.CharField(max_length = 40, blank = True, verbose_name = 'Rotation Je Recherche')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    creation_date = models.CharField(max_length = 60, default = CREATION_DATE, choices = PLACE_TYPE_CHOICES, verbose_name = 'Date du lieu de culte')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Type de lieu de culte'
        verbose_name_plural = 'Types de lieux de culte'
        ordering = ['name']

    def __str__(self):
        return self.name


class Place(Monitor, Location, Geo):
    id = models.BigAutoField(primary_key = True)
    place_type = models.ForeignKey(PlaceType, models.SET_NULL, blank = True, null = True, verbose_name = 'Type de lieu')
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    name = models.CharField(max_length = 150, db_index = True, verbose_name = 'Nom')
    slug = models.SlugField(unique = True, blank = True, null = True)
    logo = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Logo')
    slogan = models.CharField(max_length = 400, blank = True, verbose_name = 'Slogan')
    e_shop = models.URLField(blank = True, null = True, verbose_name = 'E-boutique')
    theme = models.CharField(max_length = 255, blank = True, verbose_name = 'Thème')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    responsible = models.OneToOneField('PlaceOfficiant', models.SET_NULL, blank = True, null = True, related_name = 'officiant',
                                       verbose_name = 'Responsable')
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image')
    file_url = models.URLField(blank = True, null = True, verbose_name = 'URL du fichier')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    fax = models.CharField(max_length = 45, blank = True, verbose_name = 'Fax')
    website = models.URLField(blank = True, null = True, verbose_name = 'Site Web')
    email = models.EmailField(blank = True, verbose_name = 'Email')
    address = models.CharField(max_length = 150, blank = True, verbose_name = 'Adresse')
    opening_time = models.CharField(max_length = 150, blank = True, verbose_name = 'Horaires de réception')
    area = models.FloatField(blank = True, null = True, verbose_name = 'Surface')
    seats = models.IntegerField(blank = True, null = True, verbose_name = 'Nombre de places assises')
    creation_date = models.DateField(blank = True, null = True, verbose_name = 'Date de création')
    location_detail = models.CharField(max_length = 255, blank = True, verbose_name = 'Répère Géographique')
    organization = models.ManyToManyField('organization.Organization', through = 'organization.PlaceOrganization', blank = True,
                                          related_name = 'places', verbose_name = 'Organisations')
    # events = models.ManyToManyField('Event', related_name = 'events', blank = True, verbose_name = 'Evénements')
    # activities = models.ManyToManyField('Activity', through = 'PlaceActivity', related_name = 'activities', blank = True)
    # services = models.ManyToManyField('Service', related_name = 'services', blank = True)
    # spec = models.ManyToManyField('DenominationSpec', through = 'PlaceSpecValue', related_name = 'specs')
    status = models.BooleanField(choices = STATE_CHOICES, default = ACTIVATED)
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Lieu de prière'
        verbose_name_plural = 'Lieux de prière'
        ordering = ['name']

    def __str__(self):
        return '{0} {1}, {2}'.format(self.place_type.name, self.name, self.city.name)

    def save(self, *args, **kwargs):
        self.absolute_url = self.get_absolute_url()
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(Place, self).save(*args, **kwargs)

    @property
    def location(self):
        return '{0}{1}{2}'.format('{}'.format(self.city.name) if self.city is not None else '',
                                  ', {}'.format(self.town.name) if self.town is not None else '',
                                  ', {}'.format(self.borough.name) if self.borough is not None else ''
                                  )

    @property
    def label(self):
        return '{0}{1}'.format('{}'.format(self.name), ', {}'.format(self.location))

    @property
    def place_name(self):
        return '{0} {1}, {2}'.format(self.place_type.name, self.name, self.city)

    def get_absolute_url(self):
        return reverse('core:place', kwargs = {'group': self.denomination.group.slug, 'denomination': self.denomination.slug, 'slug': self.slug})


class PlaceChapel(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey(Denomination, models.CASCADE, verbose_name = 'Dénomination')
    parish = models.ForeignKey(Place, models.CASCADE, verbose_name = 'Paroisse')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Chapelle'
        verbose_name_plural = 'Chapelles'
        ordering = ['id']

    def __str__(self):
        return self.name


class PlaceOrganizational(Monitor):
    place = models.ForeignKey(Place, models.SET_NULL, blank = True, null = True, related_name = 'organizational', verbose_name = 'Lieu de prière')
    archdiocese = models.ForeignKey(PlaceArchdiocese, models.SET_NULL, blank = True, null = True, verbose_name = 'Archidiocèse')
    diocese = models.ForeignKey(PlaceDiocese, models.SET_NULL, blank = True, null = True, verbose_name = 'Diocèse')
    vicariate = models.ForeignKey(PlaceVicariate, models.SET_NULL, blank = True, null = True, verbose_name = 'Vicariat')
    deanery = models.ForeignKey(PlaceDeanery, models.SET_NULL, blank = True, null = True, verbose_name = 'Doyenné')

    class Meta:
        verbose_name = 'Organigramme lieu de culte'
        verbose_name_plural = 'Organigrammes lieux de culte'

    def __str__(self):
        return "{}".format(self.place.name)


class PlaceOfficiantType(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey('religion.Denomination', models.CASCADE, default = 1, verbose_name = 'Dénomination réligieuse')
    label = models.CharField(max_length = 45, blank = True, verbose_name = 'Libellé')
    codename = models.CharField(max_length = 60, unique = True, editable = False)

    class Meta:
        verbose_name = 'Type d\'officiant'
        verbose_name_plural = 'Types d\'officiants'
        ordering = ['label']

    def save(self, *args, **kwargs):
        codename = to_slug(self.label)
        if self.codename != codename:
            self.codename = codename
        super(PlaceOfficiantType, self).save(*args, **kwargs)

    def __str__(self):
        return self.label


class PlaceOfficiant(Monitor):
    id = models.BigAutoField(primary_key = True)
    place = models.ForeignKey(Place, models.SET_NULL, null = True, related_name = 'officiant', verbose_name = 'Lieu de prière')
    type = models.ForeignKey(PlaceOfficiantType, models.DO_NOTHING, blank = True, null = True, verbose_name = 'Type')
    last_name = models.CharField(max_length = 45, verbose_name = 'Nom')
    first_name = models.CharField(max_length = 15, verbose_name = 'Prénom')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    email = models.EmailField(blank = True, verbose_name = 'Adresse Email')

    class Meta:
        verbose_name = 'Officant du lieu de prière'
        verbose_name_plural = 'Officiants du lieu de prière'

    def __str__(self):
        return '{0} : {1}'.format(self.place.name, self.full_name)

    @property
    def full_name(self):
        return '{0} {1}'.format(self.last_name, self.first_name)


class PlaceCave(Monitor):
    id = models.AutoField(primary_key = True)
    place = models.ForeignKey(Place, models.SET_NULL, blank = True, null = True, verbose_name = 'Lieu de prière')
    name = models.CharField(max_length = 255, verbose_name = 'Nom')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Grotte'
        verbose_name_plural = 'Grotte'
        ordering = ['id']

    def __str__(self):
        return self.name


class Religion(models.Model):
    group = models.ForeignKey(Group, models.SET_NULL, blank = True, null = True, default = None, verbose_name = 'Groupe')
    denomination = models.ForeignKey(Denomination, models.SET_NULL, blank = True, null = True, default = None, verbose_name = 'Dénomination')
    place = models.ForeignKey(Place, models.SET_NULL, blank = True, null = True, default = None, verbose_name = 'Lieu de prière')

    class Meta:
        abstract = True
