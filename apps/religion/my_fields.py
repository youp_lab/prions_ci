from django.db import models
from django.utils.translation import ugettext as _

DAY_OF_THE_WEEK = {
    0: _(u'Lundi'),
    1: _(u'Mardi'),
    2: _(u'Mercredi'),
    3: _(u'Jeudi'),
    4: _(u'Vendredi'),
    5: _(u'Samedi'),
    6: _(u'Dimanche'),
}


class DayOfTheWeekField(models.SmallIntegerField):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = tuple(sorted(DAY_OF_THE_WEEK.items()))
        super(DayOfTheWeekField, self).__init__(*args, **kwargs)
