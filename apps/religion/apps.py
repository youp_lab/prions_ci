from django.apps import AppConfig


class ReligionConfig(AppConfig):
    name = 'apps.religion'

    def ready(self):
        import apps.religion.signals
