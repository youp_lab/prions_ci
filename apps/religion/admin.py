from django.apps import apps
from django.contrib import admin
from django.utils.translation import gettext as _

# Register your models here.
from apps.organization.models import PlaceOrganization
from apps.religion.models import *
from lib.admin_helper import admin_changelist_link, admin_link

for model in apps.get_app_config('religion').models.values():
    admin.site.register(model)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'status', 'created_at', 'created_by',)
    search_fields = ['name']
    prepopulated_fields = {'slug': ('name',)}

    @admin_changelist_link(
        'denomination',
        _('Denominations'),
        query_string = lambda c: 'group_id={}'.format(c.pk)
    )
    def denominations_link(self, denominations):
        return denominations

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)

        for afile in request.FILES.getlist('photos_multiple'):
            obj.photos.create(image = afile)


class DenominationOrganizationAdmin(admin.ModelAdmin):
    list_filter = ('group',)
    list_display = ('id', 'group', 'parent', 'name',)
    search_fields = ['name']
    autocomplete_fields = ['group', 'parent']

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class DenominationDetailsInline(admin.StackedInline):
    model = DenominationDetail


class DenominationAdmin(admin.ModelAdmin):
    list_filter = ('group',)
    list_display = ('id', 'name', 'group_link', 'description', 'status', 'created_at', 'created_by',)
    search_fields = ['name']
    autocomplete_fields = ['group', 'parent']
    prepopulated_fields = {'slug': ('name',)}
    inlines = [DenominationDetailsInline, ]

    list_select_related = ('group',)

    admin_select_related = 'group_link'

    @admin_link('group', _('Group'))
    def group_link(self, group):
        return group

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class DenominationDetailAdmin(admin.ModelAdmin):
    list_filter = ('denomination',)
    search_fields = ['name']


class DenominationFaithAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    autocomplete_fields = ['denomination']
    search_fields = ['title']
    list_display = ('id', 'title', 'status', 'created_at', 'created_by',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class PlaceArchdioceseAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PlaceDioceseAdmin(admin.ModelAdmin):
    list_filter = ('archdiocese',)
    search_fields = ['name']


class PlaceVicariateAdmin(admin.ModelAdmin):
    list_filter = ('diocese',)
    search_fields = ['name']


class PlaceDeaneryAdmin(admin.ModelAdmin):
    list_filter = ('vicariate',)
    search_fields = ['name']


class PlaceTypeAdmin(admin.ModelAdmin):
    list_filter = ('group', 'denomination',)
    autocomplete_fields = ['group', 'denomination']
    list_display = ('id', 'name', 'group', 'denomination', 'rotate_text', 'description')
    search_fields = ['name']
    prepopulated_fields = {'rotate_text': ('name',)}


class PlaceOrganizationInline(admin.StackedInline):
    model = PlaceOrganization
    extra = 1
    autocomplete_fields = ['place', 'organization', 'responsible']


class PlaceOfficiantInline(admin.StackedInline):
    model = PlaceOfficiant
    extra = 1


class PlaceAdmin(admin.ModelAdmin):
    list_filter = ('denomination', 'place_type',)
    autocomplete_fields = ['place_type', 'denomination', 'responsible', 'city', 'town', 'borough']
    list_display = ('id', 'name', 'place_type', 'denomination', 'responsible', 'status', 'city', 'town', 'borough',)
    search_fields = ['name']
    # exclude = ('activities',)
    inlines = [PlaceOrganizationInline, PlaceOfficiantInline, ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class PlaceOrganizationalAdmin(admin.ModelAdmin):
    list_filter = ('place',)
    autocomplete_fields = ['place', 'archdiocese', 'diocese', 'vicariate', 'deanery']


class PlaceTagAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PlaceCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name']
    autocomplete_fields = ['place', 'parent']


class PlaceAnnounceAdmin(admin.ModelAdmin):
    list_filter = ('place',)
    autocomplete_fields = ['place', 'category', 'tags']
    list_display = ('id', 'place', 'title', 'category', 'status',)
    search_fields = ['title']
    prepopulated_fields = {'slug': ('title',)}

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class PlaceLifeAdmin(admin.ModelAdmin):
    list_filter = ('place',)
    autocomplete_fields = ['place', 'category', 'tags']
    list_display = ('id', 'place', 'title', 'category', 'status',)
    search_fields = ['title']
    prepopulated_fields = {'slug': ('title',)}

    # inlines = [PlaceLifeImageInline, ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class PlaceOfficiantTypeAdmin(admin.ModelAdmin):
    autocomplete_fields = ['denomination']
    search_fields = ['label']


class PlaceOfficiantAdmin(admin.ModelAdmin):
    list_filter = ('place', 'type',)
    autocomplete_fields = ['place', 'type']
    list_display = ('id', 'place', 'type', 'last_name', 'first_name',)
    search_fields = ['last_name', 'first_name']

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class ActivityTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('id', 'name', 'description',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class ActivityAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ('id', 'title', 'description', 'day_of_week',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class PlaceActivityDetailsInline(admin.StackedInline):
    model = PlaceActivityDetails
    extra = 1
    search_fields = ['place_activity']
    autocomplete_fields = ['officiant']


class PlaceActivityAdmin(admin.ModelAdmin):
    list_filter = ('place', 'activity',)
    autocomplete_fields = ['place', 'activity']
    list_display = ('id', 'place', 'activity', 'start_time', 'end_time',)
    # search_fields = ['place']
    inlines = [PlaceActivityDetailsInline, ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class ReceiveRequestFileInline(admin.TabularInline):
    model = ReceiveRequestImage


class ReceiveRequestAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['place']
    search_fields = ['subject']
    list_display = ('subject', 'email', 'status', 'created_at')
    inlines = [ReceiveRequestFileInline, ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class ActivityRequestTypeAdmin(admin.ModelAdmin):
    list_filter = ('denomination',)
    autocomplete_fields = ['denomination']
    list_display = ('id', 'name', 'denomination')
    search_fields = ['name']


admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)
admin.site.unregister(Denomination)
admin.site.register(Denomination, DenominationAdmin)
admin.site.unregister(DenominationDetail)
admin.site.register(DenominationDetail, DenominationDetailAdmin)
admin.site.unregister(DenominationFaith)
admin.site.register(DenominationFaith, DenominationFaithAdmin)
admin.site.unregister(PlaceType)
admin.site.register(PlaceType, PlaceTypeAdmin)
admin.site.unregister(PlaceArchdiocese)
admin.site.register(PlaceArchdiocese, PlaceArchdioceseAdmin)
admin.site.unregister(PlaceDiocese)
admin.site.register(PlaceDiocese, PlaceDioceseAdmin)
admin.site.unregister(PlaceVicariate)
admin.site.register(PlaceVicariate, PlaceVicariateAdmin)
admin.site.unregister(PlaceDeanery)
admin.site.register(PlaceDeanery, PlaceDeaneryAdmin)
admin.site.unregister(Place)
admin.site.register(Place, PlaceAdmin)
admin.site.unregister(PlaceOrganizational)
admin.site.register(PlaceOrganizational, PlaceOrganizationalAdmin)
admin.site.unregister(PlaceCategory)
admin.site.register(PlaceCategory, PlaceCategoryAdmin)
admin.site.unregister(PlaceTag)
admin.site.register(PlaceTag, PlaceTagAdmin)
admin.site.unregister(PlaceAnnounce)
admin.site.register(PlaceAnnounce, PlaceAnnounceAdmin)
admin.site.unregister(PlaceCommunity)
admin.site.register(PlaceCommunity, PlaceLifeAdmin)
admin.site.unregister(PlaceOfficiantType)
admin.site.register(PlaceOfficiantType, PlaceOfficiantTypeAdmin)
admin.site.unregister(PlaceOfficiant)
admin.site.register(PlaceOfficiant, PlaceOfficiantAdmin)
admin.site.unregister(Activity)
admin.site.register(Activity, ActivityAdmin)
admin.site.unregister(ActivityType)
admin.site.register(ActivityType, ActivityTypeAdmin)
admin.site.unregister(PlaceActivity)
admin.site.register(PlaceActivity, PlaceActivityAdmin)
admin.site.unregister(ReceiveRequest)
admin.site.register(ReceiveRequest, ReceiveRequestAdmin)
admin.site.unregister(ActivityRequestType)
admin.site.register(ActivityRequestType, ActivityRequestTypeAdmin)
