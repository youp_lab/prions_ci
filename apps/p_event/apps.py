from django.apps import AppConfig


class PEventConfig(AppConfig):
    name = 'apps.p_event'
    verbose_name = 'Events'

    def ready(self):
        pass
