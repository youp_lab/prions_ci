from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.p_event.models import *

for model in apps.get_app_config('p_event').models.values():
    admin.site.register(model)


class EventTypeAdmin(admin.ModelAdmin):
    autocomplete_fields = ['group', 'denomination']
    search_fields = ['name']


class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    autocomplete_fields = ['group', 'denomination', 'place', 'city', 'town', 'borough', 'event_type']
    # search_fields = ['title']
    list_display = ('id', 'title', 'description', 'start_date', 'start_time',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


admin.site.unregister(Event)
admin.site.register(Event, EventAdmin)
admin.site.unregister(EventType)
admin.site.register(EventType, EventTypeAdmin)
