from django.db import models
from django.urls import reverse
from sorl.thumbnail import ImageField

from apps.location.models import Location
from apps.religion.models import Religion
from lib.middleware import get_unique_slug, Hider, Monitor, upload_path


class EventType(Religion):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 150, verbose_name = 'Titre')
    reservable = models.BooleanField(blank = True, verbose_name = 'Réservable')
    place = Hider()

    class Meta:
        verbose_name = 'Type d\'événement'
        verbose_name_plural = 'Types d\'événements'
        ordering = ['name']

    def __str__(self):
        return self.name


class Event(Monitor, Religion, Location):
    id = models.BigAutoField(primary_key = True)
    event_type = models.ForeignKey(EventType, models.PROTECT, verbose_name = 'Type')
    title = models.CharField(max_length = 255, verbose_name = 'Titre')
    slug = models.SlugField(unique = True, blank = True, null = True)
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image de l\'événement')
    description = models.TextField(default = '', blank = True, verbose_name = 'Description')
    start_date = models.DateField(blank = True, null = True, db_index = True, verbose_name = 'Date de début')
    end_date = models.DateField(db_index = True, blank = True, null = True, verbose_name = 'Date de fin')
    start_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de début')
    end_time = models.TimeField(blank = True, null = True, verbose_name = 'Heure de fin')
    duration = models.SmallIntegerField(blank = True, null = True, verbose_name = 'Durée')
    is_full_day_event = models.BooleanField(verbose_name = 'Toute la journée')
    flier = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Flyer de l\'événement')
    location_detail = models.CharField(max_length = 255, blank = True, verbose_name = 'Répère Géographique')
    is_popup = models.BooleanField(default = False, verbose_name = 'Ouvrir un popup ?')
    order_link = models.URLField(blank = True, null = True, verbose_name = 'Lien de réservation')
    price = models.CharField(max_length = 60, blank = True, verbose_name = 'Prix')
    particularity = models.TextField(blank = True, verbose_name = 'Disposition particulière')
    url = models.URLField(blank = True, null = True, verbose_name = 'Lien de l\'événement')

    # objects = EventsManager()

    class Meta:
        verbose_name = 'Evénement'
        verbose_name_plural = 'Evénements'
        ordering = ['-end_date']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.title)
        if self.slug != slug:
            self.slug = slug
        super(Event, self).save(*args, **kwargs)

    @property
    def location(self):
        return '{0}{1}{2}'.format('{}'.format(self.city.name) if self.city is not None else '',
                                  ', {}'.format(self.town.name) if self.town is not None else '',
                                  ', {}'.format(self.borough.name) if self.borough is not None else ''
                                  )

    @property
    def link(self):
        if self.url:
            return self.url
        else:
            return reverse('core:event',
                           kwargs = {
                               'group': self.group.slug,
                               'denomination': self.denomination.slug,
                               'slug': self.slug
                           })
