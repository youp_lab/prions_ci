from django.db.models.signals import pre_save
from django.dispatch import receiver
from django_cleanup.signals import cleanup_pre_delete
from sorl.thumbnail import delete


@receiver(pre_save)
def update_location(sender, instance, **kwargs):
    city = None
    town = None
    if instance.__class__.__name__ not in ['LogEntry']:
        if hasattr(sender, 'borough') and instance.borough:
            town = instance.borough.town
            city = instance.borough.town.city
        if hasattr(sender, 'town') and instance.town:
            town = instance.town
            city = instance.town.city
        if hasattr(sender, 'city') and instance.city:
            city = instance.city

        instance.city = city
        instance.town = town


def sorl_delete(**kwargs):
    delete(kwargs['file'])


cleanup_pre_delete.connect(sorl_delete)
