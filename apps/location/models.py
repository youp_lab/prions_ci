from django.db import models

from lib.middleware import Monitor


class City(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 255, verbose_name = 'Nom')

    class Meta:
        app_label = 'location'
        verbose_name = 'Ville'
        verbose_name_plural = 'Villes'
        ordering = ['name']

    def __str__(self):
        return self.name


class Town(Monitor):
    id = models.BigAutoField(primary_key = True)
    city = models.ForeignKey(City, models.SET_NULL, blank = True, null = True, verbose_name = 'Ville')
    name = models.CharField(max_length = 70, verbose_name = 'Nom')

    class Meta:
        app_label = 'location'
        verbose_name = 'Commune'
        verbose_name_plural = 'Communes'
        ordering = ['name']

    def __str__(self):
        return self.name


class Borough(Monitor):
    id = models.BigAutoField(primary_key = True)
    town = models.ForeignKey(Town, models.SET_NULL, blank = True, null = True, verbose_name = 'Commune')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')

    class Meta:
        app_label = 'location'
        verbose_name = 'Quartier'
        verbose_name_plural = 'Quartiers'
        ordering = ['name']

    def __str__(self):
        return self.name


class Location(models.Model):
    city = models.ForeignKey(City, models.SET_NULL, blank = True, null = True, verbose_name = 'Ville')
    town = models.ForeignKey(Town, models.SET_NULL, blank = True, null = True, verbose_name = 'Commune')
    """
    town = ChainedForeignKey(
        Town,
        chained_field = 'city',
        chained_model_field = 'city',
        show_all = False,
        auto_choose = True,
        sort = True,
        blank = True,
        null = True,
        verbose_name = 'Commune')
    """
    borough = models.ForeignKey(Borough, models.SET_NULL, blank = True, null = True, verbose_name = 'Quartier')
    """
    borough = ChainedForeignKey(
        Borough,
        chained_field = 'town',
        chained_model_field = 'town',
        show_all = False,
        auto_choose = True,
        sort = True,
        blank = True,
        null = True,
        verbose_name = 'Quartier')
    """

    class Meta:
        app_label = 'location'
        abstract = True


class Geo(models.Model):
    lat = models.DecimalField(max_digits = 20, decimal_places = 15, blank = True, null = True, verbose_name = 'Latitude')
    lon = models.DecimalField(max_digits = 20, decimal_places = 15, blank = True, null = True, verbose_name = 'Longitude')
    zoom = models.SmallIntegerField(default = 18, editable = False)
    map = models.TextField(blank = True, verbose_name = 'Carte')

    class Meta:
        app_label = 'location'
        abstract = True
