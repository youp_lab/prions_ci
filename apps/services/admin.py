from attachments.admin import AttachmentInlines
from django.contrib import admin

# Register your models here.
from apps.services.models import *


class ServiceTypeAdmin(admin.ModelAdmin):
    autocomplete_fields = ['group', 'denomination', 'place']
    search_fields = ['name']
    list_display = ('id', 'name', 'group', 'denomination', 'rotate_text',)
    prepopulated_fields = {'rotate_text': ('name',)}


class ServiceDetailsInline(admin.StackedInline):
    model = ServiceDetails


class ServiceAdmin(admin.ModelAdmin):
    autocomplete_fields = ['group', 'denomination', 'place', 'city', 'town', 'borough', 'service_type']
    search_fields = ['name']
    list_filter = ('service_type',)
    list_display = ('id', 'service_type', 'name', 'description', 'group', 'denomination', 'place',)
    prepopulated_fields = {'slug': ('name',)}
    inlines = [AttachmentInlines, ServiceDetailsInline, ]


admin.site.register(Service, ServiceAdmin)
admin.site.register(ServiceType, ServiceTypeAdmin)
