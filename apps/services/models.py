from django.db import models
from sorl.thumbnail import ImageField

from apps.location.models import Location, Geo
from apps.religion.models import Religion
from lib.middleware import get_unique_slug, upload_path, Monitor


class ServiceType(Religion):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 150, verbose_name = 'Titre')
    rotate_text = models.CharField(max_length = 40, blank = True, verbose_name = 'Rotation Je Recherche')

    class Meta:
        verbose_name = 'Type de service'
        verbose_name_plural = 'Types des services'
        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)


class Service(Monitor, Religion, Location, Geo):
    id = models.BigAutoField(primary_key = True)
    service_type = models.ForeignKey(ServiceType, models.PROTECT, verbose_name = 'Type')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    description = models.CharField(max_length = 255, blank = True, verbose_name = 'Description')
    slug = models.SlugField(unique = True, blank = True, null = True)
    responsible = models.ForeignKey('person.Responsible', models.SET_NULL, blank = True, null = True, verbose_name = 'Responsable')
    # is_popup = models.BooleanField(default = False, verbose_name = 'Ouvrir un popup ?')
    url = models.URLField(blank = True, null = True, verbose_name = 'Lien')
    image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image du service')
    location_detail = models.CharField(max_length = 255, blank = True, verbose_name = 'Répère Géographique')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    fax = models.CharField(max_length = 45, blank = True, verbose_name = 'Fax')
    website = models.CharField(max_length = 70, blank = True, verbose_name = 'Site Internet')
    email = models.EmailField(blank = True, verbose_name = 'Email')
    address = models.CharField(max_length = 150, blank = True, verbose_name = 'Adresse')

    # facebook = models.CharField(max_length = 255, blank = True, verbose_name = 'Facebook')
    # twitter = models.CharField(max_length = 255, blank = True, verbose_name = 'Twitter')
    # instagram = models.CharField(max_length = 255, blank = True, verbose_name = 'Instagram')
    # youtube = models.CharField(max_length = 255, blank = True, verbose_name = 'YouTube')

    class Meta:
        verbose_name = 'Service, Association, etc'
        verbose_name_plural = 'Services, Associations, etc'
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)

    def save(self, *args, **kwargs):
        slug = get_unique_slug(self, self.name)
        if self.slug != slug:
            self.slug = slug
        super(Service, self).save(*args, **kwargs)

    @property
    def location(self):
        return '{0}{1}{2}'.format('{}'.format(self.city.name) if self.city is not None else '',
                                  ', {}'.format(self.town.name) if self.town is not None else '',
                                  ', {}'.format(self.borough.name) if self.borough is not None else ''
                                  )


class ServiceResponsible(Monitor):
    service = models.ForeignKey(Service, models.CASCADE, verbose_name = 'Service')
    organization = models.ForeignKey('religion.Organization', models.CASCADE, verbose_name = 'Organisation')
    responsible_last_name = models.CharField(max_length = 45, verbose_name = 'Nom du responsable')
    responsible_first_name = models.CharField(max_length = 15, verbose_name = 'Prénom du responsable')
    responsible_contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique du responsable')
    responsible_email = models.EmailField(blank = True, verbose_name = 'Adresse Email du responsable')

    class Meta:
        abstract = True
        unique_together = (('place', 'organization'),)
        verbose_name = 'Organisation et Responsable de lieu de culte'
        verbose_name_plural = 'Organisations et Responsables des lieux de culte'

    def __str__(self):
        return '{0} : {1} - {2} {3}'.format(self.service, self.organization, self.responsible_first_name, self.responsible_last_name)

    def responsible_full_name(self):
        return '{0} {1}'.format(self.responsible_last_name, self.responsible_first_name)


class ServiceDetails(Monitor):
    service = models.ForeignKey(Service, models.PROTECT, related_name = 'servicedetails')
    label = models.CharField(max_length = 150, verbose_name = 'Paramètre')
    value = models.CharField(max_length = 150, blank = True, verbose_name = 'Valeur')

    class Meta:
        verbose_name = 'Info supplémentaire'
        verbose_name_plural = 'Infos supplémentaires'

    def __str__(self):
        return 'Service : {} - {} => {}'.format(self.service, self.label, self.value)
