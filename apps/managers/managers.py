from django.core.exceptions import FieldError
from django.db import models

from apps.core.enums import PUBLISHED


class GlobalQuerySet(models.QuerySet):
    def published(self, status = PUBLISHED):
        """Returns published objects"""
        try:
            return self.filter(status = status)
        except FieldError:
            return self

    def visible(self, visible = PUBLISHED):
        """Returns displayed objects"""
        return self.filter(visible = visible)

    def objects_by_group(self, group):
        """Returns all objects by group"""
        return self.filter(group = group)

    def objects_by_denomination(self, denomination):
        """Returns objects by denomination"""
        return self.filter(denomination = denomination)

    def objects_by_place(self, place):
        """Returns objects by place"""
        return self.filter(place = place)

    def get_from_universe(self, universe, slug):
        """Get objects by universe"""
        if universe is not None:
            if universe == 'group':
                return self.objects_by_group(slug)
            if universe == 'denomination':
                return self.objects_by_denomination(slug)
            if universe == 'place':
                return self.objects_by_place(slug)
        else:
            return None


class PublishingManager(models.Manager):
    """Publishing manager for models"""

    def get_queryset(self):
        """Returns QuerySet"""
        return GlobalQuerySet(self.model, using = self._db)

    def published(self, active = PUBLISHED):
        """Returns the published models"""
        return self.filter(status = active)
