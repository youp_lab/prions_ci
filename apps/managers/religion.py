from apps.managers import GlobalQuerySet


class ReligionQuerySet(GlobalQuerySet):

    def posted_by_group(self, group):
        """Returns all posted articles"""
        return self.published().objects_by_group(group)

    def posted_by_denomination(self, denomination):
        """Returns posted articles by denomination"""
        return self.published().objects_by_denomination(denomination)

    def posted_by_place(self, place):
        """Returns posted articles by place"""
        return self.published().objects_by_place(place)
