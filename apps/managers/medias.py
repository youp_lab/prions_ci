from django.db import models

from apps.managers import PublishingManager


class MediaQuerySet(models.QuerySet, PublishingManager):
    def media_type(self, media_type = 'video'):
        """Returns posted medias by type"""
        return self.filter(media_type__type = media_type)


class MediaManager(PublishingManager):
    def get_queryset(self):
        return MediaQuerySet(self.model, using = self._db)

    def media_type(self, media_type = 'video'):
        return self.get_queryset().media_type(media_type = media_type)
