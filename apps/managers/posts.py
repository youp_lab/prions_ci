from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F, Count, Q

from apps.managers import GlobalQuerySet, PublishingManager
from lib.config import POSTS_COUNT
from apps.core.enums import PUBLISHED


class PostsQuerySet(GlobalQuerySet):
    def published_posts(self):
        """Returns the published models"""
        is_published = self.published().filter(published_on__lte = datetime.now())
        is_published = is_published.filter(Q(end_publication__isnull = True) | Q(end_publication__gt = datetime.now()))
        return is_published.order_by('-published_on')

    def posted_by_group(self, group):
        """Returns all posted articles"""
        return self.published_posts().objects_by_group(group)

    def posted_by_denomination(self, denomination):
        """Returns posted articles by denomination"""
        return self.published_posts().objects_by_denomination(denomination)

    def posted_by_place(self, place):
        """Returns posted articles by place"""
        return self.published_posts().objects_by_place(place)

    def recent(self, universe, value, count = PUBLISHED):
        """Returns all recent posts """
        return self.get_from_universe(universe, value)[:count]

    def recent_without_stick(self, universe, value, count = POSTS_COUNT):
        """Returns recent without sticky posts """
        return self.get_from_universe(universe, value).filter(stick = False)[:count]

    def sticky_post(self, universe, slug):
        """Returns sticky post"""
        try:
            sticky = self.get_from_universe(universe, slug).filter(stick = True)
        except ObjectDoesNotExist:
            sticky = None

        return sticky

    def related_categories(self, denomination, count = POSTS_COUNT):
        return self.posted_by_denomination(denomination) \
                   .values(category_name = F('category__name'), category_slug = F('category__slug')) \
                   .annotate(num_posts = Count('title')) \
                   .order_by('category_name')[:count]

    def similar_posts(self, denomination, post, count = POSTS_COUNT):
        post_tags_ids = post.tags.values_list('id', flat = True)
        similar_posts = self.posted_by_denomination(denomination).filter(tags__in = post_tags_ids).exclude(id = post.id)
        return similar_posts.annotate(same_tags = Count('tags')).order_by('-same_tags', '-created_at')[:count]


class PostsManager(PublishingManager):
    def get_queryset(self):
        return PostsQuerySet(self.model, using = self._db)

    def posted_by_group(self, group):
        """Returns all posted articles"""
        return self.get_queryset().objects_by_group(group)

    def posted_by_denomination(self, denomination):
        """Returns posted articles by denomination"""
        return self.get_queryset().objects_by_denomination(denomination)

    def posted_by_place(self, place):
        """Returns posted articles by place"""
        return self.get_queryset().objects_by_place(place)

    def recent(self, universe, value, count = POSTS_COUNT):
        """Returns all recent posts """
        return self.get_queryset().recent(universe, value, count)

    def recent_without_stick(self, universe, value, count = POSTS_COUNT):
        """Returns recent without sticky posts """
        return self.get_queryset().recent_without_stick(universe, value, count)

    def sticky_post(self, universe, value):
        """Returns sticky post """
        return self.get_queryset().sticky_post(universe, value)

    def related_categories(self, denomination, count = POSTS_COUNT):
        return self.get_queryset().related_categories(denomination, count)

    def similar_posts(self, denomination, post, count = POSTS_COUNT):
        return self.get_queryset().similar_posts(denomination, post, count)
