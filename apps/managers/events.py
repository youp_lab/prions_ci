from datetime import datetime

from django.db import models

from apps.core.enums import PUBLISHED
from lib.middleware import sql_query


class PublishingManager(models.Manager):
    """Publishing manager for models"""

    def events(self, denomination):
        """Returns the published models"""
        return self.filter(placeevent__place__denomination = denomination)

    """Publishing manager for models"""

    def published(self, active = PUBLISHED):
        """Returns the published models"""
        return self.filter(status = active)

    """Publishing manager for posts"""

    def published_posts(self):
        """Returns the published models"""
        return self.published().filter(published_on__lte = datetime.now()).order_by('-published_on')


class EventsManager(PublishingManager):
    @staticmethod
    def get_schedule(denomination, city_id, town_id = None, borough_id = None, event_type = 'messe',
                     recurring_type = 'weekly', day_of_week = 7,
                     schedule_len = 1):
        if schedule_len is 0:
            schedule = ';'
        else:
            schedule = ' LIMIT {};'.format(int(schedule_len))

        if town_id is not None and town_id != "":
            add_town = 'INNER JOIN `town` ON `place`.town_id = `town`.id AND `place`.`town_id` = %s'
        else:
            add_town = 'LEFT JOIN `town` ON `place`.town_id = `town`.id OR `place`.`town_id` = %s'

        if borough_id is not None and borough_id != "":
            add_borough = 'INNER JOIN `borough` ON `place`.borough_id = `borough`.id AND `place`.`borough_id` = %s'
        else:
            add_borough = 'LEFT JOIN `borough` ON `place`.borough_id = `borough`.id OR `place`.`borough_id` = %s'

        sql = '''
                SELECT `event`.`id`                                           id,
                       `event`.`parent_id`                                    event_parent_id,
                       `event_type`.`label`                                   event_type_label,
                       `event_type`.`description`                             event_type_description,
                       `event`.`title`                                        event_title,
                       `event`.`description`                                  event_description,
                       `event`.`start_date`                                   event_start_date,
                       `event`.`end_date`                                     event_end_date,
                       `event`.`start_time`                                   event_start_time,
                       `event`.`end_time`                                     event_end_time,
                       `event`.`is_full_day_event`                            event_is_full_day_event,
                       `event`.`is_recurring`                                 event_is_recurring,
                       `event`.`created_by_id`                                event_created_by_id,
                       `event`.`updated_by_id`                                event_updated_by_id,
                       `event`.`created_at`                                   event_created_at,
                       `event`.`updated_at`                                   event_updated_at,
                       `recurring_pattern`.`separation_count`                 pattern_separation_count,
                       `recurring_pattern`.`max_num_of_occurences`            pattern_max_num_of_occurences,
                       `recurring_pattern`.`day_of_week`                      pattern_day_of_week,
                       `recurring_pattern`.`week_of_month`                    pattern_week_of_month,
                       `recurring_pattern`.`day_of_month`                     pattern_day_of_month,
                       `recurring_pattern`.`month_of_year`                    pattern_month_of_year,
                       `place`.id                                             place_id,
                       `place_type`.label                                     place_type_label,
                       `place`.name                                           place_name,
                       `place`.description                                    place_description,
                       `place`.slug                                           place_slug,
                       `place`.image                                          place_image,
                       CONCAT(city.name, ', ', town.name, ', ', borough.name) place_location,
                       `place_detail`.contact                                 place_contact,
                       `place_detail`.website                                 place_website,
                       `place_detail`.address                                 place_address,
                       `place_detail`.map                                     place_map,
                       `place_detail`.area                                    place_area,
                       `place_detail`.seats                                   place_seats,
                       `place_detail`.location_detail                         place_location_detail
                FROM `event`
                       LEFT JOIN `event_type` ON (`event`.`event_type_id` = `event_type`.`id`)
                       INNER JOIN `recurring_pattern` ON (`event`.`id` = `recurring_pattern`.`event_id`)
                       INNER JOIN `recurring_type` ON (`recurring_pattern`.`recurring_type_id` = `recurring_type`.`id`)
                       INNER JOIN `place_event` ON (`event`.`id` = place_event.`event_id`)
                       INNER JOIN `place` ON (`place_event`.`place_id` = `place`.`id`)
                            AND (`place`.`denomination_id` = %s)
                            AND (`place`.`city_id` = %s)
                       INNER JOIN `place_detail` ON `place`.id = `place_detail`.place_id
                       INNER JOIN `place_type` ON (`place`.`place_type_id` = `place_type`.`id`)
                       INNER JOIN `city` ON `place`.city_id = `city`.id
                       ''' + add_town + '''
                       ''' + add_borough + '''
                WHERE `event_type`.`label` = %s
                  AND `recurring_type`.`label` = %s
                  AND recurring_pattern.`day_of_week` = %s
                ORDER BY recurring_pattern.`day_of_week`, `event`.`start_time` ASC'''
        sql += schedule

        return sql_query(sql, [denomination, city_id, town_id, borough_id, event_type, recurring_type, day_of_week])
