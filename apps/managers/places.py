from django.db import models

from apps.managers import PublishingManager
from lib.middleware import sql_query


class PlacesQuerySet(models.QuerySet):
    def places_by_group(self, group):
        """Returns all places"""
        return self.filter(group = group)

    def places_by_denomination(self, denomination):
        """Returns places by denomination"""
        return self.filter(denomination = denomination)


class PlacesManager(PublishingManager):
    def get_queryset(self):
        return PlacesQuerySet(self.model, using = self._db)

    def places_by_group(self, group):
        """Returns all places"""
        return self.get_queryset().places_by_group(group)

    def places_by_denomination(self, denomination):
        """Returns places by denomination"""
        return self.get_queryset().places_by_denomination(denomination)

    def get_places_list(self, denomination, city, place_type = 0):
        if place_type == 0:
            added_query = 'LEFT JOIN place_type ON place.place_type_id = place_type.id'
        else:
            try:
                place_type = int(place_type)
                added_query = 'INNER JOIN place_type ON place.place_type_id = place_type.id AND place_type.id = {}'.format(
                    place_type)
            except ValueError:
                added_query = 'LEFT JOIN place_type ON place.place_type_id = place_type.id'

        return self.raw('''
                    SELECT
                      place.id                                                                           id,
                      place_type.label                                                                   place_type_label,
                      place.name                                                                         place_name,
                      place.image                                                                        place_image,
                      place.slug                                                                         place_slug,
                      CONCAT(city.name, ', ', town.name, ', ', borough.name)                             place_location,
                      place.location_detail                                                              place_location_details,
                      place.contact                                                                      place_contact,
                      place.address                                                                      place_address,
                      place.seats                                                                        place_seats,
                      CONCAT(place.responsible_lastname, ' ', place.responsible_firstname)               place_responsible
                    FROM place
                      ''' + added_query + '''
                      LEFT JOIN city ON place.city_id = city.id
                      LEFT JOIN town ON place.town_id = town.id
                      LEFT JOIN borough ON place.borough_id = borough.id
                    WHERE place.status = TRUE AND place.denomination_id = %s AND place.city_id = %s
                ''', [denomination, city])

    @staticmethod
    def get_place(place_id = None):
        sql = '''
                SELECT
                  place.id                           id,
                  place_type.label                   place_type_label,
                  place.name                         place_name,
                  place.description                  place_description,
                  place.slug                         place_slug,
                  place.image                        place_image,
                  city.name                          place_city,
                  town.name                          place_town,
                  borough.name                       place_borough,
                  place_detail.responsible_title     place_responsible_title,
                  place_detail.responsible_lastname  place_responsible_lastname,
                  place_detail.responsible_firstname place_responsible_firstname,
                  place_detail.responsible_contact   place_responsible_contact,
                  place_detail.contact               place_contact,
                  place_detail.fax                   place_fax,
                  place_detail.website               place_website,
                  place_detail.email                 place_email,
                  place_detail.address               place_address,
                  place_detail.lat                   place_lat,
                  place_detail.lon                   place_long,
                  place_detail.map                   place_map,
                  place_detail.zoom                  place_map_zoom,
                  place_detail.area                  place_area,
                  place_detail.seats                 place_seats,
                  place_detail.creation_date         place_creation_date,
                  place_detail.location_detail       place_location_detail,
                  organization_type.id               place_organization_type_id,
                  organization_type.label            place_organization_type_label,
                  organization_type.description      place_organization_type_description,
                  organization.label                 place_organization_label,
                  organization.parent_id             place_organization_parent_id,
                  organization.description           place_organization_description,
                  responsible_type.label             place_organization_responsible_type_label,
                  responsible_type.name              place_organization_responsible_type_name,
                  responsible.lastname               place_organization_responsible_lastname,
                  responsible.firstname              place_organization_responsible_firstname,
                  responsible.contact                place_organization_responsible_contact,
                  responsible.website                place_organization_responsible_website,
                  responsible.email                  place_organization_responsible_email
                FROM place
                  LEFT JOIN place_type ON place.place_type_id = place_type.id
                  LEFT JOIN city ON place.city_id = city.id
                  LEFT JOIN town ON place.town_id = town.id
                  LEFT JOIN borough ON place.borough_id = borough.id
                  LEFT JOIN place_detail ON place.id = place_detail.place_id
                  LEFT JOIN place_organization ON place.id = place_organization.place_id
                  LEFT JOIN organization ON place_organization.organization_id = organization.id
                  LEFT JOIN organization_type ON organization.organization_type_id = organization_type.id
                  LEFT JOIN responsible ON place_organization.responsible_id = responsible.id
                  LEFT JOIN responsible_type ON responsible.responsible_type_id = responsible_type.id
                WHERE place.id = %s AND place.status = TRUE;
                '''
        return sql_query(sql, [place_id])
