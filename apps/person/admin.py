from django.contrib import admin

# Register your models here.
from apps.person.models import *


class ResponsibleTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'codename': ('name',)}
    list_filter = ('denomination',)
    search_fields = ['name']


class ResponsibleAdmin(admin.ModelAdmin):
    search_fields = ['last_name', 'first_name']
    autocomplete_fields = ['responsible_type']


admin.site.register(ResponsibleType, ResponsibleTypeAdmin)
admin.site.register(Responsible, ResponsibleAdmin)
