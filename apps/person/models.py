from django.db import models

from lib.middleware import Monitor, to_slug


class ResponsibleType(Monitor):
    id = models.AutoField(primary_key = True)
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Responsable Parent')
    denomination = models.ForeignKey('religion.Denomination', models.CASCADE, blank = True, null = True, verbose_name = 'Dénomination réligieuse')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    codename = models.CharField(max_length = 150, verbose_name = 'Libellé')

    class Meta:
        verbose_name = 'Type de responsable'
        verbose_name_plural = 'Types de responsables'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        codename = to_slug(self.name)
        if self.codename != codename:
            self.codename = codename
        super(ResponsibleType, self).save(*args, **kwargs)


class Responsible(Monitor):
    id = models.BigAutoField(primary_key = True)
    responsible_type = models.ForeignKey(ResponsibleType, models.SET_NULL, blank = True, null = True, verbose_name = 'Type de responsable')
    last_name = models.CharField(max_length = 45, verbose_name = 'Nom')
    first_name = models.CharField(max_length = 15, verbose_name = 'Prénom')
    contact = models.CharField(max_length = 45, blank = True, verbose_name = 'Contact Téléphonique')
    email = models.EmailField(blank = True, verbose_name = 'Adresse Email')

    class Meta:
        verbose_name = 'Responsable'
        verbose_name_plural = 'Responsables'
        ordering = ['last_name', 'first_name', ]

    def __str__(self):
        return '{} {}'.format(self.last_name, self.first_name)

    @property
    def full_name(self):
        return '{0} {1}'.format(self.last_name, self.first_name)
