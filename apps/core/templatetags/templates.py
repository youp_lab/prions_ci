import json

from django import template
from django.db.models import Q
from django.forms import model_to_dict
from django.http import Http404

from apps.core.enums import ACTIVATED
from apps.p_event.models import EventType, Event
from apps.posts.models import Necrology
from apps.religion.models import PlaceType, Place
from apps.services.models import ServiceType
from lib.config import django_logger, POSTS_BY_SLIDER
from lib.middleware import JSONEncoder, get_fields_and_properties

register = template.Library()


@register.inclusion_tag('core/denomination/partials/widgets/i_wish.html', takes_context = True)
def i_wish(context):
    group = context.get('group')
    denomination = context.get('denomination')
    page_name = context.get('page_name')
    place_types = PlaceType.objects.filter(Q(denomination = denomination) | Q(group = group) | Q(group = None))
    place_type_list = [x.rotate_text for x in place_types if len(x.rotate_text) > 0]
    service_types = ServiceType.objects.filter(Q(denomination = denomination) | Q(group = group) | Q(group = None))
    service_type_list = [x.rotate_text for x in service_types if len(x.rotate_text) > 0]

    return {
        'group': group,
        'denomination': denomination,
        'page_name': page_name,
        'rotate_list': place_type_list + service_type_list,
        'place_types': PlaceType.objects.filter(Q(denomination = denomination) | Q(group = group) | Q(denomination__isnull = True) | Q(group__isnull = True)),
        'event_types': EventType.objects.filter(group = group),
        'service_types': ServiceType.objects.filter((Q(denomination = denomination) | Q(denomination__isnull = True)) | Q(group = group))
    }


@register.inclusion_tag('core/denomination/partials/widgets/calendar.html', takes_context = True)
def calendar(context):
    group = context.get('group')
    denomination = context.get('denomination')
    place = context.get("place", None)
    page_name = context.get('page_name')
    calendar_events = []
    try:
        if place is not None:
            place = Place.objects.get(pk = place.pk)
            # events = Event.objects.filter(Q(group = group) | Q(place = place))
            events = Event.objects.filter(place = place)
        else:
            events = Event.objects.filter(Q(group = group) | Q(denomination = denomination))
        if events.count() > 0:
            data = [get_fields_and_properties(Event, x, ['event_type', 'title', 'start_date']) for x in events]
            calendar_events = json.dumps(data, cls = JSONEncoder)
    except Exception as e:
        django_logger.error(e)
        raise Http404

    return {
        'group': group,
        'denomination': denomination,
        'page_name': page_name,
        'place': place,
        'calendar_events': calendar_events
    }


@register.inclusion_tag('core/denomination/partials/widgets/necrology.html', takes_context = True)
def necrology(context):
    group = context.get('group')
    denomination = context.get('denomination')
    page_name = context.get('page_name')
    return {
        'group': group,
        'denomination': denomination,
        'page_name': page_name,
        'necrology_list': Necrology.objects.filter(status = ACTIVATED).order_by("-created_at")[:POSTS_BY_SLIDER],
    }


@register.inclusion_tag('core/denomination/directory/search_results/result_line.html')
def display_field(field, field_name, label_name = ""):
    if field:
        name, value = field._meta.get_field(field_name).verbose_name, getattr(field, field_name)
        if value is None or value == "":
            value = "à venir"
        return {
            'field': label_name if label_name else name,
            'value': value
        }


@register.inclusion_tag('administrator/partials/result_line.html')
def admin_display_field(field, field_name, label_name = ""):
    if field:
        name, value = field._meta.get_field(field_name).verbose_name, getattr(field, field_name)
        if value is None or value == "":
            value = "à venir"
        return {
            'field': label_name if label_name else name,
            'value': value
        }
