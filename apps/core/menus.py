from django.contrib.sites.models import Site

domain = Site.objects.get_current().domain

MENUS = {
    'MENU_CATHOLIQUE': [
        {
            "name": "News",
            "url": "#",
            "root": True,
            "submenu": [
                {
                    "name": "Actualité",
                    "url": "{}/christianisme/catholique/articles".format(domain),
                },
                {
                    "name": "Announces",
                    "url": "{}/christianisme/catholique/annonces".format(domain),
                }
            ]
        },
        {
            "name": "Foi Catholique",
            "url": "{}/christianisme/catholique/foi".format(domain)
        },
        {
            "name": "Je Recherche",
            "url": "{}/christianisme/catholique/je-recherche".format(domain)
        },
        {
            "name": "Outils",
            "url": "#",
            "submenu": [
                # {
                #     "name": "E-Book",
                #     "url": "{}/christianisme/catholique/ebook".format(domain),
                # },
                {
                    "name": "Pèlerinage & Retraite",
                    "url": "{}/christianisme/catholique/pelerinage-retraite".format(domain),
                },
                {
                    "name": "Demander une célébration",
                    "url": "{}/christianisme/catholique/faire-une-demande".format(domain),
                },
                {
                    "name": "Contacter un prêtre",
                    "url": "{}/christianisme/catholique/contacter".format(domain),
                },
            ]
        },
        {
            "name": "Donner / Recevoir",
            "url": "#",
            "submenu": [
                {
                    "name": "Faire un don",
                    "url": "{}/christianisme/catholique/demandes".format(domain),
                },
                {
                    "name": "Solliciter une aide",
                    "url": "{}/christianisme/catholique/solliciter-une-aide".format(domain),
                },
            ]
        },
        {
            "name": "Nécrologie",
            "url": "{}/christianisme/catholique/necrologie".format(domain)
        },
        {
            "name": "Témoignages",
            "url": "{}/christianisme/catholique/temoignages".format(domain)
        },
        {
            "name": "Médias",
            "url": "{}/christianisme/catholique/mediatheque".format(domain)
            # "submenu": [
            #     {
            #         "name": "Audios",
            #         "url": "{}/christianisme/catholique/mediatheque/audio".format(domain),
            #     },
            #     {
            #         "name": "Images",
            #         "url": "{}/christianisme/catholique/mediatheque/image".format(domain),
            #     },
            #     {
            #         "name": "Vidéos",
            #         "url": "{}/christianisme/catholique/mediatheque/video".format(domain),
            #     },
            #     {
            #         "name": "Documents",
            #         "url": "{}/christianisme/catholique/mediatheque/document".format(domain),
            #     },
            # ]
        },
        {
            "name": "E-shop",
            "url": "https://eboutique.prions.ci/",
            "target": "_blank"
        }
    ],
    'ISLAM_MENU': [
        {
            "name": "News",
            "url": "#",
            "root": True,
            "submenu": [
                {
                    "name": "Actualité",
                    "url": "articles",
                },
                {
                    "name": "Announces",
                    "url": "annonces",
                }
            ]
        },
        {
            "name": "Foi Islamique",
            "url": "foi"
        },
        {
            "name": "Je Recherche",
            "url": "je-recherche"
        },
        {
            "name": "Outils",
            "url": "#",
            "submenu": [
                {
                    "name": "E-Book",
                    "url": "e-book",
                },
                {
                    "name": "Pélérinage & Retraite",
                    "url": "pelerinage-retraite",
                },
                {
                    "name": "Contacter un Imam",
                    "url": "contacter",
                },
            ]
        },
        {
            "name": "Donner / Recevoir",
            "url": "#",
            "submenu": [
                {
                    "name": "Faire un don",
                    "url": "demandes",
                },
                {
                    "name": "Solliciter une aide",
                    "url": "solliciter-une-aide",
                },
                {
                    "name": "La Zakat",
                    "url": "#",
                },
                {
                    "name": "L'aumône",
                    "url": "#",
                },
            ]
        },
        {
            "name": "Nécrologie",
            "url": "necrologie"
        },
        {
            "name": "Témoignages",
            "url": "temoignages"
        },
        {
            "name": "Médias",
            "url": "#",
            "submenu": [
                {
                    "name": "Sermons",
                    "url": "#",
                },
                {
                    "name": "Ecouter le Coran",
                    "url": "#",
                },
                {
                    "name": "Lire le Coran",
                    "url": "#",
                },
            ]
        },
        {
            "name": "E-shop",
            "url": "https://eboutique.prions.ci/",
            "target": "_blank"
        }
    ]
}
