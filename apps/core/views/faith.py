from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView

from lib.config import django_logger
from apps.religion.models import Group, Denomination, DenominationFaith


class DenominationFaithListView(ListView):
    model = DenominationFaith
    template_name = 'core/denomination/faith/faith_list.html'
    context_object_name = 'faith_list'
    group = None
    denomination = None

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return DenominationFaith.objects.posted_by_denomination(self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'faith'
        context['page_name'] = 'faith_list'
        context['page_title'] = 'Foi {}'.format(self.denomination.denomination_details.common_name)

        return context


class DenominationFaithView(DetailView):
    model = DenominationFaith
    template_name = 'core/denomination/faith/faith.html'
    context_object_name = "faith"
    group = None
    denomination = None

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        try:
            return DenominationFaith.objects.posted_by_denomination(denomination = self.denomination).get(slug = self.kwargs.get('slug'))
        except DenominationFaith.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'faith'
        context['page_name'] = 'faith'
        context['page_title'] = self.object.title

        return context
