from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
# from hitcount.views import HitCountDetailView
from apps.posts.models import MediaType, Gallery
from apps.religion.models import Group, Denomination
from lib.config import POSTS_PAGINATE_BY, django_logger


class GalleryListView(ListView):
    model = MediaType
    template_name = 'core/denomination/media/media_list.html'
    context_object_name = 'media_type_list'
    group = None
    denomination = None
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return MediaType.objects.all().exclude(codename = 'ebook')

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'medias'
        context['page_name'] = 'media_list'
        context['page_title'] = 'Mediathèque'
        context['media_list'] = Gallery.objects.filter((Q(denomination = self.denomination) | Q(denomination__isnull = True)),
                                                       group = self.group).exclude(type__codename = 'ebook')

        return context


class GalleryView(ListView):
    model = MediaType
    # template_name = 'core/denomination/media/media_list.html'
    group = None
    denomination = None
    type = None
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        self.type = get_object_or_404(MediaType, type = self.kwargs['type'])
        return Gallery.objects.filter(denomination = self.denomination, media_type = self.type)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        media_type = self.kwargs.get('type')
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'medias'
        context['page_name'] = 'media'
        context['page_title'] = 'Mediathèque {}'.format(media_type)
        context['media_type'] = media_type

        return context


class GalleryDetailView(DetailView):
    model = Gallery
    #count_hit = True
    template_name = 'core/denomination/media/media.html'
    group = None
    denomination = None
    context_object_name = 'media'

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        try:
            return Gallery.objects.filter(denomination = self.denomination, pk = self.kwargs.get('pk'))
        except Gallery.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'medias'
        context['page_name'] = 'media'
        context['page_title'] = self.object.name

        try:
            context['next_media'] = Gallery.get_next_by_created_at(self.object, denomination = self.denomination)
        except Gallery.DoesNotExist:
            context['next_media'] = None

        try:
            context['previous_media'] = Gallery.get_previous_by_created_at(self.object, denomination = self.denomination)
        except Gallery.DoesNotExist:
            context['previous_media'] = None

        return context
