import itertools
from datetime import datetime
from gettext import gettext as _
from operator import itemgetter

from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.views import View
from django.views.generic import DetailView, TemplateView, ListView

from apps.p_event.models import Event, EventType
from apps.religion.models import Group, Denomination, Place, PlaceType, ActivityLanguage, PlaceActivity, PlaceAnnounce, PlaceCommunity
from apps.religion.my_fields import DAY_OF_THE_WEEK
from apps.services.models import Service, ServiceType
from lib.config import django_logger, RECENT_POSTS_COUNT
from lib.middleware import str_to_json, get_class_instance, get_geo_loc, date_reverse, get_occurrences, convert, extract_values


class PlaceView(DetailView):
    model = Place
    template_name = 'core/denomination/directory/place/place.html'
    context_object_name = 'place'
    group = None
    denomination = None

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'place'
        context['page_title'] = '{0} {1}, {2}'.format(self.object.place_type, self.object.name, self.object.city)
        context['announce_list'] = PlaceAnnounce.objects.posted_by_place(self.object)[:RECENT_POSTS_COUNT]
        context['community_list'] = PlaceCommunity.objects.posted_by_place(self.object)[:RECENT_POSTS_COUNT]
        context['organizations'] = self.object.placeorganization_set.all()
        place_activities = PlaceActivity.objects.filter(place = self.object).order_by('activity__day_of_week')
        masses = place_activities.filter(activity__activity_type__codename = "messe")
        others = place_activities.exclude(activity__activity_type__codename = "messe")

        mass_hours = {}
        other_hours = {}

        for key, group in itertools.groupby(masses, key = lambda x: DAY_OF_THE_WEEK[x.activity.day_of_week]):
            mass_hours[key] = (list(group))
        context['mass_hours'] = mass_hours

        for key, group in itertools.groupby(others, key = lambda x: DAY_OF_THE_WEEK[x.activity.day_of_week]):
            other_hours[key] = (list(group))
        context['other_hours'] = other_hours

        return context


class PlaceAnnounceDetailView(DetailView):
    model = PlaceAnnounce
    template_name = 'core/denomination/directory/place/place_announce.html'
    context_object_name = 'announce'
    group = None
    denomination = None
    place = None

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        self.place = get_object_or_404(Place, slug = self.kwargs['place'])
        pa = PlaceAnnounce.objects.posted_by_place(self.place).get(slug = self.kwargs.get('announce'))
        print("pa => {}".format(pa))
        try:
            return PlaceAnnounce.objects.posted_by_place(self.place).get(slug = self.kwargs.get('announce'))
        except PlaceAnnounce.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'announce'
        context['page_title'] = "Annonce: ".format(self.object.title)
        context['place'] = self.place

        try:
            context['next_announce'] = PlaceAnnounce.get_next_by_published_on(self.object)
        except PlaceAnnounce.DoesNotExist:
            context['next_announce'] = None

        try:
            context['previous_announce'] = PlaceAnnounce.get_previous_by_published_on(self.object)
        except PlaceAnnounce.DoesNotExist:
            context['previous_announce'] = None

        return context


class PlaceCommunityDetailView(DetailView):
    model = PlaceCommunity
    template_name = 'core/denomination/directory/place/place_community.html'
    context_object_name = 'community'
    group = None
    denomination = None
    place = None

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        self.place = get_object_or_404(Place, slug = self.kwargs['place'])
        pa = PlaceCommunity.objects.posted_by_place(self.place).get(slug = self.kwargs.get('community'))
        print("pa => {}".format(pa))
        try:
            return PlaceCommunity.objects.posted_by_place(self.place).get(slug = self.kwargs.get('community'))
        except PlaceCommunity.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'community'
        context['page_title'] = self.object.title
        context['place'] = self.place

        try:
            context['next_community'] = PlaceCommunity.get_next_by_published_on(self.object)
        except PlaceCommunity.DoesNotExist:
            context['next_community'] = None

        try:
            context['previous_community'] = PlaceCommunity.get_previous_by_published_on(self.object)
        except PlaceCommunity.DoesNotExist:
            context['previous_community'] = None

        return context


class EventView(DetailView):
    model = Event
    template_name = 'core/denomination/directory/event.html'
    context_object_name = 'event'
    group = None
    denomination = None

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'event'
        context['page_title'] = '{0} : {1}, {2}'.format(self.object.event_type, self.object.title, self.object.city)

        return context


class ServiceView(DetailView):
    model = Service
    template_name = 'core/denomination/directory/service.html'
    context_object_name = 'service'
    group = None
    denomination = None

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'service'
        context['page_title'] = '{0} : {1}, {2}'.format(self.object.service_type, self.object.name, self.object.city)

        return context


class DirectoryWishSearchView(TemplateView):
    group = None
    denomination = None

    def get(self, request, *args, **kwargs):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return redirect('core:denomination', group = self.group.slug, denomination = self.denomination.slug)

    def post(self, request, *args, **kwargs):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])

        post = request.POST
        search_type = post.get('search_type', None)

        geo_loc = str_to_json(post.get('geo_loc', ""))
        search_elements = str_to_json(post.get('search_elements', ""))

        search_elements = dict((key, d[key]) for d in search_elements for key in d)
        place_search = search_elements.get('place', None)
        activity_place_search = search_elements.get('activity-place', None)
        location_search = search_elements.get('location', None)

        if search_type in ['worship-place', 'worship-activity', 'worship-event', 'worship-service']:
            search_result = {}
            count_search_result = 0
            search_type_text = ""
            from_geo_loc = False

            if search_type == 'worship-place':
                model = Place
                if extract_values(place_search) is not None:
                    place_id = extract_values(place_search)['id']
                    if place_id is not None:
                        place = get_class_instance(model, pk = place_id)
                        if place is not None:
                            return redirect('core:place', group = self.group.slug, denomination = self.denomination.slug, slug = place.slug)
                        else:
                            messages.success(request, _('Aucun lieu de culte ne correspond à votre recherche !'))
                            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                    else:
                        messages.success(request, _('Aucun lieu de culte ne correspond à votre recherche !'))
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                else:
                    search_type_text = "lieux de culte"
                    search_result = model.objects.posted_by_denomination(self.denomination)
                    place_type = post.get('place_type', None)

                    if place_type is not None and place_type != "":
                        search_result = search_result.filter(place_type = place_type)

                    if extract_values(location_search) is not None:
                        location_id = extract_values(location_search)['id']
                        location_type = extract_values(location_search)['type']
                        if location_id is not None and location_type is not None:
                            if location_type == 'city':
                                search_result = search_result.filter(city = location_id)
                            elif location_type == 'town':
                                search_result = search_result.filter(town = location_id)
                            elif location_type == 'borough':
                                search_result = search_result.filter(borough = location_id)
                            else:
                                search_result = search_result.filter(Q(city = location_id) | Q(town = location_id) | Q(borough = location_id))
                    else:
                        search_result = get_geo_loc(model, geo_loc, search_result, by = "coords")
                        search_result, count_search_result = search_result[0], search_result[1]
                        if search_result:
                            from_geo_loc = True

            if search_type == 'worship-activity':
                model = PlaceActivity
                activity_date = post.get('activity_date', None)
                search_type_text = "horaires de {}".format(self.denomination.denomination_details.celebration)
                search_result = model.objects.filter(place__denomination = self.denomination, activity__activity_type__codename = "messe")
                activities_list = {}

                if extract_values(activity_place_search) is not None:
                    place_id = extract_values(activity_place_search)['id']
                    if place_id is not None:
                        search_result = search_result.filter(place = place_id)
                else:
                    if extract_values(location_search) is not None:
                        location_id = extract_values(location_search)['id']
                        location_type = extract_values(location_search)['type']
                        if location_id is not None and location_type is not None:
                            if location_type == 'city':
                                search_result = search_result.filter(place__city = location_id)
                            elif location_type == 'town':
                                search_result = search_result.filter(place__town = location_id)
                            elif location_type == 'borough':
                                search_result = search_result.filterplace__(borough = location_id)
                            else:
                                search_result = search_result.filter(
                                    Q(place__city = location_id) | Q(place__town = location_id) | Q(place__borough = location_id))
                    else:
                        search_result = get_geo_loc(model, geo_loc, search_result, by = "address", place = True)

                if search_result is not None:
                    # search_result = get_occurrences(place_activities = None, activity_date = activity_date)
                    occ = [get_occurrences(x.id) for x in search_result if get_occurrences(x.id) and x.recurrences is not None]

                    t_list = list(itertools.chain.from_iterable(occ))

                    if activity_date and activity_date != "":
                        activity_date = date_reverse("/", "-", activity_date)
                        activity_date = convert(activity_date)
                        t_list = [x for x in t_list if x['date'] == activity_date]

                    sorted_activities_list = sorted(t_list, key = itemgetter('date', 'start_time'))
                    for key, group in itertools.groupby(sorted_activities_list, key = lambda x: x['date']):
                        activities_list[key] = (list(group))

                search_result = activities_list
                count_search_result = len(search_result)

            elif search_type == 'worship-event':
                model = Event
                search_type_text = "événements"
                search_result = model.objects.filter(Q(group = self.group) | Q(denomination = self.denomination))
                event_type = post.get('event_type', None)

                if event_type is not None and event_type != "":
                    search_result = search_result.filter(event_type = event_type)

                if extract_values(location_search) is not None:
                    location_id = extract_values(location_search)['id']
                    location_type = extract_values(location_search)['type']
                    if location_id is not None and location_type is not None:
                        if location_type == 'city':
                            search_result = search_result.filter(city = location_id)
                        elif location_type == 'town':
                            search_result = search_result.filter(town = location_id)
                        elif location_type == 'borough':
                            search_result = search_result.filter(borough = location_id)
                        else:
                            search_result = search_result.filter(Q(city = location_id) | Q(town = location_id) | Q(borough = location_id))
                else:
                    search_result = get_geo_loc(model, geo_loc, search_result, by = "address")
                    from_geo_loc = True
                event_date = post.get('event_date', "")

                if event_date and event_date != "":
                    event_date = date_reverse("/", "-", event_date)
                    search_result = search_result.filter(start_date = event_date)
                else:
                    search_result = search_result.filter(end_date__gt = datetime.today().date())
                count_search_result = search_result.count()

            elif search_type == 'worship-service':
                model = Service
                search_type_text = "services et associations"
                search_result = model.objects.filter(Q(group = self.group) | Q(denomination = self.denomination))
                service_type = post.get('service_type', None)

                if service_type is not None and service_type != "":
                    search_result = search_result.filter(service_type = service_type)

                if extract_values(location_search) is not None:
                    location_id = extract_values(location_search)['id']
                    location_type = extract_values(location_search)['type']
                    if location_id is not None and location_type is not None:
                        if location_type == 'city':
                            search_result = search_result.filter(city = location_id)
                        elif location_type == 'town':
                            search_result = search_result.filter(town = location_id)
                        elif location_type == 'borough':
                            search_result = search_result.filter(borough = location_id)
                        else:
                            search_result = search_result.filter(Q(city = location_id) | Q(town = location_id) | Q(borough = location_id))
                else:
                    search_result = get_geo_loc(model, geo_loc, search_result, by = "address")
                    from_geo_loc = True
                count_search_result = search_result.count()

            if count_search_result == 0 or count_search_result >= 1:
                context = {
                    'group': self.group,
                    'denomination': self.denomination,
                    'menu': 'directory',
                    'page_name': 'annuaire',
                    'page_title': 'Liste des {} à proximité'.format(search_type_text),
                    'search_type': search_type,
                    'search_result': search_result,
                    'from_geo_loc': from_geo_loc
                }
                return render(request, 'core/denomination/directory/i_wish_search.html', context)
        else:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class DirectoryLookupView(ListView):
    template_name = 'core/denomination/directory/lookup.html'
    group = None
    denomination = None

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return Place.objects.filter(status = True).filter(denomination = self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'directory'
        context['page_name'] = 'lookup'
        context['page_title'] = 'Horaires & Annuaire - Je Recherche'
        context['place_types'] = PlaceType.objects.filter(
            Q(denomination = self.denomination) | Q(group = self.group) | Q(denomination__isnull = True) | Q(group__isnull = True))
        context['event_types'] = EventType.objects.filter((Q(denomination = self.denomination) | Q(denomination__isnull = True)), group = self.group)
        context['service_types'] = ServiceType.objects.filter((Q(group = self.group) | Q(group__isnull = True)))
        context['languages'] = ActivityLanguage.objects.all()

        # context['activity_types'] = ActivityType.objects.filter((Q(denomination = self.denomination) | Q(denomination__isnull = True)), group = self.group)
        # context['districts'] = District.objects.all()
        # context['regions'] = Region.objects.all()
        # context['cities'] = City.objects.exclude(latitude__isnull = True)

        return context


class DirectorySearchFullView(View):
    group = None
    denomination = None

    def post(self, request, *args, **kwargs):
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        post = request.POST

        lookup = post.get('lookup', None)

        geo_loc = str_to_json(post.get('geo_loc', ""))
        search_elements = str_to_json(post.get('search_elements', ""))

        search_elements = dict((key, d[key]) for d in search_elements for key in d)
        place_search = search_elements.get('place', None)
        location_search = search_elements.get('location', None)

        if lookup == "activity":
            template = "activity_lookup"
            search_result = get_activities(post, self.denomination, place_search, location_search, geo_loc)

            t_data = {
                "group": self.group,
                "denomination": self.denomination,
                "results": search_result
            }
            templates = render_to_string('core/denomination/directory/lookup/{}.html'.format(template), t_data)
        else:
            lookup_type = post.get('lookup_type', None)
            lookup_keyword = post.get('lookup_keyword', "")
            if lookup_type == "event":
                template = "{}_lookup".format(lookup_type)
                model = Event
                search_result = model.objects.filter(Q(group = self.group) | Q(denomination = self.denomination))
                event_type = post.get('event_type', None)

                if event_type is not None and event_type != "":
                    search_result = search_result.filter(event_type = event_type)

                if lookup_keyword and lookup_keyword != "":
                    search_result = search_result.filter(title__icontains = lookup_keyword)

                if extract_values(location_search) is not None:
                    location_id = extract_values(location_search)['id']
                    location_type = extract_values(location_search)['type']
                    if location_id is not None and location_type is not None:
                        if location_type == 'city':
                            search_result = search_result.filter(city = location_id)
                        elif location_type == 'town':
                            search_result = search_result.filter(town = location_id)
                        elif location_type == 'borough':
                            search_result = search_result.filter(borough = location_id)
                        else:
                            search_result = search_result.filter(Q(city = location_id) | Q(town = location_id) | Q(borough = location_id))
                else:
                    search_result = get_geo_loc(model, geo_loc, search_result, by = "address")
                    from_geo_loc = True
                event_date = post.get('event_date', "")

                if event_date and event_date != "":
                    event_date = date_reverse("/", "-", event_date)
                    search_result = search_result.filter(start_date = event_date)
                else:
                    search_result = search_result.filter(end_date__gt = datetime.today().date())

                t_data = {
                    "group": self.group,
                    "denomination": self.denomination,
                    "results": search_result
                }
                templates = render_to_string('core/denomination/directory/lookup/{}.html'.format(template), t_data)
            else:
                template = "{}_lookup".format(lookup_type)
                model = Service
                search_result = model.objects.filter(Q(group = self.group) | Q(denomination = self.denomination))
                service_type = post.get('service_type', None)

                if service_type is not None and service_type != "":
                    search_result = search_result.filter(service_type = service_type)

                if lookup_keyword and lookup_keyword != "":
                    search_result = search_result.filter(name__icontains = lookup_keyword)

                if extract_values(location_search) is not None:
                    location_id = extract_values(location_search)['id']
                    location_type = extract_values(location_search)['type']
                    if location_id is not None and location_type is not None:
                        if location_type == 'city':
                            search_result = search_result.filter(city = location_id)
                        elif location_type == 'town':
                            search_result = search_result.filter(town = location_id)
                        elif location_type == 'borough':
                            search_result = search_result.filter(borough = location_id)
                        else:
                            search_result = search_result.filter(Q(city = location_id) | Q(town = location_id) | Q(borough = location_id))
                else:
                    search_result = get_geo_loc(model, geo_loc, search_result, by = "address")
                    from_geo_loc = True

                t_data = {
                    "group": self.group,
                    "denomination": self.denomination,
                    "results": search_result
                }
                templates = render_to_string('core/denomination/directory/lookup/{}.html'.format(template), t_data)

        if len(search_result) > 0:
            status = True
        else:
            status = False

        data = {
            'status': status,
            'templates': templates,
        }

        return JsonResponse(data, safe = True)


def get_activities(post, denomination, place_search = None, location_search = None, geo_loc = None):
    if place_search is None:
        place_search = {}
    if location_search is None:
        location_search = {}
    if geo_loc is None:
        geo_loc = {}
    model = PlaceActivity
    activity_date = post.get('activity_date', None)
    language = post.get('language', None)
    activity_time = post.get('activity_time', None)
    search_result = model.objects.filter(place__denomination = denomination, activity__activity_type__codename = "messe")
    activities_list = {}

    if extract_values(place_search) is not None:
        place_id = extract_values(place_search)['id']
        if place_id is not None:
            search_result = search_result.filter(place = place_id)
    else:
        if extract_values(location_search) is not None:
            location_id = extract_values(location_search)['id']
            location_type = extract_values(location_search)['type']
            if location_id is not None and location_type is not None:
                if location_type == 'city':
                    search_result = search_result.filter(place__city = location_id)
                elif location_type == 'town':
                    search_result = search_result.filter(place__town = location_id)
                elif location_type == 'borough':
                    search_result = search_result.filter(place__borough = location_id)
                else:
                    search_result = search_result.filter(
                        Q(place__city = location_id) | Q(place__town = location_id) | Q(place__borough = location_id))
        else:
            search_result = get_geo_loc(model, geo_loc, search_result, by = "address", place = True)

    if search_result is not None:
        if len(language) == 0:
            language = None
        if len(activity_time) == 0:
            activity_time = None

        occ = [get_occurrences(x.id, language = language, time = activity_time) for x in search_result
               if get_occurrences(x.id, language = language, time = activity_time) and x.recurrences is not None]
        t_list = list(itertools.chain.from_iterable(occ))

        if activity_date and activity_date != "":
            activity_date = date_reverse("/", "-", activity_date)
            activity_date = convert(activity_date)
            t_list = [x for x in t_list if x['date'] == activity_date]

        sorted_activities_list = sorted(t_list, key = itemgetter('date', 'start_time'))
        for key, group in itertools.groupby(sorted_activities_list, key = lambda x: x['date']):
            activities_list[key] = (list(group))

    return activities_list
