from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.views.generic import ListView, DetailView

from apps.core.forms import RetirementTravelForm, RetirementPlaceForm
from apps.religion.models import Group, Denomination
from apps.retirement.models import *
from lib.config import POSTS_PAGINATE_BY
from lib.middleware import get_dates_from_date_range


class RetirementPlaceListView(ListView):
    model = RetirementPlace
    template_name = 'core/denomination/retirement/retirement_place_list.html'
    group = None
    denomination = None
    context_object_name = 'retirement_place_list'
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return RetirementPlace.objects.posted_by_denomination(self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'tools'
        context['page_name'] = 'retirement_list'
        context['page_title'] = 'Pèlerinages et retraites'
        context['retirement_place_form'] = RetirementPlaceForm()
        context['retirement_travel_form'] = RetirementTravelForm()

        return context


class RetirementSearchView(ListView):
    group = None
    denomination = None

    def post(self, request, *args, **kwargs):
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        post = request.POST

        retirement_search_type = post.get('retirement_search_type')
        if retirement_search_type in ['place', 'travel']:
            template = ''
            status = False

            name = post.get('name', None)
            target = post.get('target', None)
            reason = post.get('reason', None)
            city = post.get('city', None)
            environment = post.get('environment', None)

            search_result = RetirementPlace.objects.all()
            if retirement_search_type == 'place':
                if name is not None and len(name) > 0:
                    search_result = search_result.filter(
                        Q(name__icontains = name) |
                        Q(city__name__icontains = name) |
                        Q(city__country__name__icontains = name)
                    )

                if target is not None and len(target) > 0:
                    search_result = search_result.filter(target = target)
                if reason is not None and len(reason) > 0:
                    search_result = search_result.filter(reason = reason)
                if city is not None and len(city) > 0:
                    search_result = search_result.filter(city = city)
                if environment is not None and len(environment) > 0:
                    search_result = search_result.filter(environment = environment)
                convenience = post.getlist('convenience_place', None)
                if convenience is not None and len(convenience) > 0:
                    search_result = search_result.filter(convenience__in = convenience).distinct()
                
                status = True
                context = {
                    'request': request,
                    'group': self.group,
                    'denomination': self.denomination,
                    'retirement_place_list': search_result
                }
                template = render_to_string('core/denomination/retirement/retirement_place_search.html', context)
            else:
                name = post.get('name', None)

                if name is not None and len(name) > 0:
                    search_result = search_result.filter(name__icontains = name)
                if target is not None and len(target) > 0:
                    search_result = search_result.filter(target = target)
                if reason is not None and len(reason) > 0:
                    search_result = search_result.filter(reason = reason)
                if city is not None and len(city) > 0:
                    search_result = search_result.filter(city = city)
                if environment is not None and len(environment) > 0:
                    search_result = search_result.filter(environment = environment)
                convenience = post.getlist('convenience_travel', None)
                if convenience is not None and len(convenience) > 0:
                    search_result = search_result.filter(convenience__in = convenience).distinct()

                travels = RetirementTravel.objects.all()
                travel_type = post.get('type', None)
                if travel_type is not None and len(travel_type) > 0:
                    travels = travels.filter(type = travel_type)
                travels = travels.filter(retirement_place__in = search_result)

                travel_instances = RetirementTravelInstance.objects.filter(retirement_travel__in = travels)

                date_range = post.get('date_range', None)
                if date_range is not None and len(date_range) > 0:
                    dates = get_dates_from_date_range(date_range)
                    travel_instances = travel_instances.filter(departure_date__range = dates)
                if travel_instances:
                    status = True
                    context = {
                        'request': request,
                        'group': self.group,
                        'denomination': self.denomination,
                        'retirement_travel_list': travel_instances
                    }
                    template = render_to_string('core/denomination/retirement/retirement_travel_search.html', context)

            data = {
                'status': status,
                'template': template,
            }

            return JsonResponse(data, safe = False)


class RetirementTravelSearchView(ListView):
    model = RetirementPlace
    group = None
    denomination = None

    def post(self, request, *args, **kwargs):
        form = RetirementTravelForm(data = request.POST)
        post = form.data
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])

        if form.is_valid():
            search_result = RetirementPlace.objects.all()
            name = post.get('name', None)
            target = post.get('target', None)
            reason = post.get('reason', None)
            city = post.get('city', None)
            environment = post.get('environment', None)
            convenience = post.getlist('convenience', None)

            if name is not None and len(name) > 0:
                search_result = search_result.filter(name__icontains = name)
            if target is not None and len(target) > 0:
                search_result = search_result.filter(target = target)
            if reason is not None and len(reason) > 0:
                search_result = search_result.filter(reason = reason)
            if city is not None and len(city) > 0:
                search_result = search_result.filter(city = city)
            if environment is not None and len(environment) > 0:
                search_result = search_result.filter(environment = environment)
            if convenience is not None and len(convenience) > 0:
                search_result = search_result.filter(convenience__in = convenience).distinct()

            travels = RetirementTravel.objects.all()
            travel_type = post.get('type', None)
            if travel_type is not None and len(travel_type) > 0:
                travels = travels.filter(type = travel_type)
            travels = travels.filter(retirement_place__in = search_result)

            travel_instances = RetirementTravelInstance.objects.filter(retirement_travel__in = travels)

            date_range = post.get('date_range', None)
            if date_range is not None and len(date_range) > 0:
                dates = get_dates_from_date_range(date_range)
                travel_instances = travel_instances.filter(departure_date__range = dates)

            context = {
                'group': self.group,
                'denomination': self.denomination,
                'menu': 'tools',
                'page_name': 'retirement_list',
                'page_title': 'Recherche pèlerinages et retraites',
                'search_term': name,
                # 'search_type': RetirementTravel.objects.get(pk = travel_type).type.name,
                'search_count': travel_instances.count(),
                'travels': travel_instances,
                # 'all_travels': travel_instances,
                # 'retirement_place_list': search_result,
            }

            return render(request, 'core/denomination/retirement/retirement_travel_search.html', context)


class RetirementPlaceDetailView(DetailView):
    model = RetirementPlace
    template_name = 'core/denomination/retirement/retirement_place_details.html'
    group = None
    denomination = None
    context_object_name = "retirement_place"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = get_object_or_404(Group, slug = self.kwargs['group'])
        context['denomination'] = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['menu'] = 'tools'
        context['page_name'] = 'retirement'
        context['page_title'] = '{}'.format(self.object.name)
        # context['retirement_list'] = Retirement.objects.published().filter(place = self.object)

        return context
