from django.contrib import messages
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
# from hitcount.views import HitCountDetailView
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView, DetailView, CreateView

from apps.core.forms import NecrologyCondolencesForm
from apps.posts.models import Article, Tag, Necrology, Testimony, Announce
from apps.religion.models import Group, Denomination
from lib.config import POSTS_PAGINATE_BY, django_logger, POSTS_TOTAL


class ArticleListView(ListView):
    model = Article
    template_name = 'core/denomination/posts/article_list.html'
    context_object_name = 'article_list'
    group = None
    denomination = None
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return Article.objects.posted_by_denomination(self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'news'
        context['page_name'] = 'articles'
        context['page_title'] = 'Articles'
        context['tags'] = Tag.objects.all()

        return context


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'core/denomination/posts/article.html'
    context_object_name = 'article'
    group = None
    denomination = None

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        try:
            return Article.objects.posted_by_denomination(self.denomination).get(slug = self.kwargs.get('slug'))
        except Article.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'news'
        context['page_name'] = 'article'
        context['page_title'] = self.object.title
        context['categories'] = Article.objects.related_categories(self.denomination)
        context['tags'] = Tag.objects.all()
        context['similar_posts'] = Article.objects.similar_posts(denomination = self.denomination, post = self.object)

        try:
            context['next_article'] = Article.get_next_by_published_on(self.object, denomination = self.denomination)
        except Article.DoesNotExist:
            context['next_article'] = None

        try:
            context['previous_article'] = Article.get_previous_by_published_on(self.object, denomination = self.denomination)
        except Article.DoesNotExist:
            context['previous_article'] = None

        return context


class AnnounceListView(ListView):
    model = Announce
    template_name = 'core/denomination/posts/announce_list.html'
    context_object_name = 'announce_list'
    group = None
    denomination = None
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return Announce.objects.posted_by_denomination(self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'news'
        context['page_name'] = 'announces'
        context['page_title'] = 'Announces'

        return context


class AnnounceDetailView(DetailView):
    model = Announce
    template_name = 'core/denomination/posts/announce.html'
    context_object_name = 'announce'
    group = None
    denomination = None

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        try:
            return Announce.objects.posted_by_denomination(self.denomination).get(slug = self.kwargs.get('slug'))
        except Announce.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'news'
        context['page_name'] = 'announce'
        context['page_title'] = self.object.title

        try:
            context['next_announce'] = Announce.get_next_by_published_on(self.object, denomination = self.denomination)
        except Announce.DoesNotExist:
            context['next_announce'] = None

        try:
            context['previous_announce'] = Announce.get_previous_by_published_on(self.object, denomination = self.denomination)
        except Announce.DoesNotExist:
            context['previous_announce'] = None

        return context


class NecrologyListView(ListView):
    model = Necrology
    template_name = 'core/denomination/posts/necrology_list.html'
    context_object_name = 'necrology_list'
    paginate_by = POSTS_TOTAL
    ordering = ["-created_at"]

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = get_object_or_404(Group, slug = self.kwargs['group'])
        context['denomination'] = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['menu'] = 'necrology'
        context['page_name'] = 'necrology_list'
        context['page_title'] = 'Necrologie'

        return context

    def post(self, request, *args, **kwargs):
        group = get_object_or_404(Group, slug = self.kwargs['group'])
        denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        post = request.POST
        search = post.get('necrology_search', '')

        necrology_list = {}
        if len(search) > 0:
            necrology_list = Necrology.objects.filter(title__icontains = search)

        context = {
            'group': group,
            'denomination': denomination,
            'menu': 'necrology',
            'page_name': 'necrology_list',
            'page_title': 'Nécrologie',
            'is_searched': True,
            'necrology_list': necrology_list,
        }

        return render(request, self.template_name, context)


class NecrologyDetailView(DetailView, CreateView):
    model = Necrology
    template_name = 'core/denomination/posts/necrology.html'
    context_object_name = "necrology"
    fields = '__all__'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = get_object_or_404(Group, slug = self.kwargs['group'])
        context['denomination'] = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['menu'] = 'necrology'
        context['page_name'] = 'necrology'
        context['page_title'] = 'Necrologie'
        context['form'] = NecrologyCondolencesForm(necrology = self.object.pk)

        return context

    def post(self, request, *args, **kwargs):
        form = NecrologyCondolencesForm(data = request.POST)
        post = form.data

        if form.is_valid():
            form.save()
            messages.success(request, _('Votre demande a bien été prise en compte'))
            return redirect('core:necrology', self.kwargs['group'], self.kwargs['denomination'], self.kwargs['slug'])
        else:
            messages.error(request, _('Error lors de l\'enregistrement de votre demande'))
            context = {
                'status': 'error'
            }
            self.get(request, context)
            return redirect('core:necrology', self.kwargs['group'], self.kwargs['denomination'], self.kwargs['slug'])


class TestimonyListView(ListView):
    model = Testimony
    template_name = 'core/denomination/posts/testimony_list.html'
    context_object_name = 'testimony_list'
    group = None
    denomination = None
    paginate_by = POSTS_TOTAL

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return Testimony.objects.posted_by_denomination(self.denomination)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'testimony'
        context['page_name'] = 'testimony'
        context['page_title'] = 'Témoignages'

        return context


class TestimonyDetailView(DetailView):
    model = Testimony
    context_object_name = "testimony"
    group = None
    denomination = None
    template_name = 'core/denomination/posts/testimony.html'

    def get_object(self, queryset = None):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        try:
            return Testimony.objects.posted_by_denomination(self.denomination).get(slug = self.kwargs.get('slug'))
        except Testimony.DoesNotExist as e:
            django_logger.error(e)
            raise Http404

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'testimony'
        context['page_name'] = 'testimony'
        context['page_title'] = 'Témoignages'
        # context['similar_posts'] = Testimony.objects.similar_posts(denomination = self.denomination, post = self.object)

        return context
