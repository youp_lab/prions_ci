from datetime import datetime

from django.contrib import messages
from django.db import IntegrityError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, ListView, DetailView

from apps.core.forms import ActivityRequestForm, ContactRequestForm, ReceiveRequestForm, DonationForm, ReceiveRequestImageForm
from apps.posts.models import Gallery, Necrology
from apps.religion.models import Group, Denomination, Place, ActivityRequest, ContactRequest, ReceiveRequest, ReceiveRequestImage, Donation, \
    ActivityRequestType
from lib.config import django_logger, POSTS_PAGINATE_BY
from lib.middleware import get_class_instance, get_dates_from_date_range


class EbookListView(ListView):
    model = Gallery
    template_name = 'core/denomination/media/ebook_list.html'
    context_object_name = 'ebook_list'
    group = None
    denomination = None
    paginate_by = POSTS_PAGINATE_BY

    def get_queryset(self):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        return Gallery.objects.filter(denomination = self.denomination, type__codename = "ebook")

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Ebook'
        context['page_name'] = 'ebook'
        context['group'] = self.group
        context['denomination'] = self.denomination

        return context


# class EbookDetailView(ListView):
#     model = Gallery
#     template_name = 'frontend/denomination/gallery/ebook_list.html'
#     group = None
#     denomination = None
#     type = None
#     paginate_by = POSTS_PAGINATE_BY
#
#     def get_queryset(self):
#         self.group = get_object_or_404(Group, slug = self.kwargs['group'])
#         self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
#         return Gallery.objects.filter(denomination = self.denomination, media_type__type = "ebook")
#
#     def get_context_data(self, *, object_list = None, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['page_title'] = 'Ebook'
#         context['page_name'] = 'ebook'
#         context['group'] = self.group
#         context['denomination'] = self.denomination
#
#         try:
#             context['next_article'] = Gallery.get_next_by_created_at(self.object, denomination = self.denomination)
#         except Gallery.DoesNotExist:
#             context['next_article'] = None
#
#         try:
#             context['previous_article'] = Gallery.get_previous_by_created_at(self.object, denomination = self.denomination)
#         except Gallery.DoesNotExist:
#             context['previous_article'] = None
#
#         return context


class ActivityRequestView(CreateView):
    model = ActivityRequest
    template_name = 'core/denomination/tools/activity_request.html'
    group = None
    denomination = None
    form_class = ActivityRequestForm

    def get_form_kwargs(self):
        kwargs = super(ActivityRequestView, self).get_form_kwargs()
        kwargs.update(self.kwargs)
        if all(k in kwargs for k in ['place']):
            kwargs['initial']['place'] = get_class_instance(Place, slug = kwargs.get('place'))
            kwargs['initial']['necrology'] = get_class_instance(Necrology, slug = kwargs.get('necrology'))
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'tools'
        context['page_name'] = 'activity_request'
        context['page_title'] = 'Faire une demande'
        context['place_list'] = Place.objects.posted_by_denomination(self.denomination)
        if self.kwargs.get('place') is not None:
            context['hasPlace'] = True
            context['place'] = get_class_instance(Place, slug = self.kwargs.get('place'))
        if self.kwargs.get('necrology') is not None:
            context['hasNecrology'] = True
            context['necrology'] = get_class_instance(Necrology, slug = self.kwargs.get('necrology'))

        return context

    def post(self, request, *args, **kwargs):
        post = request.POST
        activity_request_type = get_class_instance(ActivityRequestType, post.get('type', ''))

        date_range = post.get('date_range', None)
        start_date = None
        end_date = None
        if date_range is not None and len(date_range) > 0:
            dates = get_dates_from_date_range(date_range)
            start_date = datetime.strptime(dates[0], '%Y-%m-%d').date()
            end_date = datetime.strptime(dates[1], '%Y-%m-%d').date()

        data = {
            'type': activity_request_type,
            'sender_last_name': post.get('sender_last_name', ''),
            'sender_first_name': post.get('sender_first_name', ''),
            'description': post.get('description', ''),
            'place_id': post.get('place', ''),
            'sender_contact': post.get('sender_contact', ''),
            'sender_email': post.get('sender_email', ''),
            'start_date': start_date,
            'end_date': end_date
        }

        try:
            req = ActivityRequest(**data)
            req.save()
            last_insert_id = ActivityRequest.objects.get(pk = req.id).id
            msg = _('Votre demande a bien été prise en compte')
        except IntegrityError as e:
            last_insert_id = None
            msg = _('Error lors de l\'enregistrement de votre demande')
            django_logger.error(request, msg)

        data = {
            'created': bool(last_insert_id),
            'msg': msg
        }

        return JsonResponse(data)


class ContactRequestView(CreateView):
    model = ContactRequest
    template_name = 'core/denomination/tools/contact_request.html'
    group = None
    denomination = None
    form_class = ContactRequestForm

    def get_form_kwargs(self):
        kwargs = super(ContactRequestView, self).get_form_kwargs()
        kwargs.update(self.kwargs)
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'tools'
        context['page_name'] = 'contact_request'
        context['page_title'] = self.denomination.denomination_details.contact_request

        return context

    def post(self, request, *args, **kwargs):
        post = request.POST
        data = {
            'subject': post.get('subject', ''),
            'sender_last_name': post.get('sender_last_name', ''),
            'sender_first_name': post.get('sender_first_name', ''),
            'description': post.get('description', ''),
            'place_id': post.get('place', ''),
            'sender_contact': post.get('sender_contact', ''),
            'sender_email': post.get('sender_email', '')
        }
        try:
            req = ContactRequest(**data)
            req.save()
            last_insert_id = ContactRequest.objects.get(pk = req.id).id
            msg = _('Votre demande a bien été prise en compte')
        except IntegrityError as e:
            last_insert_id = None
            msg = _('Error lors de l\'enregistrement de votre demande')
            django_logger.error(request, msg)

        data = {
            'created': bool(last_insert_id),
            'msg': msg
        }

        return JsonResponse(data)


class ReceiveRequestListView(ListView):
    model = ReceiveRequest
    template_name = 'core/denomination/tools/receive_request_list.html'
    context_object_name = 'receive_list'
    group = None
    denomination = None

    def get_queryset(self):
        return ReceiveRequest.objects.published()

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'receives'
        context['page_name'] = 'receive_list'
        context['page_title'] = 'Liste des Demandes'
        # context['place_list'] = Place.objects.filter(denomination__slug = self.denomination)

        return context


class ReceiveRequestDetailView(DetailView):
    model = ReceiveRequest
    template_name = 'core/denomination/tools/receive_request.html'
    context_object_name = 'receive'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group'] = get_object_or_404(Group, slug = self.kwargs['group'])
        context['denomination'] = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['menu'] = 'receives'
        context['page_name'] = 'receive_detail'
        context['page_title'] = 'Demande d\'aide'
        context['donations'] = Donation.objects.filter(receive_request = self.object)
        context['donation_form'] = DonationForm(initial = {'receive_request_id': self.object.id})

        return context


class ReceiveRequestCreateView(CreateView):
    model = ReceiveRequest
    template_name = 'core/denomination/tools/receive_request_create.html'
    group = None
    denomination = None
    form_class = ReceiveRequestForm

    def get_form_kwargs(self):
        kwargs = super(ReceiveRequestCreateView, self).get_form_kwargs()
        kwargs.update(self.kwargs)
        if all(k in kwargs for k in ['denomination']):
            kwargs['initial']['denomination'] = get_class_instance(Denomination, slug = kwargs.get('denomination'))
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])
        context['group'] = self.group
        context['denomination'] = self.denomination
        context['menu'] = 'receives'
        context['page_name'] = 'receive_create'
        context['page_title'] = 'Solliciter une aide'
        context['image_form'] = ReceiveRequestImageForm()
        context['place_list'] = Place.objects.filter(denomination__slug = self.denomination)

        return context

    def post(self, request, *args, **kwargs):
        form = ReceiveRequestForm(data = request.POST, denomination = self.denomination)
        image_form = ReceiveRequestImageForm(request.POST, request.FILES)
        files = request.FILES.getlist('image')

        if form.is_valid() and image_form.is_valid():
            receive_request_instance = form.save()
            for f in files:
                file_instance = ReceiveRequestImage(image = f, receive_request = receive_request_instance)
                file_instance.save()

            messages.success(request, _('Votre demande a bien été prise en compte'))
            return redirect('core:receive-create', self.kwargs['group'], self.kwargs['denomination'])
        else:
            messages.error(request, _('Error lors de l\'enregistrement de votre demande'))
            context = {
                'status': 'error'
            }
            self.get(request, context)
            return redirect('core:receive-create', self.kwargs['group'], self.kwargs['denomination'])


class ReceiveRequestDonationView(CreateView):
    def post(self, request, *args, **kwargs):
        form = DonationForm(data = request.POST)
        receive_request_id = self.kwargs.get('pk', None)

        if form.is_valid():
            donation_instance = form.save(commit = False)
            donation_instance.receive_request_id = receive_request_id
            donation_instance.save()

            messages.success(request, _('Votre participation a bien été prise en compte'))
            return redirect('core:receive', self.kwargs['group'], self.kwargs['denomination'], receive_request_id)
        else:
            messages.error(request, _('Error lors de l\'enregistrement de votre participation'))
            context = {
                'status': 'error'
            }
            self.get(request, context)
            return redirect('core:receive', self.kwargs['group'], self.kwargs['denomination'], receive_request_id)
