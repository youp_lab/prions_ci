from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import signing
from django.core.signing import BadSignature
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, CreateView

from apps.core.enums import ACTIVATED, INACTIVATED
from apps.core.forms import KYFDenominationForm
from apps.core.functions import register_menu_by_model
from apps.core.views import open_frame
from apps.posts.models import Article, Announce
from apps.religion.models import Group, Denomination
from lib.config import POSTS_TOTAL, POSTS_BY_SLIDER, RECENT_POSTS_COUNT


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Bienvenue sur Prions.ci'
        context['page_name'] = 'index'
        context['registered_groups'] = Group.objects.filter(status = ACTIVATED, visible = ACTIVATED)
        context['unregistered_groups'] = Group.objects.filter(status = ACTIVATED, visible = INACTIVATED)
        context['denominations'] = Denomination.objects.filter(status = ACTIVATED)

        frame = None
        get_data = self.request.GET
        if get_data and 'r' in get_data:
            try:
                req = signing.loads(get_data.get('r'))
                if 'frame' in req and req.get('frame') is True:
                    frame = open_frame(self.request, req)
            except BadSignature as e:
                pass
        context['has_frame'] = frame

        return context


class DenominationListView(LoginRequiredMixin, TemplateView):
    group = None

    def get(self, request, *args, **kwargs):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        if self.group.slug in ["islam"]:
            return redirect('core:denomination', group = self.group.slug, denomination = self.group.slug)

        group_menu = register_menu_by_model(self.group.slug, Denomination, ['name', 'slug', 'group'])
        if not group_menu.exists():
            messages.warning(request, 'Aucune dénomination réligieuse rattachée à ce groupe !')
            return redirect('core:index')
        else:
            articles = Article.objects.recent('group', self.group, POSTS_TOTAL)
            announces = Announce.objects.recent('group', self.group, POSTS_TOTAL)
            context = {
                'group': self.group,
                'menu': 'group',
                'page_name': 'home_group',
                'page_title': 'Portail {}'.format(self.group.common_name),
                'group_menu': group_menu,
                'recent_articles': articles[:POSTS_BY_SLIDER],
                'old_articles': articles[POSTS_BY_SLIDER:POSTS_TOTAL],
                'recent_announces': announces[:POSTS_BY_SLIDER],
                'old_announces': announces[POSTS_BY_SLIDER:POSTS_TOTAL],
                # 'recent_pictures': Gallery.objects.media_type('image')[:MEDIA_POSTS_COUNT],
                # 'recent_videos': Gallery.objects.media_type('video')[:MEDIA_POSTS_COUNT],
                # 'categories': Category.objects.all(),
                # 'tags': Tag.objects.all(),
                # 'most_visited': HitCount.objects.all()[:3],
            }
            return render(request, 'core/group/index.html', context)


class DenominationHomeView(LoginRequiredMixin, TemplateView):
    denomination = None
    group = None

    def get(self, request, *args, **kwargs):
        self.group = get_object_or_404(Group, slug = self.kwargs['group'])
        self.denomination = get_object_or_404(Denomination, slug = self.kwargs['denomination'])

        articles = Article.objects.recent('denomination', self.denomination, POSTS_TOTAL)
        announces = Announce.objects.recent('denomination', self.denomination, POSTS_TOTAL)

        context = {
            'group': self.group,
            'denomination': self.denomination,
            'menu': 'denomination',
            'page_name': 'home_denomination',
            'page_title': 'Portail {0}'.format(self.denomination.denomination_details.common_name),
            'recent_articles': articles[:POSTS_BY_SLIDER],
            'old_articles': articles[POSTS_BY_SLIDER:RECENT_POSTS_COUNT],
            'recent_announces': announces[:POSTS_BY_SLIDER],
            'old_announces': announces[POSTS_BY_SLIDER:RECENT_POSTS_COUNT],
        }

        if 'recent_posts' in context and len(context['recent_posts']) == 4:
            context['recent_posts'].pop()

        return render(request, 'core/denomination/index.html', context)


class AboutUsView(TemplateView):
    template_name = 'core/common/about.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Faites vous connaître'
        context['page_name'] = 'kyc'

        return context


class KYCView(TemplateView):
    template_name = 'core/common/kyc/index.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Faites vous connaître'
        context['page_name'] = 'kyc'

        return context


class KYCViewDenomination(CreateView):
    model = Denomination
    template_name = 'core/common/kyc/denomination.html'
    fields = '__all__'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Inscrivez une dénomination'
        context['page_name'] = 'kyc_denomination'
        context['form'] = KYFDenominationForm

        return context

    def post(self, request, *args, **kwargs):
        print("request.POST => {}".format(request.POST))
        return
        form = KYFDenominationForm(data = request.POST)
        receive_request_id = self.kwargs.get('pk', None)

        if form.is_valid():
            donation_instance = form.save(commit = False)
            donation_instance.receive_request_id = receive_request_id
            donation_instance.save()

            messages.success(request, _('Votre participation a bien été prise en compte'))
            return redirect('core:receive', self.kwargs['group'], self.kwargs['denomination'], receive_request_id)
        else:
            messages.error(request, _('Error lors de l\'enregistrement de votre participation'))
            context = {
                'status': 'error'
            }
            self.get(request, context)
            return redirect('core:receive', self.kwargs['group'], self.kwargs['denomination'], receive_request_id)


class KYCViewPlace(TemplateView):
    template_name = 'core/common/kyc/index.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Faites vous connaître'
        context['page_name'] = 'kyc'

        return context


class KYCViewService(TemplateView):
    template_name = 'core/common/kyc/index.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Faites vous connaître'
        context['page_name'] = 'kyc'

        return context
