import json
from datetime import datetime
from itertools import chain

from django.contrib.sites.models import Site
from django.core import signing
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from apps.location.models import City, Town, Borough
from apps.religion.models import Place, Group, Denomination, PlaceActivity
from apps.retirement.models import RetirementTravelInstance
from lib.config import django_logger
from lib.middleware import api_call, str_to_json, get_geo_loc, get_occurrences, nearest_date, get_class_instance, get_activity, JSONEncoder
from lib.resources import confs


def location_search(request):
    get_data = request.GET
    location = get_data.get('location', None)

    status = False
    results = []

    cities_list = City.objects.filter(name__icontains = location).values()
    city = [dict(x, type = 'city') for x in cities_list]

    towns_list = Town.objects.filter(name__icontains = location).values()
    town = [dict(x, type = 'town') for x in towns_list]

    boroughs_list = Borough.objects.filter(name__icontains = location).values()
    borough = [dict(x, type = 'borough') for x in boroughs_list]

    values = sorted(chain(city, town, borough), key = lambda instance: instance['name'])

    if values:
        status = True
        results = values

    data = {
        'status': status,
        'results': results,
    }

    return JsonResponse(data, safe = False)


def place_search(request):
    get_data = request.GET
    place_name = get_data.get('place_name', '')
    place_type = get_data.get('place_type', '')
    denomination = get_data.get('denomination', '')

    status = False
    results = []

    place = Place.objects.posted_by_denomination(denomination)
    if len(place_type) > 0:
        place = place.filter(place_type = place_type)
    place = place.filter(name__icontains = place_name)

    if place:
        status = True
        results = list(place.values())

    data = {
        'status': status,
        'results': results,
    }

    return JsonResponse(data, safe = False)


@csrf_exempt
def get_location(request):
    latitude = request.POST.get('latitude', None)
    longitude = request.POST.get('longitude', None)

    if latitude and longitude:
        geo_api = confs.get('api')['GEO_API']

        url = geo_api['url'].replace('{latitude}', latitude).replace('{longitude}', longitude)
        place = api_call(url = url, method = geo_api['method'], timeout = 5)

        if not hasattr(place, 'status_code'):
            out_msg = place["message"]
            django_logger.error("GEO API NOT REACHABLE !!! => {}".format(out_msg))

            return JsonResponse(None, safe = False)
        else:
            r = place.text
            return JsonResponse(json.loads(r))


def get_retirement_travel(request):
    get_data = request.GET
    instance_id = get_data.get('instance', '')
    group_id = get_data.get('group', '')
    denomination_id = get_data.get('denomination', '')

    group = get_class_instance(Group, pk = group_id)
    denomination = get_class_instance(Denomination, pk = denomination_id)

    status = False

    instance = get_class_instance(RetirementTravelInstance, pk = instance_id)
    template = ''

    if instance:
        status = True
        # template = TemplateResponse(request, 'core/denomination/partials/modals/retirement_travel_instance.html', context = instance)
        context = {
            'request': request,
            'group': group,
            'denomination': denomination,
            'instance': instance
        }
        template = render_to_string('core/denomination/partials/modals/retirement_travel_instance.html', context)

    data = {
        'status': status,
        'travel_title': '{} ({})'.format(instance.retirement_travel.title, instance.departure_date.year),
        'template': template,
    }

    return JsonResponse(data, safe = False)


@csrf_exempt
def get_next_celebration(request):
    post = request.POST
    group = post.get('group', None)
    denomination = post.get('denomination', None)
    geo_loc = str_to_json(post.get('address', ""))

    from apps.religion.models import PlaceActivity
    place_activities = PlaceActivity.objects.filter(place__denomination = denomination, activity__activity_type__codename = "messe")

    search_result = get_geo_loc(PlaceActivity, geo_loc, place_activities, by = "address", place = True)

    occ = [get_occurrences(x.id, 'next') for x in search_result if get_occurrences(x.id, 'next') and x.recurrences is not None]

    next_occ = nearest_date(occ, datetime.today().date())
    template = ''

    if next_occ is None:
        status = False
    else:
        status = True
        domain = Site.objects.get_current().domain
        pa = next_occ.get('place_activity', {})
        date = next_occ.get('date', None)
        pa_params = {'frame': True, 'type': 'activity', 'id': pa.activity.pk, 'date': date.strftime('%d-%m-%Y')}
        encoded_params = signing.dumps(pa_params)
        # next_occ['url'] = url_with_querystring(domain, encoded_params)
        next_occ['url'] = "{}?r={}".format(domain, encoded_params)
        next_occ['request'] = request
        template = render_to_string('core/denomination/partials/modals/next_celebration.html', next_occ)

    data = {
        'status': status,
        'template': template,
    }

    return JsonResponse(data)


def open_frame(request, params):
    if bool(params) is True:
        request_type = params.get('type')
        pa_id = params.get('id')
        date = params.get('date')
        if request_type == 'activity':
            place_activity = get_class_instance(PlaceActivity, pa_id)
            activity_date = date
            occ = get_activity(place_activity, activity_date)
            next_occ = nearest_date(occ, datetime.today().date())
            next_occ['request'] = request
            next_occ['frame'] = True

            template = ''

            if next_occ is None:
                status = False
            else:
                status = True
                template = render_to_string('core/denomination/partials/modals/next_celebration.html', next_occ)

            data = {
                'status': status,
                'template': template,
            }

            return json.dumps(data)

        print("get_data => {}".format(get_data))
        instance_id = get_data.get('instance', '')
        group_id = get_data.get('group', '')
        denomination_id = get_data.get('denomination', '')
        frame = get_data.get('open_frame', '')
        print("frame => {}".format(frame))
        return

        group = get_class_instance(Group, pk = group_id)
        denomination = get_class_instance(Denomination, pk = denomination_id)

        status = False

        instance = get_class_instance(RetirementTravelInstance, pk = instance_id)
        template = ''

        if instance:
            status = True
            # template = TemplateResponse(request, 'core/denomination/partials/modals/retirement_travel_instance.html', context = instance)
            context = {
                'request': request,
                'group': group,
                'denomination': denomination,
                'instance': instance
            }
            template = render_to_string('core/denomination/partials/modals/retirement_travel_instance.html', context)

        data = {
            'status': status,
            'travel_title': '{} ({})'.format(instance.retirement_travel.title, instance.departure_date.year),
            'template': template,
        }

        return JsonResponse(data, safe = False)
