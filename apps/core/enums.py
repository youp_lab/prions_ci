from django.utils.translation import gettext as _
from model_utils import Choices

ACTIVATED = True
INACTIVATED = False

STATE_CHOICES = Choices((ACTIVATED, _("Affiché")), (INACTIVATED, _("Masqué")))

ORGANIZATION = "organization"
SERVICE = "service"
ORGANIZATION_CATEGORY_CHOICES = Choices((ORGANIZATION, _("Organisation")), (SERVICE, _("Service")))

ACTIVITY = "activity"
EVENT = "event"
EVENT_TYPE_CHOICES = Choices((ACTIVITY, _("Activité")), (EVENT, _("Evénement")))

DRAFT = None
HIDDEN = False
PUBLISHED = True
STATUS_CHOICES = ((DRAFT, _("Brouillon")), (HIDDEN, "Non publié"), (PUBLISHED, "Publié"))

GIFT = "gift"
CASH = "cash"
RECEIVE_TYPE_CHOICES = Choices((GIFT, _("Nature")), (CASH, _("Numéraire")))

DATE = "date"
TEXT = "text"
SPEC_TYPE_CHOICES = Choices((TEXT, _("Texte")), (DATE, _("Date")))

A_SELF = "_self"
A_BLANK = "_blank"
A_PARENT = "_parent"
# A_TOP = "_top"
TARGET_CHOICES = ((A_SELF, "Même onglet"), (A_BLANK, _("Nouvel Onglet")), (A_PARENT, "Onglet Parent"))

CREATION_DATE = "creation"
CONSECRATION_DATE = "consecration"
PLACE_TYPE_CHOICES = Choices((CREATION_DATE, _("Date de création")), (CONSECRATION_DATE, _("Date de consécration")))

PARENT = "parent"
FRIEND = "friend"
ACQUAINTANCE = "acquaintance"
ANONYMOUS = "anonymous"
DEATH_LINK_CHOICES = Choices((PARENT, _("Parent")), (FRIEND, _("Ami")), (ACQUAINTANCE, _("Connaissance")), (ANONYMOUS, _("Anonyme")))
