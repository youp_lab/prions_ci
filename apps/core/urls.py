from django.urls import path, include, re_path

from apps.core.views import *

app_name = 'core'

urlpatterns = [
    path('', IndexView.as_view(), name = 'index'),
    path('qui-sommes-nous', AboutUsView.as_view(), name = 'about-us'),
    path('faite-vous-connaitre', KYCView.as_view(), name = 'kyc'),
    path('faite-vous-connaitre/denomination', KYCViewDenomination.as_view(), name = 'kyc-denomination'),
    path('faite-vous-connaitre/lieu-de-culte', KYCViewPlace.as_view(), name = 'kyc-place'),
    path('faite-vous-connaitre/service', KYCViewService.as_view(), name = 'kyc-service'),
]

# AJAX PATTERNS
urlpatterns += [
    re_path(r'^ajax/location-search$', location_search, name = 'location-search'),
    re_path(r'^ajax/place-search$', place_search, name = 'place-search'),
    re_path(r'^ajax/get-location$', get_location, name = 'get-location'),
    re_path(r'^ajax/get-next-celebration$', get_next_celebration, name = 'get-next'),
    re_path(r'^ajax/add-activity-request$', ActivityRequestView.as_view(), name = 'add-activity-request'),
    re_path(r'^ajax/add-contact-request$', ContactRequestView.as_view(), name = 'add-contact-request'),
    re_path(r'^ajax/add-receive-request$', ReceiveRequestCreateView.as_view(), name = 'add-receive-request'),
    re_path(r'^ajax/get-retirement-travel$', get_retirement_travel, name = 'get-retirement-travel'),
]

# FRAMES
urlpatterns += [
    re_path(r'^frame/(?P<request_type>\w+)$', open_frame, name = 'open-frame'),
]

# MAIN ROUTES
urlpatterns += [
    path('<slug:group>', DenominationListView.as_view(), name = 'group'),
    path('<slug:group>/necrologie', NecrologyListView.as_view(), name = 'necrology-list-group'),
    # path('<slug:group>/articles', ArticleListGroupView.as_view(), name = 'articles_group'),
    # path('<slug:group>/announces', views.AnnounceListGroupView.as_view(), name = 'announces_group'),
    # path('<slug:group>/calendar-view', CalendarPartialView.as_view(), name = 'calendar_group_view'),
    path('<slug:group>/', include([
        path('<slug:denomination>', DenominationHomeView.as_view(), name = 'denomination'),
        # NEWS
        path('<slug:denomination>/articles', ArticleListView.as_view(), name = 'article-list'),
        path('<slug:denomination>/article/<slug:slug>', ArticleDetailView.as_view(), name = 'article'),
        path('<slug:denomination>/annonces', AnnounceListView.as_view(), name = 'announce-list'),
        path('<slug:denomination>/annonce/<slug:slug>', AnnounceDetailView.as_view(), name = 'announce'),

        # FAITH
        path('<slug:denomination>/foi', DenominationFaithListView.as_view(), name = 'faith-list'),
        path('<slug:denomination>/foi/<slug:slug>', DenominationFaithView.as_view(), name = 'faith'),

        # DIRECTORY
        path('<slug:denomination>/je-recherche', DirectoryLookupView.as_view(), name = 'directory-lookup'),
        path('<slug:denomination>/directory-search-full', DirectorySearchFullView.as_view(), name = 'directory-search-full'),
        path('<slug:denomination>/annuaire/search', DirectoryWishSearchView.as_view(), name = 'annuaire-search'),
        path('<slug:denomination>/annuaire/<slug:slug>', PlaceView.as_view(), name = 'place'),
        # path('<slug:denomination>/annuaire/<slug:slug>/annonces', PlaceView.as_view(), name = 'place-announces'),
        path('<slug:denomination>/annuaire/<slug:place>/annonces/<slug:announce>', PlaceAnnounceDetailView.as_view(), name = 'place-announce'),
        path('<slug:denomination>/annuaire/<slug:place>/communauté/<slug:community>', PlaceCommunityDetailView.as_view(), name = 'place-community'),
        path('<slug:denomination>/annuaire/<slug:place>/remonter-une-erreur', ContactRequestView.as_view(), name = 'error-report'),
        # FORM ACTIVITY DIRECT LINK
        path('<slug:denomination>/annuaire/<slug:slug>/priere/<int:pk>', PlaceView.as_view(), name = 'prayer'),
        path('<slug:denomination>/evenements/<slug:slug>', EventView.as_view(), name = 'event'),
        path('<slug:denomination>/services/<slug:slug>', ServiceView.as_view(), name = 'service'),

        # TOOLS
        path('<slug:denomination>/faire-une-demande', ActivityRequestView.as_view(), name = 'activity-request'),
        path('<slug:denomination>/faire-une-demande/p/<slug:place>', ActivityRequestView.as_view(),
             name = 'activity-request-from-place'),
        path('<slug:denomination>/faire-une-demande/n/<slug:necrology>', ActivityRequestView.as_view(),
             name = 'activity-request-from-necrology'),
        path('<slug:denomination>/contacter', ContactRequestView.as_view(), name = 'contact-request'),

        path('<slug:denomination>/demandes', ReceiveRequestListView.as_view(), name = 'receive-list'),
        path('<slug:denomination>/demandes/<int:pk>', ReceiveRequestDetailView.as_view(), name = 'receive'),
        path('<slug:denomination>/solliciter-une-aide', ReceiveRequestCreateView.as_view(), name = 'receive-create'),
        path('<slug:denomination>/demandes/<int:pk>/donner', ReceiveRequestDonationView.as_view(), name = 'receive-donate'),

        # RETIREMENT
        path('<slug:denomination>/pelerinages-retraites', RetirementPlaceListView.as_view(), name = 'retirement-place-list'),
        path('<slug:denomination>/retirement-search', RetirementSearchView.as_view(), name = 'retirement-search'),
        path('<slug:denomination>/pelerinages-retraites/recherche', RetirementTravelSearchView.as_view(), name = 'retirement-travel-search'),
        path('<slug:denomination>/pelerinages-retraites/<slug:slug>', RetirementPlaceDetailView.as_view(), name = 'retirement-place'),

        # NECROLOGY
        path('<slug:denomination>/necrologie', NecrologyListView.as_view(), name = 'necrology-list'),
        path('<slug:denomination>/necrologie/search', NecrologyListView.as_view(), name = 'necrology-search'),
        path('<slug:denomination>/necrologie/<slug:slug>', NecrologyDetailView.as_view(), name = 'necrology'),

        # TESTIMONY
        path('<slug:denomination>/temoignages', TestimonyListView.as_view(), name = 'testimony-list'),
        path('<slug:denomination>/testimony/<slug:slug>', TestimonyDetailView.as_view(), name = 'testimony'),

        # MEDIA
        path('<slug:denomination>/ebook', EbookListView.as_view(), name = 'ebook-ist'),
        # path('<slug:denomination>/ebook/<slug:slug>', EbookDetailView.as_view(), name = 'ebook'),
        path('<slug:denomination>/mediatheque', GalleryListView.as_view(), name = 'media-list'),
        path('<slug:denomination>/mediatheque/<slug:type>', GalleryView.as_view(), name = 'media-type'),
        path('<slug:denomination>/mediatheque/<slug:type>/<int:pk>', GalleryDetailView.as_view(), name = 'media'),
    ])),
]
