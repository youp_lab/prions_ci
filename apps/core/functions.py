def register_menu_by_model(group_slug = 'christianisme', model_name = None, parent_field = None):
    if parent_field is None:
        parent_field = ['name', 'slug', 'group']
    return model_name.objects.published().filter(group__slug = group_slug).values(*parent_field)
