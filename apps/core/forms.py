from captcha.fields import CaptchaField
from crispy_forms.helper import FormHelper
from django import forms
from django.utils.translation import gettext as _

from apps.core.enums import INACTIVATED
from apps.posts.models import NecrologyCondolences
from apps.religion.models import Denomination, Place, ActivityRequest, ContactRequest, ReceiveRequest, ReceiveRequestImage, Donation
from apps.retirement.models import *


class ActivityRequestForm(forms.ModelForm):
    date_range = forms.CharField(
        label = 'Periode', required = False,
        widget = forms.TextInput(attrs = {'placeholder': 'dd/mm/yyyy - dd/mm/yyyy', 'class': 'tleft select-date-range', 'autocomplete': 'off'})
    )

    class Meta:
        model = ActivityRequest
        fields = '__all__'

    def __init__(self, group = None, denomination = None, place = None, necrology = None, *args, **kwargs):
        super(ActivityRequestForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'
        place_denomination = Denomination.objects.get(slug = denomination)
        self.fields['place'].queryset = Place.objects.posted_by_denomination(place_denomination)
        self.fields['place'].label = place_denomination.denomination_details.place_type


class ContactRequestForm(forms.ModelForm):
    class Meta:
        model = ContactRequest
        fields = '__all__'

    def __init__(self, group = None, denomination = None, *args, **kwargs):
        super(ContactRequestForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'
        place_denomination = Denomination.objects.get(slug = denomination)
        self.fields['place'].queryset = Place.objects.posted_by_denomination(place_denomination)
        self.fields['place'].label = place_denomination.denomination_details.place_type


class ReceiveRequestImageForm(forms.ModelForm):
    class Meta:
        model = ReceiveRequestImage
        fields = ['image']


class ReceiveRequestForm(forms.ModelForm):
    class Meta:
        model = ReceiveRequest
        fields = '__all__'

    def __init__(self, group = None, denomination = None, *args, **kwargs):
        super(ReceiveRequestForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'


class DonationForm(forms.ModelForm):
    class Meta:
        model = Donation
        fields = '__all__'
        exclude = ('receive_request',)

    def __init__(self, group = None, denomination = None, *args, **kwargs):
        super(DonationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'


class RetirementPlaceForm(forms.Form):
    name = forms.CharField(
        label = 'Mot clef', required = False, widget = forms.TextInput(attrs = {'placeholder': 'Saisissez votre recherche'}), max_length = 150
    )
    target = forms.ModelChoiceField(
        label = 'Pour qui ?', required = False, queryset = RetirementTarget.objects.all(), empty_label = _('Indifférent')
    )
    reason = forms.ModelChoiceField(
        label = 'Pourquoi ?', required = False, queryset = RetirementReason.objects.all(), empty_label = _('Indifférent')
    )
    city = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = RetirementCity.objects.all(), empty_label = _('Indifférent')
    )
    environment = forms.ModelChoiceField(
        label = 'Environnement', required = False, queryset = RetirementEnvironment.objects.all(), empty_label = _('Indifférent')
    )
    convenience_place = forms.ModelMultipleChoiceField(
        label = 'Particularité', required = False, widget = forms.CheckboxSelectMultiple, queryset = RetirementConvenience.objects.all()
    )

    def __init__(self, *args, **kwargs):
        super(RetirementPlaceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'


class RetirementTravelForm(forms.Form):
    type = forms.ModelChoiceField(
        label = 'Type', required = False, queryset = RetirementType.objects.all(), empty_label = _('Indifférent')
    )
    name = forms.CharField(
        label = 'Mot clef', required = False, widget = forms.TextInput(attrs = {'placeholder': 'Saisissez votre recherche'}), max_length = 150
    )
    target = forms.ModelChoiceField(
        label = 'Pour qui ?', required = False, queryset = RetirementTarget.objects.all(), empty_label = _('Indifférent')
    )
    reason = forms.ModelChoiceField(
        label = 'Pourquoi ?', required = False, queryset = RetirementReason.objects.all(), empty_label = _('Indifférent')
    )
    city = forms.ModelChoiceField(
        label = 'Ville', required = False, queryset = RetirementCity.objects.all(), empty_label = _('Indifférent')
    )
    date_range = forms.CharField(
        label = 'Periode', required = False,
        widget = forms.TextInput(attrs = {'placeholder': 'dd/mm/yyyy - dd/mm/yyyy', 'class': 'tleft select-date-range', 'autocomplete': 'off'})
    )
    environment = forms.ModelChoiceField(
        label = 'Environnement', required = False, queryset = RetirementEnvironment.objects.all(), empty_label = _('Indifférent')
    )
    convenience_travel = forms.ModelMultipleChoiceField(
        label = 'Particularité', required = False, widget = forms.CheckboxSelectMultiple, queryset = RetirementConvenience.objects.all()
    )

    def __init__(self, *args, **kwargs):
        super(RetirementTravelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'


class NecrologyCondolencesForm(forms.ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = NecrologyCondolences
        fields = '__all__'
        widgets = {
            'necrology': forms.HiddenInput(),
            'status': forms.HiddenInput(),
        }

    def __init__(self, necrology = None, *args, **kwargs):
        super(NecrologyCondolencesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'
        self.fields['necrology'].initial = necrology
        self.fields['status'].initial = INACTIVATED


class KYFDenominationForm(forms.ModelForm):
    class Meta:
        model = Denomination
        fields = '__all__'
        exclude = ['content']
        widgets = {
            'status': forms.HiddenInput(),
        }

    def __init__(self, necrology = None, *args, **kwargs):
        super(KYFDenominationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form form-horizontal'
        self.helper.label_class = 'col-xs-4 col-sm-4 col-md-4 col-lg-4 col-form-label text-sm-left text-md-right nott'
        self.helper.field_class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7'
        self.fields['status'].initial = INACTIVATED
