from django.db import models
from sitetree.models import TreeBase, TreeItemBase

from apps.core.enums import TARGET_CHOICES, A_SELF
from lib.middleware import Monitor


class Menu(Monitor):
    id = models.AutoField(primary_key = True)
    denomination = models.ForeignKey('religion.Denomination', models.CASCADE, blank = True, null = True)
    name = models.CharField(max_length = 60, default = '', verbose_name = 'Nom du menu')

    class Meta:
        verbose_name = 'Menu'
        verbose_name_plural = 'Menus'


class MenuItem(Monitor):
    id = models.AutoField(primary_key = True)
    menu = models.ForeignKey(Menu, models.SET_NULL, blank = True, null = True, verbose_name = 'Menu')
    parent = models.ForeignKey('self', models.SET_NULL, blank = True, null = True, verbose_name = 'Onglet parent')
    name = models.CharField(max_length = 60, verbose_name = 'Nom de l\'onglet')
    url = models.URLField(verbose_name = 'URL')
    root = models.BooleanField(verbose_name = 'Racine ?')
    icon_class = models.CharField(max_length = 60, verbose_name = 'Classe d\'icônes')
    validators = models.CharField(max_length = 255, blank = True, verbose_name = 'Validateurs')
    target = models.CharField(max_length = 15, choices = TARGET_CHOICES, default = A_SELF, verbose_name = 'Cible')

    class Meta:
        verbose_name = 'Onglet'
        verbose_name_plural = 'Onglets'


class MyTree(TreeBase):
    """This is my custom tree model.
    And here I added `denomination` to all fields existing in `TreeBase`.

    """
    denomination = models.ForeignKey('religion.Denomination', models.CASCADE, default = 1, blank = True, null = True)

