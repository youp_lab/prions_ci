from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Column, Submit, Button
from django import forms
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from apps.organization.models import Organization
from apps.p_event.models import Event
from apps.person.models import Responsible
from apps.religion.models import PlaceActivity, PlaceActivityDetails, Place, PlaceAnnounce, PlaceCommunity, PlaceOfficiant, PlaceOrganizational, \
    PlaceCategory, PlaceTag
from apps.services.models import Service


class PlaceOrganizationalForm(forms.ModelForm):
    class Meta:
        model = PlaceOrganizational
        fields = '__all__'
        widgets = {
            'place': forms.HiddenInput(),
        }


class PlaceForm(forms.ModelForm):
    class Meta:
        model = Place
        fields = '__all__'
        widgets = {
            'denomination': forms.HiddenInput(),
            'slug': forms.HiddenInput(),
            'name': forms.TextInput(attrs = {'disabled': True}),
            'logo': forms.HiddenInput(),
            'place_type': forms.Select(attrs = {'disabled': True}),
            'city': forms.Select(attrs = {'disabled': True}),
            'town': forms.Select(attrs = {'disabled': True}),
            'borough': forms.Select(attrs = {'disabled': True}),
            'e_shop': forms.TextInput(attrs = {'disabled': True}),
            # 'responsible': forms.Select(attrs = {'class': 'select2'}),
            'contact': forms.TextInput(attrs = {'type': 'tel'}),
            'fax': forms.TextInput(attrs = {'type': 'tel'}),
            'status': forms.HiddenInput(),
            # 'creation_date': forms.DateInput(attrs = {'type': 'date'}),
        }

    def __init__(self, place = None, *args, **kwargs):
        super(PlaceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('administrator:place')
        self.fields['creation_date'].label = place.place_type.get_creation_date_display
        self.helper.layout = Layout(
            Row(
                Column('place_type'),
                Column('name'),
            ),
            Row(
                Column('slogan'),
                Column('logo'),
            ),
            Row(
                Column('city'),
                Column('town'),
                Column('borough'),
            ),
            'location_detail',
            # BaseInput('Get location value', '...'),
            Row(
                Column(Button('Get Location', 'Me localiser', css_class = 'btn-info get-location')),
                Column('lat'),
                Column('lon'),
            ),
            'map',
            'e_shop',
            # 'diocese',
            'theme',
            'description',
            'image',
            'file_url',
            'responsible',
            # 'organization',
            Row(
                Column('opening_time'),
                Column('contact'),
                Column('fax'),
            ),
            Row(
                Column('website'),
                Column('email'),
                Column('address'),
            ),
            Row(
                Column('seats'),
                Column('area'),
            ),
            'creation_date',
            'denomination',
            'status',
            Submit('submit', 'Enregistrer', css_class = 'btn btn-info')
        )


class PlaceOfficiantForm(forms.ModelForm):
    class Meta:
        model = PlaceOfficiant
        fields = '__all__'
        widgets = {
            'type': forms.Select(attrs = {'class': 'select2'}),
            'place': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(PlaceOfficiantForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class PlaceCategoryForm(forms.ModelForm):
    class Meta:
        model = PlaceCategory
        fields = '__all__'
        widgets = {
            'place': forms.HiddenInput(),
            'slug': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(PlaceCategoryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class PlaceTagForm(forms.ModelForm):
    class Meta:
        model = PlaceTag
        fields = '__all__'
        widgets = {
            'place': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(PlaceTagForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class PlaceAnnounceForm(forms.ModelForm):
    published_on = forms.DateField(input_formats = ['%d-%m-%Y'], widget = forms.DateInput(attrs = {}, format = '%d-%m-%Y'))
    end_publication = forms.DateField(input_formats = ['%d-%m-%Y'], widget = forms.DateInput(attrs = {}, format = '%d-%m-%Y'))

    class Meta:
        model = PlaceAnnounce
        fields = '__all__'
        widgets = {
            'group': forms.HiddenInput(),
            'denomination': forms.HiddenInput(),
            'place': forms.HiddenInput(),
            'category': forms.Select(attrs = {'class': 'select2'}),
            'tags': forms.Select(attrs = {'class': 'select2', 'multiple': True}),
            'slug': forms.HiddenInput(),
            # 'published_on': forms.DateInput(attrs = {'type': 'date'}, format = '%d-%m-%Y'),
            # 'end_publication': forms.DateInput(attrs = {'type': 'date'}),
        }

    def __init__(self, *args, **kwargs):
        super(PlaceAnnounceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.fields['end_publication'].required = False
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class PlaceCommunityForm(forms.ModelForm):
    published_on = forms.DateField(input_formats = ['%d-%m-%Y'], widget = forms.DateInput(attrs = {}, format = '%d-%m-%Y'))
    end_publication = forms.DateField(input_formats = ['%d-%m-%Y'], widget = forms.DateInput(attrs = {}, format = '%d-%m-%Y'))

    class Meta:
        model = PlaceCommunity
        fields = '__all__'
        widgets = {
            'group': forms.HiddenInput(),
            'denomination': forms.HiddenInput(),
            'place': forms.HiddenInput(),
            'slug': forms.HiddenInput(),
            'category': forms.Select(attrs = {'class': 'select2'}),
            'tags': forms.Select(attrs = {'class': 'select2', 'multiple': True}),
            # 'published_on': forms.DateInput(attrs = {'type': 'date'}, format = '%d-%m-%Y'),
            # 'end_publication': forms.DateInput(attrs = {'type': 'date'}),
        }

    def __init__(self, *args, **kwargs):
        super(PlaceCommunityForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.fields['end_publication'].required = False
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class PlaceActivityForm(forms.ModelForm):
    class Meta:
        model = PlaceActivity
        fields = '__all__'
        widgets = {
            'activity': forms.Select(attrs = {'class': 'select2'}),
            'start_time': forms.TimeInput(attrs = {'type': 'time'}),
            'end_time': forms.TimeInput(attrs = {'type': 'time'}),
            'place': forms.HiddenInput(),
        }

    def __init__(self, place = None, *args, **kwargs):
        super(PlaceActivityForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal form-element'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        # self.fields['place'].initial = place


class PlaceActivityDetailsForm(forms.ModelForm):
    date = forms.DateField(input_formats = ['%d-%m-%Y'], widget = forms.DateInput(attrs = {}, format = '%d-%m-%Y'))

    class Meta:
        model = PlaceActivityDetails
        fields = '__all__'
        widgets = {
            'officiant': forms.Select(attrs = {'class': 'select2'}),
            # 'date': forms.DateInput(attrs = {'type': 'date'}),
            'place_activity': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(PlaceActivityDetailsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = '__all__'
        widgets = {
            'place': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super(OrganizationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))


class ResponsibleForm(forms.ModelForm):
    organization = forms.ModelChoiceField(
        label = 'Organisation', required = True, widget = forms.Select(attrs = {'class': 'select2'}), queryset = Organization.objects.none(),
        empty_label = _('Séléctionner une organisation')
    )

    class Meta:
        model = Responsible
        fields = '__all__'

    def __init__(self, place, organization, *args, **kwargs):
        super(ResponsibleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.fields['organization'].queryset = Organization.objects.filter(place = place)
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))

    def clean_organization(self):
        organization = self.cleaned_data['organization']
        return organization


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
        widgets = {
            'event_type': forms.Select(attrs = {'class': 'select2'}),
            'start_date': forms.DateInput(attrs = {'type': 'date'}),
            'end_date': forms.DateInput(attrs = {'type': 'date'}),
            'start_time': forms.TimeInput(attrs = {'type': 'time'}),
            'end_time': forms.TimeInput(attrs = {'type': 'time'}),
            'group': forms.HiddenInput(),
            'denomination': forms.HiddenInput(),
            'place': forms.HiddenInput(),
            'slug': forms.HiddenInput(),
            'city': forms.HiddenInput(),
            'town': forms.HiddenInput(),
            'borough': forms.HiddenInput(),
            'is_popup': forms.HiddenInput(),
        }

    def __init__(self, place = None, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))

        """self.helper.layout = Layout(
            'event_type',
            'title',
            'image',
            'description',
            Row(
                Column('start_date'),
                Column('end_date')
            ),
            Row(
                Column('start_time'),
                Column('end_time')
            ),
            'is_full_day_event',
            'flier',
            'location_detail',
            'is_popup',
            'order_link',
            'price',
            'particularity',
        )"""


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = '__all__'
        widgets = {
            'service_type': forms.Select(attrs = {'class': 'select2'}),
            'group': forms.HiddenInput(),
            'denomination': forms.HiddenInput(),
            'place': forms.HiddenInput(),
            'slug': forms.HiddenInput(),
            # 'city': forms.HiddenInput(),
            # 'town': forms.HiddenInput(),
            # 'borough': forms.HiddenInput(),
        }

    def __init__(self, place = None, *args, **kwargs):
        super(ServiceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_show_errors = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-form-label col-3'
        self.helper.field_class = 'col-9'
        self.helper.add_input(Submit('submit', 'Enregistrer', css_class = 'btn btn-info'))
