from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from apps.administrator.views import *

app_name = 'administrator'

urlpatterns = [
    # path('', IndexView.as_view(), name = 'index'),
    # path('login/', LoginView.as_view(), name = 'login'),
    path('login/', CustomLoginView.as_view(), name = 'login'),
    path('logout/', LogoutView.as_view(next_page = '/administrator/login/?next=/administrator/'), name = 'logout'),

    # PLACE
    path('', PlaceView.as_view(), name = 'place'),

    # ACTIVITY
    path('activity/', ActivityCreateView.as_view(), name = 'activity'),
    path('activity/<int:pk>', ActivityUpdateView.as_view(), name = 'activity-update'),
    path('activity/schedule/<int:pk>/create', ActivityDetailsCreateView.as_view(), name = 'schedule-create'),
    path('activity/schedule/<int:pk>/update', ActivityDetailsUpdateView.as_view(), name = 'schedule-update'),

    # OFFICIANT
    path('officiants/', PlaceOfficiantListView.as_view(), name = 'officiant-list'),
    path('officiant/', PlaceOfficiantCreateView.as_view(), name = 'officiant-create'),
    path('officiants/<int:pk>', PlaceOfficiantUpdateView.as_view(), name = 'officiant-update'),

    # ORGANIZATION
    path('organizations/', OrganizationListView.as_view(), name = 'organization-list'),
    path('organization/', OrganizationCreateView.as_view(), name = 'organization-create'),
    path('organizations/<int:pk>', OrganizationUpdateView.as_view(), name = 'organization-update'),

    # PERSON
    path('persons/', ResponsibleListView.as_view(), name = 'person-list'),
    path('person/', ResponsibleCreateView.as_view(), name = 'person-create'),
    path('persons/<int:pk>', ResponsibleUpdateView.as_view(), name = 'person-update'),

    # CATEGORY
    path('categories/', CategoryListView.as_view(), name = 'category-list'),
    path('category/', CategoryCreateView.as_view(), name = 'category-create'),
    path('categories/<int:pk>', CategoryUpdateView.as_view(), name = 'category-update'),

    # TAG
    path('tags/', TagListView.as_view(), name = 'tag-list'),
    path('tag/', TagCreateView.as_view(), name = 'tag-create'),
    path('tags/<int:pk>', TagUpdateView.as_view(), name = 'tag-update'),

    # ANNOUNCE
    path('announces/', AnnounceListView.as_view(), name = 'announce-list'),
    path('announce/', AnnounceCreateView.as_view(), name = 'announce-create'),
    path('announces/<int:pk>', AnnounceUpdateView.as_view(), name = 'announce-update'),

    # LIFE
    path('communities/', CommunityListView.as_view(), name = 'community-list'),
    path('community/', CommunityCreateView.as_view(), name = 'community-create'),
    path('communities/<int:pk>', CommunityUpdateView.as_view(), name = 'community-update'),

    # EVENT
    path('events/', EventListView.as_view(), name = 'event-list'),
    path('event/', EventCreateView.as_view(), name = 'event-create'),
    path('events/<int:pk>', EventUpdateView.as_view(), name = 'event-update'),

    # SERVICE
    path('services/', ServiceListView.as_view(), name = 'service-list'),
    path('service/', ServiceCreateView.as_view(), name = 'service-create'),
    path('services/<int:pk>', ServiceUpdateView.as_view(), name = 'service-update'),
]
