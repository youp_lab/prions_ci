from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    place = models.ForeignKey('religion.Place', models.SET_NULL, blank = True, null = True, verbose_name = 'Lieu de prière')

    class Meta:
        verbose_name = 'Profil'
        verbose_name_plural = 'Profils'


@receiver(post_save, sender = User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user = instance)
