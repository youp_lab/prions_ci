from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.administrator.models import *

for model in apps.get_app_config('administrator').models.values():
    admin.site.register(model)


class ProfileAdmin(admin.ModelAdmin):
    list_filter = ('place',)
    autocomplete_fields = ['user', 'place']
    list_display = ('user', 'place',)


admin.site.unregister(Profile)
admin.site.register(Profile, ProfileAdmin)
