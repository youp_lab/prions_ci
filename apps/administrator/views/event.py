from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import EventForm
from apps.p_event.models import Event
from apps.religion.models import Place


class EventListView(LoginRequiredMixin, ListView):
    model = Event
    template_name = 'administrator/event/event_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return Event.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'event'
        context['page_name'] = 'event_list'
        context['page_title'] = 'Liste des événements'

        return context


class EventCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Event
    template_name = 'administrator/event/event_create.html'
    form_class = EventForm
    login_url = '/administrator/login/'
    success_url = '/administrator/events/'
    success_message = "Evénement ajouté avec succès"

    def get_initial(self):
        initial = super().get_initial()
        initial['place'] = get_object_or_404(Place, slug = 'sainte-cecile')
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'event'
        context['page_name'] = 'event_create'
        context['page_title'] = 'Créer un événement'

        return context


class EventUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Event
    template_name = 'administrator/event/event_update.html'
    form_class = EventForm
    login_url = '/administrator/login/'
    success_url = '/administrator/events/'
    success_message = "Evénement mis à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'event'
        context['page_name'] = 'event_update'
        context['page_title'] = 'Mettre à jour un événement'

        return context
