from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import PlaceAnnounceForm
from apps.religion.models import Group, Denomination, Place, PlaceAnnounce


class AnnounceListView(LoginRequiredMixin, ListView):
    model = PlaceAnnounce
    template_name = 'administrator/announce/announce_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceAnnounce.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'announce'
        context['page_name'] = 'announce_list'
        context['page_title'] = 'Liste des annonces'

        return context


class AnnounceCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceAnnounce
    template_name = 'administrator/announce/announce_create.html'
    form_class = PlaceAnnounceForm
    login_url = '/administrator/login/'
    success_url = '/administrator/announces/'
    success_message = "Annonce ajoutée avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        initial['denomination'] = get_object_or_404(Denomination, slug = self.place.denomination.slug)
        initial['group'] = get_object_or_404(Group, slug = self.place.denomination.group.slug)
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'announce'
        context['page_name'] = 'announce_create'
        context['page_title'] = 'Créer une annonce'

        return context


class AnnounceUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceAnnounce
    template_name = 'administrator/announce/announce_update.html'
    form_class = PlaceAnnounceForm
    login_url = '/administrator/login/'
    success_url = '/administrator/announces/'
    success_message = "Annonce mise à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'announce'
        context['page_name'] = 'announce_update'
        context['page_title'] = 'Mettre à jour une annonce'

        return context
