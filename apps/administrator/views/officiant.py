from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import PlaceOfficiantForm
from apps.religion.models import Place, PlaceOfficiant


class PlaceOfficiantListView(LoginRequiredMixin, ListView):
    model = PlaceOfficiant
    template_name = 'administrator/officiant/officiant_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceOfficiant.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'officiant'
        context['page_name'] = 'officiant_list'
        context['page_title'] = 'Liste des officiants'

        return context


class PlaceOfficiantCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceOfficiant
    template_name = 'administrator/officiant/officiant_create.html'
    form_class = PlaceOfficiantForm
    login_url = '/administrator/login/'
    success_url = '/administrator/officiants/'
    success_message = "Officiant ajouté avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        initial['place'] = get_object_or_404(Place, slug = 'sainte-cecile')
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'officiant'
        context['page_name'] = 'officiant_create'
        context['page_title'] = 'Créer un officiant'

        return context


class PlaceOfficiantUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceOfficiant
    template_name = 'administrator/officiant/officiant_update.html'
    form_class = PlaceOfficiantForm
    login_url = '/administrator/login/'
    success_url = '/administrator/officiants/'
    success_message = "Officiant mis à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'officiant'
        context['page_name'] = 'officiant_update'
        context['page_title'] = 'Mettre à jour un officiant'

        return context
