from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import IntegrityError
from django.shortcuts import get_object_or_404, render
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import ResponsibleForm
from apps.organization.models import PlaceOrganization, Organization
from apps.person.models import Responsible
from apps.religion.models import Place
from lib.config import django_logger
from lib.middleware import get_class_instance


class ResponsibleListView(LoginRequiredMixin, ListView):
    # model = PlaceOrganization
    template_name = 'administrator/person/person_list.html'
    context_object_name = 'responsible_list'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceOrganization.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'person'
        context['page_name'] = 'person_list'
        context['page_title'] = 'Liste des responsables'

        return context


class ResponsibleCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Responsible
    template_name = 'administrator/person/person_create.html'
    form_class = ResponsibleForm
    login_url = '/administrator/login/'
    success_url = '/administrator/persons/'
    success_message = "Responsable ajouté avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        initial['organization'] = None
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['place'] = self.place
        kwargs['organization'] = None
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'person'
        context['page_name'] = 'person_create'
        context['page_title'] = 'Créer un responsable'

        return context

    def post(self, request, *args, **kwargs):
        post = request.POST
        initials = self.get_initial()
        place = initials.get('place', None)

        form = ResponsibleForm(data = post, place = place, organization = None)

        if form.is_valid():
            responsible = form.save(commit = False)
            organization_id = post.get('organization', '')

            placeorganization_data = {
                'place': self.place,
                'organization': get_class_instance(Organization, pk = organization_id),
                'responsible': responsible
            }
            placeorganization = PlaceOrganization(**placeorganization_data)

            try:
                responsible.save()
                placeorganization.save()
                return self.form_valid(form)
            except IntegrityError as e:
                django_logger.error(e)
                responsible.delete()
                messages.error(request, _('Une erreur est survenue, vérifiez que cette organisation n\'existe pas déjà'))
                self.object = None
                return self.form_invalid(form)
            except Exception as e:
                django_logger.error(e)
                responsible.delete()
                messages.error(request, _('Une erreur est survenue !!!'))
                self.object = None
                return self.form_invalid(form)


class ResponsibleUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Responsible
    template_name = 'administrator/person/person_update.html'
    form_class = ResponsibleForm
    login_url = '/administrator/login/'
    success_url = '/administrator/persons/'
    success_message = "Responsable mis à jour avec succès"
    place = None
    placeorganization = None

    def get_initial(self):
        initial = super().get_initial()
        instance = self.get_object()
        self.placeorganization = get_object_or_404(PlaceOrganization, responsible = instance)
        self.place = self.placeorganization.place
        initial['place'] = self.place
        initial['organization'] = self.placeorganization.organization
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['place'] = self.place
        kwargs['organization'] = self.placeorganization.organization
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'person'
        context['page_name'] = 'person_update'
        context['page_title'] = 'Mettre à jour un responsable'

        return context

    def form_valid(self, form):
        instance = self.get_object()
        keys = ['csrfmiddlewaretoken', 'organization', 'submit']
        form_data = {k: v for k, v in form.data.items() if k not in keys}

        Responsible.objects.filter(pk = instance.pk).update(**form_data)
        messages.success(self.request, _(self.success_message))
        self.object = None
        return render(self.request, self.template_name, self.get_context_data())

    def post(self, request, *args, **kwargs):
        post = request.POST
        instance = self.get_object()

        self.placeorganization = get_object_or_404(PlaceOrganization, responsible = instance)
        self.place = self.placeorganization.place

        form = ResponsibleForm(data = post, place = self.place, organization = self.placeorganization.organization)

        if form.is_valid():
            organization_id = post.get('organization', '')
            organization = get_class_instance(Organization, pk = organization_id)
            placeorganization = PlaceOrganization.objects.get(responsible = instance)
            placeorganization.organization = organization
            try:
                placeorganization.save(update_fields = ['organization'])
                return self.form_valid(form)
            except IntegrityError as e:
                django_logger.error(e)
                messages.error(request, _('Une erreur est survenue, vérifiez que cette organisation n\'existe pas déjà'))
                self.object = None
                return self.form_invalid(form)
            except Exception as e:
                django_logger.error(e)
                messages.error(request, _('Une erreur est survenue !!!'))
                self.object = None
                return self.form_invalid(form)
