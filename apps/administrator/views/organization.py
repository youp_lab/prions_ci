from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import OrganizationForm
from apps.organization.models import Organization
from apps.religion.models import Place


class OrganizationListView(LoginRequiredMixin, ListView):
    model = Organization
    template_name = 'administrator/organization/organization_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return Organization.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'organization'
        context['page_name'] = 'organization_list'
        context['page_title'] = 'Liste des organisations'

        return context


class OrganizationCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Organization
    template_name = 'administrator/organization/organization_create.html'
    form_class = OrganizationForm
    login_url = '/administrator/login/'
    success_url = '/administrator/organizations/'
    success_message = "Organisation ajoutée avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        initial['place'] = get_object_or_404(Place, slug = 'sainte-cecile')
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'organization'
        context['page_name'] = 'organization_create'
        context['page_title'] = 'Créer une organisation'

        return context


class OrganizationUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Organization
    template_name = 'administrator/organization/organization_update.html'
    form_class = OrganizationForm
    login_url = '/administrator/login/'
    success_url = '/administrator/organizations/'
    success_message = "Organisation mise à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'organization'
        context['page_name'] = 'organization_update'
        context['page_title'] = 'Mettre à jour une organisation'

        return context
