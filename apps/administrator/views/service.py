from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import ServiceForm
from apps.religion.models import Place
from apps.services.models import Service


class ServiceListView(LoginRequiredMixin, ListView):
    model = Service
    template_name = 'administrator/service/service_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return Service.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'service'
        context['page_name'] = 'service_list'
        context['page_title'] = 'Liste des services'

        return context


class ServiceCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Service
    template_name = 'administrator/service/service_create.html'
    form_class = ServiceForm
    login_url = '/administrator/login/'
    success_url = '/administrator/services/'
    success_message = "Service ajouté avec succès"

    def get_initial(self):
        initial = super().get_initial()
        initial['place'] = get_object_or_404(Place, slug = 'sainte-cecile')
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'service'
        context['page_name'] = 'service_create'
        context['page_title'] = 'Créer un service'

        return context


class ServiceUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Service
    template_name = 'administrator/service/service_update.html'
    form_class = ServiceForm
    login_url = '/administrator/login/'
    success_url = '/administrator/services/'
    success_message = "Service mis à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'service'
        context['page_name'] = 'service_update'
        context['page_title'] = 'Mettre à jour un service'

        return context
