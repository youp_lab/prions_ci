from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView


class CustomLoginView(LoginView):
    template_name = 'administrator/auth/login.html'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Login'
        context['page_name'] = 'index'

        return context


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'administrator/common/index.html'
    login_url = '/administrator/login/'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = 'Admin'
        context['page_name'] = 'index'

        return context
