from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import PlaceCategoryForm, PlaceTagForm
from apps.religion.models import Group, Denomination, Place, PlaceCategory, PlaceTag


class CategoryListView(LoginRequiredMixin, ListView):
    model = PlaceCategory
    template_name = 'administrator/post/category_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceCategory.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'category'
        context['page_name'] = 'category_list'
        context['page_title'] = 'Liste des catégories'

        return context


class CategoryCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceCategory
    template_name = 'administrator/post/category_create.html'
    form_class = PlaceCategoryForm
    login_url = '/administrator/login/'
    success_url = '/administrator/categories/'
    success_message = 'Catégorie ajoutée avec succès'
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        initial['denomination'] = get_object_or_404(Denomination, slug = self.place.denomination.slug)
        initial['group'] = get_object_or_404(Group, slug = self.place.denomination.group.slug)
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'category'
        context['page_name'] = 'category_create'
        context['page_title'] = 'Créer une catégorie'

        return context


class CategoryUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceCategory
    template_name = 'administrator/post/category_update.html'
    form_class = PlaceCategoryForm
    login_url = '/administrator/login/'
    success_url = '/administrator/categories/'
    success_message = 'Catégorie mise à jour avec succès'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'category'
        context['page_name'] = 'category_update'
        context['page_title'] = 'Mettre à jour une catégorie'

        return context


class TagListView(LoginRequiredMixin, ListView):
    model = PlaceTag
    template_name = 'administrator/post/tag_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceTag.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'tag'
        context['page_name'] = 'tag_list'
        context['page_title'] = 'Liste des tags'

        return context


class TagCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceTag
    template_name = 'administrator/post/tag_create.html'
    form_class = PlaceTagForm
    login_url = '/administrator/login/'
    success_url = '/administrator/tags/'
    success_message = 'Tag ajouté avec succès'
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        initial['denomination'] = get_object_or_404(Denomination, slug = self.place.denomination.slug)
        initial['group'] = get_object_or_404(Group, slug = self.place.denomination.group.slug)
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'tag'
        context['page_name'] = 'tag_create'
        context['page_title'] = 'Créer un tag'

        return context


class TagUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceTag
    template_name = 'administrator/post/tag_update.html'
    form_class = PlaceTagForm
    login_url = '/administrator/login/'
    success_url = '/administrator/tags/'
    success_message = 'Tag mis à jour avec succès'

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'tag'
        context['page_name'] = 'tag_update'
        context['page_title'] = 'Mettre à jour un tag'

        return context
