from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, ListView, UpdateView

from apps.administrator.forms import PlaceCommunityForm
from apps.religion.models import Group, Denomination, Place, PlaceCommunity


class CommunityListView(LoginRequiredMixin, ListView):
    model = PlaceCommunity
    template_name = 'administrator/community/community_list.html'
    login_url = '/administrator/login/'

    def get_queryset(self):
        place = get_object_or_404(Place, slug = 'sainte-cecile')
        return PlaceCommunity.objects.filter(place = place)

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'community'
        context['page_name'] = 'community_list'
        context['page_title'] = 'Liste des events'

        return context


class CommunityCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceCommunity
    template_name = 'administrator/community/community_create.html'
    form_class = PlaceCommunityForm
    login_url = '/administrator/login/'
    success_url = '/administrator/communities/'
    success_message = "Evénement ajouté avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        initial['denomination'] = get_object_or_404(Denomination, slug = self.place.denomination.slug)
        initial['group'] = get_object_or_404(Group, slug = self.place.denomination.group.slug)
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'community'
        context['page_name'] = 'community_create'
        context['page_title'] = 'Créer un événement'

        return context


class CommunityUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceCommunity
    template_name = 'administrator/community/community_update.html'
    form_class = PlaceCommunityForm
    login_url = '/administrator/login/'
    success_url = '/administrator/communities/'
    success_message = "Evénement mis à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'community'
        context['page_name'] = 'community_update'
        context['page_title'] = 'Mettre à jour un événement'

        return context
