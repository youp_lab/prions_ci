from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from extra_views import InlineFormSetFactory, UpdateWithInlinesView, NamedFormsetsMixin, SuccessMessageMixin

from apps.administrator.forms import PlaceForm
from apps.religion.models import Place, PlaceOrganizational


class PlaceOrganizationalInline(InlineFormSetFactory):
    model = PlaceOrganizational
    fields = '__all__'
    # form_class = PlaceOrganizationalForm
    # initial = [{'name': 'example1'}, {'name', 'example2'}]
    factory_kwargs = {'extra': 1, 'max_num': 1, 'can_order': False, 'can_delete': False}


class PlaceView(LoginRequiredMixin, SuccessMessageMixin, NamedFormsetsMixin, UpdateWithInlinesView):
    model = Place
    inlines = [PlaceOrganizationalInline]
    inlines_names = ['PlaceOrganizational']
    template_name = 'administrator/common/place.html'
    form_class = PlaceForm
    login_url = '/administrator/login/'
    success_url = '/administrator/'
    success_message = "Paroisse mise à jour avec succès"

    def get_object(self, queryset = None):
        return get_object_or_404(Place, slug = 'sainte-cecile')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['place'] = self.object
        return kwargs

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'place'
        context['page_name'] = 'place'
        context['page_title'] = '{0} {1}, {2}'.format(self.object.place_type, self.object.name, self.object.city)

        return context

    # def post(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     form = PlaceForm(request.POST, request.FILES, instance = instance)
    #     post = form.data
    #     print("instance => {}".format(instance))
    #     print("post => {}".format(post))
    #     print("form.is_valid() => {}".format(form.is_valid()))
    #     print("form errors => {}".format(form.errors))
    #
    #     if form.is_valid():
    #         form.save()
    #         messages.success(request, _('Mise à jour effectuée avec succès'))
    #     else:
    #         messages.error(request, _('Error lors de la mise à jour'))
    #     form = PlaceForm(request.POST)
    #     context = {
    #         'menu': 'place',
    #         'page_name': 'place',
    #         'page_title': '{0} {1}, {2}'.format(instance.place_type, instance.name, instance.city),
    #         'form': form
    #     }
    #     return render(request, self.template_name, context)
