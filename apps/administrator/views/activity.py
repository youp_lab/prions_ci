from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, UpdateView

from apps.administrator.forms import PlaceActivityForm, PlaceActivityDetailsForm
from apps.religion.models import PlaceActivity, Place, PlaceActivityDetails


class ActivityCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceActivity
    template_name = 'administrator/activity/activity_create.html'
    form_class = PlaceActivityForm
    login_url = '/administrator/login/'
    success_url = '/administrator/activity/'
    success_message = "Activité ajoutée avec succès"
    place = None

    def get_initial(self):
        initial = super().get_initial()
        self.place = get_object_or_404(Place, slug = 'sainte-cecile')
        initial['place'] = self.place
        return initial

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'activity'
        context['page_name'] = 'activity_create'
        context['page_title'] = 'Ajouter un horaire'
        context['place_activity_list'] = PlaceActivity.objects.filter(place = self.place)

        return context


class ActivityUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceActivity
    template_name = 'administrator/activity/activity_update.html'
    form_class = PlaceActivityForm
    login_url = '/administrator/login/'
    success_url = '/administrator/activity/'
    success_message = "Activité mise à jour avec succès"

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'activity'
        context['page_name'] = 'activity_update'
        context['page_title'] = 'Modifier un horaire'
        context['place_activity_list'] = PlaceActivity.objects.filter(place = self.object.place)

        return context


class ActivityDetailsCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = PlaceActivityDetails
    template_name = 'administrator/activity/activity_detail_create.html'
    form_class = PlaceActivityDetailsForm
    login_url = '/administrator/login/'
    success_message = "Programmation ajoutée avec succès"
    place_activity = None

    def get_success_url(self):
        return reverse('administrator:schedule-create', kwargs = {'pk': self.kwargs['pk']})

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'activity'
        context['page_name'] = 'schedule_create'
        context['page_title'] = 'Gestion des horaires'
        place_activity = get_object_or_404(PlaceActivity, pk = self.kwargs['pk'])
        context['place_activity'] = place_activity
        activity_detail_list = PlaceActivityDetails.objects.filter(place_activity = self.kwargs['pk'])
        context['activity_detail_list'] = activity_detail_list

        return context


class ActivityDetailsUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = PlaceActivityDetails
    template_name = 'administrator/activity/activity_detail_update.html'
    form_class = PlaceActivityDetailsForm
    login_url = '/administrator/login/'
    # success_url = '/administrator/activity/'
    success_message = "Programmation mise à jour avec succès"
    place_activity = None

    def get_success_url(self):
        return reverse('administrator:schedule-update', kwargs = {'pk': self.kwargs['pk']})

    def get_context_data(self, *, object_list = None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu'] = 'activity'
        context['page_name'] = 'schedule_update'
        context['page_title'] = 'Gestion des horaires'
        place_activity = get_object_or_404(PlaceActivity, pk = self.object.place_activity.pk)
        context['place_activity'] = place_activity
        activity_detail_list = PlaceActivityDetails.objects.filter(place_activity = self.object.place_activity.pk)
        context['activity_detail_list'] = activity_detail_list

        return context
