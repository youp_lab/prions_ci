from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.retirement.models import *

for model in apps.get_app_config('retirement').models.values():
    admin.site.register(model)


class RetirementTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']


class RetirementCountryAdmin(admin.ModelAdmin):
    search_fields = ['name']


class RetirementCityAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = ['country']


class RetirementEnvironmentAdmin(admin.ModelAdmin):
    search_fields = ['name']


class RetirementDevotionObjectAdmin(admin.ModelAdmin):
    search_fields = ['name']


class RetirementTravelInstanceInline(admin.StackedInline):
    model = RetirementTravelInstance
    extra = 1


class RetirementTravelInline(admin.StackedInline):
    model = RetirementTravel
    extra = 1
    inlines = [RetirementTravelInstanceInline, ]


class RetirementPlaceLinkInline(admin.StackedInline):
    model = RetirementPlaceLink
    extra = 1
    # autocomplete_fields = ['type']
    search_fields = ['type', 'title']


class RetirementPlaceAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = ['city', 'environment', 'devotion_object', 'denomination']
    list_display = ('name', 'denomination', 'description')
    inlines = [RetirementPlaceLinkInline, RetirementTravelInline, ]

    # exclude = ['links']

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class RetirementTravelAdmin(admin.ModelAdmin):
    # search_fields = ['name']
    autocomplete_fields = ['retirement_place']
    list_display = ('title', 'retirement_place')
    inlines = [RetirementTravelInstanceInline, ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set added_by during the first save.
            obj.created_by = request.user
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


admin.site.unregister(RetirementType)
admin.site.register(RetirementType, RetirementTypeAdmin)
admin.site.unregister(RetirementCountry)
admin.site.register(RetirementCountry, RetirementCountryAdmin)
admin.site.unregister(RetirementCity)
admin.site.register(RetirementCity, RetirementCityAdmin)
admin.site.unregister(RetirementEnvironment)
admin.site.register(RetirementEnvironment, RetirementEnvironmentAdmin)
admin.site.unregister(RetirementDevotionObject)
admin.site.register(RetirementDevotionObject, RetirementDevotionObjectAdmin)
admin.site.unregister(RetirementPlace)
admin.site.register(RetirementPlace, RetirementPlaceAdmin)
admin.site.unregister(RetirementTravel)
admin.site.register(RetirementTravel, RetirementTravelAdmin)
