from django.db import models
from sorl.thumbnail import ImageField

from apps.core.enums import STATE_CHOICES, ACTIVATED, STATUS_CHOICES, PUBLISHED
from apps.location.models import Geo
from apps.managers import ReligionQuerySet
from apps.religion.models import Religion
from lib.middleware import Hider, Monitor, upload_path, to_slug


class RetirementType(Monitor, Religion):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 60, verbose_name = 'Nom')
    place = Hider()
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Type de sortie'
        verbose_name_plural = 'Types de sorties'
        ordering = ['id']

    def __str__(self):
        return self.name


class RetirementTarget(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Cible (Pour qui ?)'
        verbose_name_plural = 'Cibles (Pour qui ?)'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementReason(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Raison (Pourquoi ?)'
        verbose_name_plural = 'Raisons (Pourquoi ?)'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementCountry(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')
    codename = models.CharField(max_length = 60, unique = True, editable = False)

    class Meta:
        verbose_name = 'Pays'
        verbose_name_plural = 'Pays'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        codename = to_slug(self.name)
        if self.codename != codename:
            self.codename = codename
        super(RetirementCountry, self).save(*args, **kwargs)


class RetirementCity(Monitor):
    id = models.AutoField(primary_key = True)
    country = models.ForeignKey(RetirementCountry, models.SET_NULL, blank = True, null = True, verbose_name = 'Pays')
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')
    codename = models.CharField(max_length = 60, unique = True, editable = False)

    class Meta:
        verbose_name = 'Ville'
        verbose_name_plural = 'Villes'

    def __str__(self):
        return '{} - {}'.format(self.name, self.country.name)

    def save(self, *args, **kwargs):
        codename = to_slug(self.name)
        if self.codename != codename:
            self.codename = codename
        super(RetirementCity, self).save(*args, **kwargs)


class RetirementEnvironment(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Environnement'
        verbose_name_plural = 'Environnements'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementDevotionObject(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Objet de dévotion'
        verbose_name_plural = 'Objets de dévotions'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementMotivationType(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Type de motivation'
        verbose_name_plural = 'Types de motivations'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementRecourse(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Recours'
        verbose_name_plural = 'Recours'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementConvenience(Monitor):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 160, unique = True, verbose_name = 'Nom')

    class Meta:
        verbose_name = 'Commodité'
        verbose_name_plural = 'Commodités'
        ordering = ['id']

    def __str__(self):
        return '{}'.format(self.name)


class RetirementLinkType(Monitor):
    id = models.AutoField(primary_key = True)
    title = models.CharField(max_length = 160, verbose_name = 'Titre')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Lien utile : Type'
        verbose_name_plural = 'Liens utiles : Types'

    def __str__(self):
        return '{}'.format(self.title)


class RetirementPlace(Monitor, Geo):
    id = models.AutoField(primary_key = True)
    target = models.ManyToManyField(RetirementTarget, related_name = 'place_targets', verbose_name = 'Pour qui ?')
    reason = models.ManyToManyField(RetirementReason, related_name = 'place_reasons', verbose_name = 'Pourquoi ?')
    city = models.ForeignKey(RetirementCity, models.DO_NOTHING, verbose_name = 'Où ?')
    environment = models.ForeignKey(RetirementEnvironment, models.DO_NOTHING, verbose_name = 'Environnement')
    devotion_object = models.ForeignKey(RetirementDevotionObject, models.DO_NOTHING, verbose_name = 'Objet de dévotion')
    motivation_type = models.ManyToManyField(RetirementMotivationType, related_name = 'place_motivation_types', verbose_name = 'Type de motivation')
    recourse = models.ManyToManyField(RetirementRecourse, related_name = 'place_recourses', verbose_name = 'Recours')
    denomination = models.ForeignKey('religion.Denomination', models.SET_NULL, blank = True, null = True, verbose_name = 'Dénomination')
    name = models.CharField(max_length = 160, verbose_name = 'Nom')
    slug = models.SlugField(unique = True, editable = False)
    description = models.TextField(blank = True, verbose_name = 'Description')
    image = ImageField(upload_to = upload_path, blank = True, null = True)
    contact = models.CharField(max_length = 30, blank = True, verbose_name = 'Contact Téléphonique')
    fax = models.CharField(max_length = 30, blank = True, verbose_name = 'Fax')
    email = models.CharField(max_length = 160, blank = True)
    web_site = models.CharField(max_length = 160, blank = True)
    status = models.BooleanField(choices = STATE_CHOICES, default = ACTIVATED)
    convenience = models.ManyToManyField(RetirementConvenience, related_name = 'conveniences', verbose_name = 'Commodité')
    particularity = models.TextField(blank = True, verbose_name = 'Disposition particulière')
    e_shop = models.URLField(blank = True, null = True, verbose_name = 'E-boutique')
    order_link = models.URLField(blank = True, null = True, verbose_name = 'Lien de réservation')
    # links = models.ManyToManyField('RetirementPlaceLink', related_name = 'links', blank = True, verbose_name = 'Liens utiles')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Lieu de pèlerinage & retraite'
        verbose_name_plural = 'Lieux de pèlerinages & retraites'
        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        slug = to_slug(self.name)
        if self.slug != slug:
            self.slug = slug
        super(RetirementPlace, self).save(*args, **kwargs)


class RetirementPlaceLink(models.Model):
    retirement_place = models.ForeignKey(RetirementPlace, models.CASCADE, related_name = 'links', verbose_name = 'Lieu de pèlerinage & retraite')
    type = models.ForeignKey(RetirementLinkType, models.CASCADE, verbose_name = 'Catégorie')
    title = models.CharField(max_length = 160, default = '', verbose_name = 'Titre')
    url = models.URLField(default = '', verbose_name = 'Lien')

    class Meta:
        verbose_name = 'Lien utile'
        verbose_name_plural = 'Liens utiles'

    def __str__(self):
        return '{0} : {1} - {2}'.format(self.retirement_place, self.type, self.title)


class RetirementTravel(Monitor):
    id = models.AutoField(primary_key = True)
    type = models.ForeignKey(RetirementType, models.PROTECT, verbose_name = 'Type')
    retirement_place = models.ForeignKey(RetirementPlace, models.PROTECT, related_name = "retirement_place_travels",
                                         verbose_name = 'Lieu de pèlerinage/retraite')
    title = models.CharField(max_length = 160, verbose_name = 'Titre')
    slug = models.SlugField(unique = True, editable = False)
    description = models.TextField(blank = True, verbose_name = 'Description')
    image = ImageField(upload_to = upload_path, blank = True, null = True)
    order_link = models.URLField(blank = True, null = True, verbose_name = 'Lien de réservation')
    # departure = models.DateField(verbose_name = 'Date de départ')
    # arrival = models.DateField(verbose_name = 'Date d\'arrivée')
    # duration = models.CharField(max_length = 160, blank = True, verbose_name = 'Durée')
    status = models.BooleanField(choices = STATUS_CHOICES, default = PUBLISHED)
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Evénement'
        verbose_name_plural = 'Evénements'

    def __str__(self):
        return '{} - {}'.format(self.title, self.retirement_place.name)

    def save(self, *args, **kwargs):
        slug = to_slug(self.title)
        if self.slug != slug:
            self.slug = slug
        super(RetirementTravel, self).save(*args, **kwargs)


class RetirementTravelInstance(Monitor):
    id = models.AutoField(primary_key = True)
    retirement_travel = models.ForeignKey(RetirementTravel, models.PROTECT, verbose_name = 'Voyage')
    description = models.TextField(blank = True, verbose_name = 'Description')
    image = ImageField(upload_to = upload_path, blank = True, null = True)
    order_link = models.URLField(blank = True, null = True, verbose_name = 'Lien de réservation')
    departure_place = models.CharField(max_length = 160, default = 'Non communiqué', blank = True, verbose_name = 'Lieu de départ')
    departure_date = models.DateField(verbose_name = 'Date de début')
    arrival_date = models.DateField(verbose_name = 'Date de fin')
    particularity = models.CharField(max_length = 150, default = '', blank = True, verbose_name = 'Dispositions particulières')

    duration = models.CharField(max_length = 160, blank = True, verbose_name = 'Durée')
    objects = ReligionQuerySet().as_manager()

    class Meta:
        verbose_name = 'Instance de voyage'
        verbose_name_plural = 'Instances de voyages'
        ordering = ['departure_date']

    def __str__(self):
        return '{} - {}'.format(self.retirement_travel.title, self.departure_date)

# class Retirement(Monitor, Religion):
#     id = models.BigAutoField(primary_key = True)
#     reference = models.CharField(max_length = 25, blank = True, editable = False)
#     retirement_place = models.ForeignKey(RetirementPlace, models.PROTECT, verbose_name = 'Lieu de retraite')
#     target = models.ManyToManyField(RetirementTarget, related_name = 'targets', verbose_name = 'Pour qui ?')
#     reason = models.ManyToManyField(RetirementReason, related_name = 'reasons', verbose_name = 'Pourquoi ?')
#     title = models.CharField(max_length = 160, blank = True, verbose_name = 'Titre')
#     description = models.TextField(max_length = 255, blank = True)
#     contact = models.CharField(max_length = 30, blank = True, verbose_name = 'Contact Téléphonique')
#     web_site = models.CharField(max_length = 160, blank = True)
#     email = models.CharField(max_length = 160, blank = True)
#     order_link = models.URLField(blank = True, null = True, verbose_name = 'Lien de réservation')
#     departure = models.DateField(verbose_name = 'Date de départ')
#     arrival = models.DateField(verbose_name = 'Date de départ')
#     duration = models.CharField(max_length = 160, blank = True, verbose_name = 'Nom de jours')
#     image = ImageField(upload_to = 'uploads/retirement/', blank = True, null = True)
#     objects = ReligionQuerySet().as_manager()
#
#     class Meta:
#         verbose_name = 'Voyage'
#         verbose_name_plural = 'Voyages'
#
#     def __str__(self):
#         return '{0} : {1}'.format(self.title, self.retirement_place.name)
