from django.apps import AppConfig


class RetirementConfig(AppConfig):
    name = 'apps.retirement'
    verbose_name = 'Pèlerinages & retraites'
