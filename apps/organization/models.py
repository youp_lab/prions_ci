from django.db import models
from sorl.thumbnail import ImageField

from lib.middleware import Monitor, upload_path


class Organization(Monitor):
    id = models.BigAutoField(primary_key = True)
    place = models.ForeignKey('religion.Place', models.SET_NULL, blank = True, null = True, related_name = 'organizations',
                              verbose_name = 'Lieu de prière')
    name = models.CharField(max_length = 150, verbose_name = 'Nom')
    description = models.CharField(max_length = 255, blank = True, verbose_name = 'Description')
    # image = ImageField(upload_to = upload_path, blank = True, null = True, verbose_name = 'Image de l\'organisation')

    class Meta:
        verbose_name = 'Organisation'
        verbose_name_plural = 'Organisations'
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


class PlaceOrganization(models.Model):
    place = models.ForeignKey('religion.Place', models.CASCADE, verbose_name = 'Lieu de prière')
    organization = models.ForeignKey(Organization, models.CASCADE, verbose_name = 'Organisation')
    responsible = models.ForeignKey('person.Responsible', models.CASCADE, related_name = 'placeresponsible', verbose_name = 'Responsable')

    class Meta:
        unique_together = (('place', 'organization'),)
        verbose_name = 'Organisation et Responsable de lieu de culte'
        verbose_name_plural = 'Organisations et Responsables des lieux de culte'

    def __str__(self):
        return '{0} : {1} - {2}'.format(self.place, self.organization, self.responsible)
