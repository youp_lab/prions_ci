from django.apps import apps
from django.contrib import admin

# Register your models here.
from apps.organization.models import Organization, PlaceOrganization

for model in apps.get_app_config('organization').models.values():
    admin.site.register(model)


class OrganizationAdmin(admin.ModelAdmin):
    list_filter = ('place',)
    search_fields = ['name', 'place']
    autocomplete_fields = ['place']
    list_display = ('id', 'place', 'name', 'description',)


class PlaceOrganizationAdmin(admin.ModelAdmin):
    list_filter = ('place', 'organization', 'responsible',)
    search_fields = ['place', 'organization', 'responsible']
    autocomplete_fields = ['place', 'organization', 'responsible']
    list_display = ('id', 'place', 'organization', 'responsible',)


admin.site.unregister(Organization)
admin.site.register(Organization, OrganizationAdmin)
admin.site.unregister(PlaceOrganization)
admin.site.register(PlaceOrganization, PlaceOrganizationAdmin)
